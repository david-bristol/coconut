How recursive xgcd works
========================

xgcd(a, b) should return three numbers (x, y, g) such that
g = GCD(a, b) and ax+by=g. Let's assume that b >= 0 (we don't define how to work
modulo a negative number).

If b = 0 we're done: xgcd(a, 0) = (1,0,a) since GCD(a, 0) = a and a*1+0*0 = a.
If b is not 0, let's try the normal GCD recursion. Let's call
(x', y', g') = xgcd(b, a%b). We know that g' = g is the GCD we want since
GCD(a, b) = GCD(b, a%b). We just have to find our x,y from the x',y' that the
recursive call returns.

We know that bx'+(a%b)y'=g. Let's write out (a%b): there's a unique (q,r) such
that a = qb+r and r is in {0,...,b-1}; in fact q=a/b (integer division) and r=
a%b. So we have bx'+ry'=g. Substitute r=a-qb to get bx'+(a-qb)y'=g and rearrange
to get ay'+b(x'-qy')=g. And now we can substitute back q=a/b to get
ay'+b(x'-a/b y')=g, i.e. with x=y' and y=x'-a/b y' we get our ax+by=g.

So the algorithm is

def xgcd(a,b):
    if b==0:
        return (1,0,a)
    else:
        (xx,yy,g)=xgcd(b,a%b)
        return (yy,xx-(a//b)*yy,g)

Perhaps we want to unwind that recursion. The (g=x;x=1) bit in the middle is
because if we get a (1,0) on the stack then g=1 and we're fine to unwind again;
if we reached (g,0) on the stack with g>1 then the recursive gcd would return
(1,0) for this call so we store g and set the value (x,y) to (1,0).

def xgcd_inline(a,b):
    if b==0:
        return (1,0,a)
    stack=[(a,b)]
    while (b > 0):
        (a,b)=(b,a%b)
        stack.append((a,b))
    (x,y)=stack.pop()
    g=x
    x=1
    while len(stack) > 0:
        (a,b)=stack.pop()
        (x,y)=(y,x-a/b*y)
    return (x,y,g)    

Without proof: xgcd (whether inline or not) for numbers of k bits takes around
k steps, so for numbers around n that's around log2(n) steps (more on this kind
of thing in complexity theory). Our inlined xgcd still uses on the order of
log(n)^2 memory for the stack (around log(n) entries, the entries themselves are
of a size proportional to log(n) too). It's possible to use only a constant
number of variables (4, to be precise) instead of a stack, but that's another
story.


