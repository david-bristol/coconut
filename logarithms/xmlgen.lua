-- utility functions for generating XML inline.
--
-- the callback technique to take a normal parameter and an object is from some
-- lua stackexchange question.
--
-- use:
--     xml.node('a') { href="/", id="home" } -> '<a href='/' id='home'></a>'
--     xml.node('p') { class="warn", _="Danger" } -> '<p class=warn'>Danger</p>'
-- If _ is a table, its entries are added one by one with newlines at the end.

local function render_table(name, t)
    local buf = {'<', name}
    for k,v in pairs(t) do
        if not (k == '_') then
            table.insert(buf, ' ' .. k .. '="' .. v .. '"')
        end
    end
    if t._ then
        table.insert(buf, '>')
        if type(t._) == "table" then
        for _, item in ipairs(t._) do
            table.insert(buf, item)
            table.insert(buf, '\n')
        end
        else
            table.insert(buf, t._)
        end
        table.insert(buf, '</' .. name .. '>')
    else
        table.insert(buf, '/>')
    end
    return table.concat(buf, '')
end

return {
    node = function(name, t)
        return function(t)
            return render_table(name, t)
        end
    end,

    stream = function(dest, name, f)
        dest:write('<' .. name .. '>\n')
        f(dest)
        dest:write('</' .. name .. '>\n')
    end
}
