﻿# Logarithms

## 1.

Back in primary school, I learnt to count with [Cuisenaire rods](https://en.wikipedia.org/wiki/Cuisenaire_rods). They are little rods that come in different lengths from 1 to 10 cm and each length is a different colour. To calculate `5 + 3`, you put a 5-rod on the end of a 3-rod, and notice that this is as long as an 8-rod:

![rods](rods.png)

For slightly larger numbers, or if you don't have a box of rods to hand, you can use two rulers. To compute `3+5`, line up the rulers so the zero of the second ruler is below the 3 on the first ruler. Then find the 5 on the second ruler; the number above it will be the sum:

![rulers](rulers.png)

Technically, what you do is line up the *neutral element* of the second ruler against the first number to add. But you can't use those words when explaining addition to primary school children.

## 2.

Can we do the same for multiplication?

If we tried to build a set of multiplication rods, then we'd notice that a 1-rod would have to have no width at all, as multiplying by 1 does not change a number (for the same reason, there is no 0-rod in the original set).

Let's arbitrarily decide that a 2-rod should be 1cm long. Whether we use cm, inches or some other unit of measure shouldn't matter, after all.

A 4-rod should now be 2cm long, as `2 * 2` means put two 2-rods in a line, and the result is 2cm long. Similarly, `8 = 2 * 2 * 2` will be 3cm long. In fact, `2^x` for integer `x` should be `x` cm long this way:

![multiplication rods](mulrods1.png)

So how long should a 3-rod be? It must be something between 1cm and 2cm (because `2 < 3 < 4`). Specifically, it should be `x` cm long such that `2^x = 3`. If we try out numbers until we get close, we find that `x` is around 1.584963. Two of thes 3-rods in a line would be around 3.17cm long, which is just a bit longer than an 8-rod at 3cm, which is about the right size for a 9-rod.

Of course, there is a more precise way to describe these lengths:

  * A rod of length `y` represents the number `2^y`.
  * The rod for number `x` is `log2(x)` cm long.

Here is a table of `log2(x)`, pronounced "the logarithm to base 2 of x" for
the first few integers:

|  x | log2(x) |  x | log2(x) |  x | log2(x) |  x | log2(x) |
|---:|:--------|---:|:--------|---:|:--------|---:|:--------|
|  1 | 0.0     | 11 | 3.45943 | 21 | 4.39232 | 31 | 4.95420 |
|  2 | 1.0     | 12 | 3.58496 | 22 | 4.45943 | 32 | 5.0     |
|  3 | 1.58496 | 13 | 3.70044 | 23 | 4.52356 | 33 | 5.04440 |
|  4 | 2.0     | 14 | 3.80735 | 24 | 4.58496 | 34 | 5.08746 |
|  5 | 2.32193 | 15 | 3.90689 | 25 | 4.64386 | 35 | 5.12929 |
|  6 | 2.58496 | 16 | 4.0     | 26 | 4.70044 | 36 | 5.16993 |
|  7 | 2.80735 | 17 | 4.08746 | 27 | 4.75489 | 37 | 5.20945 |
|  8 | 3.0     | 18 | 4.16992 | 28 | 4.80735 | 38 | 5.24793 |
|  9 | 3.16992 | 19 | 4.24793 | 29 | 4.85800 | 39 | 5.28540 |
| 10 | 3.32193 | 20 | 4.32193 | 30 | 4.90690 | 40 | 5.32193 |

The choice of basis for a logarithm is simply choosing which rod we want to make 1cm long. If we had chosen basis 3, then the 3-rod would be 1cm long and all rods would be shorter by a factor of 1.58496 than they are now.

We can do the same thing with rulers. Here is `3 * 4 = 12`:

  * Find the first factor (3) on the first ruler and line up the neutral element (1) of the second ruler below it.
  * Find the second factor (4) on the second ruler, the number above (12) is the product.

![slide rule](sliderule.png)

These rulers are drawn to a scale of 3.8:1 (the distance between 1 and 2 is 3.8cm). But as long as both rulers use the same scale, it really doesn't matter - 3 times 4 is 12 whatever logarithm you use to compute this, after all. In terms of logarithms, these rulers simply use a different basis.

Two rulers with a logarithmic scale that you can slide against each other are called a [slide rule](https://en.wikipedia.org/wiki/Slide_rule). These were a common calculating device before electronic calculators were invented.

## 3.

Exponentiation to any basis `B > 0` has the nice properties

  * `B^0 = 1`
  * `B^(u+v) = B^u B^v`

In the language of algebra, exponentiation is a *homomorphism* from the additive group to the multiplicative group. The additive group can be defined using the integers or the real numbers, the group operation is addition. The multiplicative group is all integers/reals without 0, with multiplication as the operation.

Since we are computer scientists, we might as well take `B = 2`. It doesn't really matter what basis we pick.

Is exponentiation an *isomorphism*? That is, does it have an inverse? (Note that the inverse has to apply to everything except 0, since that is banned from the multiplicative group in the first place.)

The logarithm functions (for any fixed basis `B > 0`) are functions with the properties

  * `log(ab) = log(a) + log(b)`
  * `log(1) = 0`

This is a homomorphism in the other direction, and `logB(B^x) = x`, `B^(logB(y)) = y` so these functions are inverses of each other. We have indeed found an isomorphism between addition and multiplication.

This explains why the same trick with rods or rulers works for both addition and multiplication, as long as you have the correct kind of rulers.

## 4.

A couple of notes on computing with logarithms if you are taking the CNuT exam.

The change-of-basis formula is `logB(x) = logA(x)/logA(B)`. For example, if you have a calculator that does logarithms to basis `A = 10`, then to calculate `log2(x)` you divide `log10(x)` by `log10(2)`. The constant `log10(2)` is 0.6931472, or equivalently you can multiply with the inverse: to compute `log2(x)`, compute `log10(x)` on the calculator and multiply with 1.442695.

To compute logarithms of fractions, `log2(a/b) = log2(a) - log2(b)`. So for example to compute `log2(5/11)`, we could look up in the table above that `log2(5) = 2.32193` and `log2(11) = 3.45943` so `log2(5/11) = 2.32193 - 3.45943 = (-1.1375)`. The logarithm of something less than 1 will always be negative (which is why the definition of entropy has a minus sign to make it positive again).

## 5.

Do logarithms still work over finite fields?

If we have a field, we have addition and multiplication and the distributive law that says if `a = 1 + ... + 1` (`n` times) then `a × b = b × ... × b` (`n` times). For example, `3 = 1 + 1 + 1` (at least in characteristic 5 or higher) so `3 × b = b + b + b`. How you define multiplication for elements that are not sums of 1s is the interesting part about finite fields, but for the fields GF(p) with a prime number of elements this problem does not occur.

We can do the next step and define exponentiation the usual way: if `a = 1 + ... + 1` (`n` times) then b^a = b × ... × b` (`n` times). In GF(p) this gets you the usual exponentiation, where you can reduce exponents modulo phi(p) with Euler's totient function - which just gives phi(p) = p - 1 if p is a prime.

This particular function is still an isomorphism, as long as you get the domains right and the basis element is a generator of the multiplicative group modulo p. Specifically, for any b in GF(p) that is a generator of the multiplicative group, exponentiation to basis b is an invertible function from {0, ..., p-1} to {1, ..., p-1} and `b^0 = 1` and `b^{u+v} = b^u × b^v`. The last rule also holds for any other nonzero b, but the function is not invertible any more.

This lets us define the discrete logarithm to basis B in a field GF(p): `dlogB(x)` is the value `y` such that `B^y = x`. There is always exactly one such value as long as `x` is not zero and B is a generator.

But the discrete logarithm is much more interesting than that.

In cryptography, we usually start by picking a really large prime p - let's say around 3000 bits long. The multiplicative group has order `phi(p) = p-1` which has a factor 2 as p is odd. If we're lucky, `(p-1)/2` has a large prime factor q itself, let's say around 256 bits long. This creates an order-q subgroup of the multiplicative group, which has a generator - let's call one such generator `g`. Within this subgroup of q elements, the discrete logarithm to basis `g` can be defined.

For an example with small numbers, consider p = 11. (p-1) factors as 2 × 5 so we take q = 5, and for example `g = 4` has order 5 under multiplication, as
`4×4 = 5, 4×5 = 9, 4×9 = 3, 4×3 = 1`. As a table

|  `x` | `4^x mod 11` |
|-----:|-------------:|
|    0 |            1 |
|    1 |            4 |
|    2 |            5 |
|    3 |            9 |
|    4 |            3 |

So `dlog4(5) = 2` for example.

## 6.

If the number p is `n` = 3000 bits long, then adding or multiplying numbers modulo p takes time proportional to `n`, or some small power of `n` like `n^3`. 
We call these operations *efficient*. Exponentiation is an efficient operation too, if you use the square-and-multiply algorithm or a variation of it.
However, finding discrete logarithms is to the best of everyone's knowledge not an efficent operation!

The obvious "try all possibilities" algorithm takes `2^n` time, and for `n = 3000` you really don't want to do that. The fanciest algorithm known to mathematicians for this job, the number field sieve, has a running time so complex that you need to define new functions jsut to write it down, but it's only marginally better than trying all possibilities.

In summary:

  * In theory, exponentiation is an isomorphism from an additive group to a multiplicative one, and the logarithm is its inverse. This is true both for the real numbers and for finite fields.
  * In practice, discrete exponentiation is "easy" and discrete logarithms are "hard".

What discrete exponentiation gives us in other words is a *one-way homomorphism*. This idea is the foundation of most modern cryptography. For example, Diffie-Hellman key exchange (as discussed in Security 101) has each person pick a secret `x` and send a public `y = g^x` (modulo p). Then each person exponentiates the other person's public element with their secret, and because exponentiation is a homomorphism, they both get the same key.
