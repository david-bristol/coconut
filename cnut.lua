-- Lua script to support calculations in the CNuT unit.
-- Open a terminal and run `lua -i cnut.lua` to load these
-- functions.

-- logarithms and stuff for information theory --

-- logarithm to base 2
function log(x) return math.log(x)/math.log(2) end

-- main entropy function h(x) = -x*log(x) with h(0) = 0
function h(x) if x == 0 then return 0 else return - x * log(x) end end

-- binary entropy
function h2(x) return h(x) + h(1-x) end

-- polynomials, finite fields --

-- These two global variables control how polynomial and matrix
-- arithmetic works. Setting "modulus" to a positive integer
-- reduces modulo this integer; setting "modpoly" to a polynomial
-- reduces modulo this polynomial too after polynomial multiplication.
-- Setting them back to `nil` turns this feature off again.
modulus = nil
modpoly = nil

-- modular inversion. Returns 1/x modulo 'modulus' if it is set.
function modinv(x)
    assert(modulus, "no modulus set")
    -- doing this the silly way because I'm tired and have run out of chocolate
    for i = 1, modulus do   
        if (i * x) % modulus == 1 then return i end
    end
    assert(false, "no inverse found - proably a zero-divisor")
end

-- BEGIN internal stuff, nothing to see here, move on --

    function modif(a) if modulus then return a % modulus else return a end end

    -- This is the "metatable" for polynomials which is how
    -- operator overloading works in lua.
    POLY = {
        -- is it a bird? is it a plane? is it a polynomial?
        is = function(x)
            return type(x) == "table" and x._type == "poly"
        end,

        -- create a new instance
        new = function(p)
            local r = {}
            for _, v in ipairs(p) do
                assert(type(v)=="number", "poly() coefficients must be integers")
                table.insert(r, modif(v))
            end
            r._type = "poly"
            setmetatable(r, POLY)
            return r
        end,

        -- evaluate a polynomial at a point
        eval = function(p, x)
            assert(POLY.is(p) and type(x) == "number", "wrong types")
            local t = 0
            local z = 1
            for i = 1, #p do
                t = modif(t + z * p[i])
                z = modif(z * x)
            end
            return t
        end,
        
        -- clear leading zeroes. Operates in place.
        clr = function(a)
            local degree = #a
            for i = degree, 1, -1 do
                if degree == i and a[i] == 0 then
                    degree = degree - 1
                    a[i] = nil
                end
            end
            if a[1] == nil then
                a[1] = 0
                assert(#a == 1, "Lua error checking degree.")
            else
                assert(#a == degree, "Lua error checking degree.")
            end
        end,

        -- reduce modulo modpoly, if set. Operates in place.
        reduce = function(a)
            if modpoly and #a >= #modpoly then
                for i = #a, #modpoly, -1 do
                    if a[i] ~= 0 then
                        -- power by which we need to scale modpoly
                        -- to clear out a[i]
                        local p = i - #modpoly
                        local s = modpoly
                        local x = POLY.new{0,1}
                        for j = 1, p do s = POLY.__mul(s, x, true) end
                        local coeff = (a[i] * modinv(modpoly[#modpoly])) % modulus
                        for j = 1, i do
                            a[j] = modif(a[j] - coeff * s[j])
                        end
                        assert(a[i] == 0, "Gauss elimination failed, what are you doing?")
                    end 
                end
                POLY.clr(a)
            end
        end,

        __add = function(a, b)
            if POLY.is(a) then
                local r = {}
                if POLY.is(b) then
                    local u, v
                    for i = 1, math.max(#a, #b) do
                        if i <= #a then u = a[i] else u = 0 end
                        if i <= #b then v = b[i] else v = 0 end
                        table.insert(r, modif(u+v))
                    end
                elseif type(b) == "number" and b == math.floor(b) then
                    -- adding poly and integer
                    for i = 1, #a do r[i] = modif(a[i]) end
                    r[1] = modif(r[1] + b)
                else
                    assert(false, "Polynomial addition requires polynomial or integer.")
                end
                POLY.clr(r)
                r._type = "poly"
                setmetatable(r, POLY)
                return r
            elseif POLY.is(b) then
                return POLY.__add(b, a)
            else
                assert(false, "Not a polynomial.")
            end
        end,

        -- unary minus
        __unm = function(a)
            assert(type(a) == "table" and a._type == "poly",
                "Not a polynomial.")
            local r = {}
            for _, v in ipairs(a) do
                table.insert(r, modif(-v))
            end
            r._type = "poly"
            setmetatable(r, POLY)
            return r
        end,

        __sub = function(a, b)
            return POLY.__add(a, -b) 
        end,

        __mul = function(a, b, noreduce)
            if POLY.is(a) then
                --
            elseif POLY.is(b) then
                return POLY.__mul(b, a)
            else
                assert(false, "Not a polynomial")
            end
        
            local r = {}
            
            function update(deg, val)
                if r[deg] then
                    r[deg] = modif(r[deg] + val)
                else
                    r[deg] = modif(val)
                end
            end

            if POLY.is(b) then
                for i = 1, #a do
                    for j = 1, #b do
                        update(i+j-1, a[i]*b[j])
                    end
                end
            elseif type(b) == "number" and math.floor(b) == b then
                for i = 1, #a do
                    update(i, b * a[i])
                end
            else
                assert(false, "Multiplying poly with neither poly nor int")
            end
                
            POLY.clr(r)
            -- this flag stops us from infinite recursion as reduce itself uses mul
            if not noreduce then POLY.reduce(r) end
            r._type = "poly"
            setmetatable(r, POLY)
            return r
        
        end,

        __pow = function(a, b)
            assert(type(a) == "table" and a._type == "poly", "not a polynomial")
            assert(type(b) == "number", "can only exponentiate with numbers")
            assert(b == math.floor(b), "can only exponentiate with integers")
            assert(b >= 0, "can only exponentiate with positive integers")
            if b == 0 then  
                return poly {1}
            else
                local t = poly {1}
                for i = 1, b do
                    t = t * a
                end
                return t
            end
        end,

        __eq = function(a,b)
            if type(a) == "table" and a._type == "poly" and
               type(b) == "table" and b._type == "poly"
            then
                if #a ~= #b then return false end
                for i = 1, #a do if a[i] ~= b[i] then return false end end
                return true
            else
                error("poly = only works on polynomials")
            end
        end,

        __tostring = function(a)
            assert(type(a) == "table" and a._type == "poly",
                "Not a polynomial.")
            local items = {}
            for i, v in ipairs(a) do
                if v ~= 0 then
                    local power = ""
                    if i > 2 then 
                        power = " X^" .. (i-1)
                    elseif i == 2 then
                        power = " X"
                    end
                    table.insert(items, v .. power)
                end
            end
            if #items == 0 then
                return "(0)"
            else
                return '(' .. table.concat(items, " + ") .. ')'
            end
        end
    }
    
    MATRIX = {
        is = function(x)
            return type(x)=="table" and x._type=="matrix"
        end,
        rows = function(x)
            assert(MATRIX.is(x), "not a matrix")
            return #x
        end,
        cols = function(x)
            assert(MATRIX.is(x), "not a matrix")
            return #x[1]
        end,
        -- function name based on perl
        bless = function(m)
            m._type = "matrix"
            setmetatable(m, MATRIX)
        end,
        
        -- matrix addition on same dimensions
        __add = function(a, b)
            assert(MATRIX.is(a) and MATRIX.is(b), "not both matrices")
            assert(MATRIX.rows(a) == MATRIX.rows(b) and
                   MATRIX.cols(a) == MATRIX.cols(b),
                   "matrix add requires same dimensions")
            local m = {}
            for i = 1, MATRIX.rows(a) do
                local row = {}
                for j = 1, MATRIX.cols(a) do
                    table.insert(row, modif(a[i][j] + b[i][j]))
                end
                table.insert(m, row)
            end
            MATRIX.bless(m)
            return m
        end,
        
        -- identity matrix with r rows, c cols
        identity = function(r, c)
            local m = {}
            for i = 1, r do
                local row = {}
                for j = 1, c do
                    if r == c then
                        table.insert(row, 1)
                    else
                        table.insert(row, 0)
                    end
                end
                table.insert(m, row)
            end
            MATRIX.bless(m)
            return m
        end,
        
        -- matrix multiplication
        __mul = function(a, b)
            assert(MATRIX.is(a) and MATRIX.is(b), "not both matrices")
            assert(MATRIX.cols(a) == MATRIX.rows(b), "dimension mismatch")
            local m = {}
            for i = 1, MATRIX.rows(a) do
                local row = {}
                for j = 1, MATRIX.cols(b) do
                    local el = 0
                    for k = 1, MATRIX.cols(a) do
                        el = modif(el + a[i][k] * b[k][j])
                    end
                    table.insert(row, el)
                end
                table.insert(m, row)
            end
            MATRIX.bless(m)
            return m
        end,
        
        -- currently fixes 2 characters per entry
        __tostring = function(m)
            local output = {}
            for row = 1, MATRIX.rows(m) do
                local s = '['
                for col = 1, MATRIX.cols(m) do
                    if col > 1 then s = s .. ' ' end
                    if m[row][col] < 10 then s = s .. ' ' end
                    s = s .. m[row][col]
                end
                s = s .. ']'
                table.insert(output, s)
            end
            return table.concat(output, '\n')
        end
    }

-- END internal stuff --

-- Polynomial constructor. Use: `p = poly{1, 3, 2}` to make
-- p represent the polynomial (1 + 3X + 2X^2). Note the use
-- of curly instead of round braces.
function poly(p)
    assert(type(p)=="table", "poly() requires a table of coefficients")
    local r = {}
    for _, v in ipairs(p) do
        assert(type(v)=="number", "poly() coefficients must be integers")
        table.insert(r, modif(v))
    end
    r._type = "poly"
    setmetatable(r, POLY)
    return r
end

-- use X as a polynomial
X = poly {0, 1}

-- matrix constructor, takes list of lists
function matrix(m)
    assert(type(m)=="table", "not a table")
    assert(m[1] and type(m[1]) == "table", "not a nested table")
    local rows = #m
    local cols = #m[1]
    local mat = {}
    for i = 1, rows do
        assert(#m[i] == cols, "not all rows same length")
        table.insert(mat, m[i])
    end
    MATRIX.bless(mat)
    return mat
end

function mat_inv(m)
    assert(MATRIX.is(m), "not a matrix")
    assert(modulus, "mat_inv only works with modulus set")
    
    -- for debugging
    function print_matrix(a)
        for i = 1, #a do
            print('[' .. table.concat(a[i], ", ") .. ']')
        end
    end
    
    -- can be used inline in an expression
    local function case(c, a, b)
        if c then return a else return b end
    end
    
    local n_rows = #m
    local n_cols = #(m[1])
    assert(n_rows == n_cols, "matrix not square")
    local n = n_rows
    
    -- create the double-width matrix
    local M = {}
    local row
    local col
    for row = 1, n do
        local item = {}
        for col = 1, 2*n do
            if col <= n then
                table.insert(item, m[row][col])
            else
                table.insert(item, case(row+n==col,1,0))
            end
        end
        table.insert(M, item)
    end
    
    print_matrix(M)
    
    -- general-purpose linear row operation
    -- row[r3] = c1 * row[r1] + c2 * row[r2]
    -- this function is basically magic
    local function A(c1, r1, c2, r2, r3)
        for col = 1, 2*n do
            M[r3][col] = (c1 * M[r1][col] + c2 * M[r2][col]) % modulus
        end
        print()
        print("row" .. r3 .. " = " .. c1 .. " x row" .. r1 ..
              " + " .. c2 .. " x row" .. r2)
        print_matrix(M)
    end
    
    -- clear below the diagonal
    for col = 1, n do
        -- swap if there's a zero in M[row][col]
        if M[col][col] == 0 then
            local is_zero = 1
            for row = col+1, n do
                if M[row][col] ~= 0 then
                    -- swap ri/rj by ri = ri + rj; rj = ri - rj; ri = ri - rj
                    A(1, row, 1, col, row)
                    A(1, row, modulus-1, col, col)
                    A(1, row, modulus-1, col, row)
                    is_zero = 0
                end
            end
            assert(is_zero == 0, "column of zeroes found, singular matrix")
        end
        -- set the diagonal element to 1
        print()
        print("Setting diagonal element " .. col .. " to 1.")
        A(0, 1, modinv(M[col][col]), col, col)
        -- and clear the rows below
        for row = col + 1, n do
            if M[row][col] ~= 0 then
            print()
                print("Clearing row " .. row .. " col " .. col)
                A(1, row, modulus - M[row][col], col, row)
            end
        end
    end
    -- clear above the diagonal
    for col = 2, n do
        for row = 1, col-1 do
            if M[row][col] ~= 0 then
                print()
                print("Clearing row " .. row .. " col " .. col)
                A(1, row, modulus - M[row][col], col, row)
            end
        end
    end
    
    -- check we've got it right and create the result matrix
    local r = {}
    for row = 1, n do
        local item = {}
        for col = 1, n do
            assert(M[row][col] == case(row==col,1,0), "error at " .. row .. "," .. col)
            table.insert(item, M[row][col + n])
        end
        table.insert(r, item)
    end
    MATRIX.bless(r)
    return r
end
