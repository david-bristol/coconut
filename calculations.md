Calculations
============

CNuT is about calculating things. While a "traditional" unit on group theory would involve proving every step from the axioms upwards, your focus for this unit should be on learning and practicing the calculations.

See also the separate notes on

  * [Working with probabilities](probabilities.md)
  * [Logarithms](logarithms/logarithms.md)

*Extra information on the exam will appear here later in the term.*

You should study the following for the exam:

## Groups and subgroups

  * Add, subtract and invert modulo an integer n.
  * The meaning of "order" of a group or of an element, and how to compute it.
  * Permuatations - how to compose, invert and convert to/from cycle notation.
  * What a subgroup is, and how to check if a subset is one.
  * What a generator is, how to find one (or check if it exists) and what the `<g>` notation means where g is an element in some group.
  * Simple proofs in group theory, as done in the lectures and in the script (excluding "diamond" sections).

Proofs of the kind discussed in the lectures may appear in the exam.
Examples of this include why `x+a = x+b` implies `a=b`, or showing that something is an equivalence relation.

## Rings and polynomials

  * Multiply and (where possible) divide modulo an integer n.
  * The meaning of "unit" and "zero-divisor" and how to classify elements in a ring as one of four cases (zero, unit, zero-divisor, neither).
  * Compute Euler's totient (phi) function.
  * Perform exponent arithmetic, e.g. (a^b mod n).
  * Divide polynomials with remainder modulo an integer n.
  * Define and compute the characteristic of a ring.

Simple proofs may appear in the exam, such as when `ax=ay` implies `x=y` and why (it's not the same as for `a+x=a+y`).

## Finite fields

  * Perform arithmetic modulo a polynomial, in particular reducing higher-degree polynomials.
  * Understand the notation `GF(p^n)`.
  * Check if a polynomial of degree 2 or 3 is irreducible; find such polynomials.
  * Add, subtract, multiply and divide (except by 0) in `GF(p^n)`, including writing down the operation tables and finding the explicit multiplication formula,
  * Run Euclid's algorithm (extended version), compute the gcd of integers or polynomials and solve equations of the form `ax+by = c` for x, y where a, b, c are all integers or polynomials.
  * Compute the Frobenius map and the other automorphisms in a finite field; know how many automorphisms there are in GF(p^n).
  * State for which integers n there can be a field with n elements.
 
## Linear algebra

  * Do arithmetic in vector spaces over finite fields.
  * Check if vectors are linearly independent.
  * Evaluate matrix-vector products and find the inverse of a given matrix.
  * Solve linear equation systems over finite fields.
  * Write down Vandermonde matrices for given points and dimensions.
  * Interpolate and evaluate polynomials in a finite field.
  * Interpret polynomials as vectors and finite fields `GF(p^n)` as vector spaces over `GF(p)`.
  * Convert a linear function on a finite field (such as the Frobenius map) between polynomial and matrix notation.

## Coding Theory

  * Understand the difference between a code and an encoding.
  * Understand the minimum distance of a code and how it relates to error detection/correction.
  * Compute the Hamming distance of two words and of a block code.
  * Use the Hamming distance of a linear code to determine how many errors a code can correct or detect.
  * Work with repetition codes and checksum codes.
  * Write a generator matrix for a checksum code given as a function.
  * Encode with a generator matrix.
  * Convert a generator matrix to systematic form.
  * Compute a parity-check matrix from a systematic generator matrix and use it to check if a word is in a code or not.
  * Encode, decode and check for errors with the (7, 4) Hamming code.
  * Encode with Reed-Solomon codes, including creating the generator matrix.

The ISBN, UPC (barcode) and Luhn codes will not appear in the exam, nor will the topic on detecting transpositions (as opposed to single symbol errors).
  
The Singleton bound, Hamming bound and Gilbert-Varshamov bound will not appear in the exam.

Generalised Hamming codes will not feature in the exam.

## Probability

Probability theory is not a core topic in this unit, but for the information theory part you should be able to do the following,
based on what you were taught in the first-year probability unit:

  * Compute probabilities of events from a given distribution.
  * Create joint, conditional and marginal probability tables for two events or random variables.
  * Determine if two events or random variables are independent.
  * Use the total probability theorem and Bayes' theorem to compute probabilities in multi-stage experiments.
  * Compute the output distributions of simple randomised algorithms.
  * Understand what a random variable is, and the difference between an event and a random variable.

## Information Theory
  
  * Encode and decode with variable-length codes.
  * Draw a decoding tree from a table of an encoding function.
  * Identify if a code is prefix-free.
  * Compute logarithms to base 2 and the functions `h(x)` and `h2(x)`. 
  * Compute the information content of an event and the entropy of a random variable.
  * Compute conditional entropies and mutual information between random variables.
  * Construct a Huffman code for a given probability distribution.
  * Convert descriptions of channels between diagram and conditional probability table forms.
  * Compute the output distribution and error probability of a channel given an input distribution and the channel characteristics.
  * Compute the joint and conditional distributions for simple channels.
  * Possibly in "Part 2": compute the capacity of a channel, where this is possible with the material in this unit.
  * Be able to define the binary symmetric and erasure channels (with parameter p).

A table of logarithms to base 2 for the integers 1-40 will be provided on the exam sheet.

You do not need to know about the Morse, Baudot or UTF-8 codes nor about the distribution and entropy of the English language for the exam.

## Cryptography

Cryptography is very exciting, but if it appears in the CNuT exam it will only be as an example of some other topic from CNuT.
Crypto A in year 3 will teach you cryptography as a subject in its own right.

## Bees

🐝 BEES!
