Unit materials and schedule of lectures
=======================================

The unit script is available at [PDF/coconut-all.pdf](https://gitlab.com/david-bristol/coconut/raw/master/PDF/coconut-all.pdf?inline=false).
You can read the relevant parts of the script before or after the corresponding lecture, depending on how you prefer to learn.
Each numbered section of the script is roughly one lecture and the main unit content is in sections 1 to 18.

Before starting information theory, we will have a refresher lecture on probability theory; at the very end if time allows
there will be a bonus lecture on applications in cryptography.

The exercise sheets are stored in the folder [PDF/](PDF/). Please also see the
page on [exercises, assessment, coursework and exams](assessment.md) for more
guidance on the exercises.

Schedule of lectures
--------------------

For the 2019-20 academic year:

  * Lecture 1: Tuesdays, 4-5pm in FRY LG.02
  * Lecture 2: Thursdays, 4-5pm in FRY LG.02
  * Labs will be on Thursdays between 11 and 1. Your assigned lab place
    and time will appear in your individual timetable.

| Week     | Lecture 1          | Lecture 2          |
|----------|--------------------|--------------------|
| Week 1   | Groups             | Modular Arithmetic |
| Week 2   | Rings              | Fields & Euclid    |
| Week 3   | Polynomials        | Finite fields      |
| Week 4   | Vector spaces      | Linear algebra     |
| Week 5   | Homomorphisms      | Field isos         |
| Week 6   | Coding theory      | Hamming weight     |
| Week 7   | Linear codes       | Reed-Solomon codes |
| Week 8   | _Explore week_     | _no lectures_      |
| Week 9   | Information theory | Probabilities      |
| Week 10  | Entropy            | Source coding      |
| Week 11  | Channel coding     | Cryptography       |
| Week 12  | _Revision week_    | _no lectures_      |

The material is roughly grouped into 5 weeks of Number Theory and algebra
and 5 weeks of coding and information theory.

If we lose a lecture due to a fire alarm or similar, the last lecture of
week 11 will be used as a back-up. New for 2019-20, there is no new content
in any of the units in week 12 which is a revision week: I will still be
around for the lecture hours, but they will become drop-in sessions.

Exercise solutions
------------------

Links to the solutions to the exercise sheets will appear on blackboard as the term
progresses. See [this page](https://www.ole.bris.ac.uk/webapps/blackboard/content/listContentEditable.jsp?content_id=_4118518_1&course_id=_237258_1&mode=reset)
for links.
