> What man is there that knoweth not how to go about
> doing arithmetic on polynomials ?

(The result of [training a Markov Chain](http://kingjamesprogramming.tumblr.com/) on a combination of [KJV](https://en.wikipedia.org/wiki/KJV) and [SICP](https://mitpress.mit.edu/sicp/full-text/book/book.html).)


CNuT
====

This repository contains the sources and PDFs for the unit COMS20002
"Communication and Number Theory" or 
[CNuT](https://en.wikipedia.org/wiki/Cnut_the_Great) for short. 

You are currently viewing the CNuT materials for the 2019-20 academic year.
This may be the last time that the unit runs in its current form.

A long time ago, this unit also contained some complexity theory and was known as
CoCoNuT.

CNuT is one of the more mathsy units - aimed at computer scientists who enjoyed
the formal parts of Theory of Computation. While not a formal prerequisite (at
the time of writing), CNuT is highly recommended for single-honours
computer science students wishing to take Cryptography A/B in their third year.

The lecturing style of this unit is that I write on the blackboard - there
are no slides. A PDF version of the complete lecture notes will be available
at the start of term, but I cannot guarantee that replay (lecture recording)
will be available - in most lecture theatres it does not record the blackboard.

Most courses on Number Theory are centered around proofs, developing the entire
material from a set of axioms and then asking students to re-prove some of the
steps in the exams. CNuT takes a different approach: the focus is on computing
things, sometimes by hand and sometimes with the help of a computer.

CNuT aims to give students a practical introduction to topics in coding and
number theory. Covering the same material 'properly', that is with proofs of
all statements, would take around 40CP at the very least; some of the material
here does not appear until 3rd or 4th year in the Maths department.

For these reasons, CNuT in 2019-20 is aimed only at single-honours CS students,
*not* joint honours Maths/CS students. If you are doing Maths/CS, you should
take the Maths units instead that cover the same material.

Compiled PDF scripts for the unit and exercise sheets are in subdirectory
`PDF/` of this repo at
https://gitlab.com/david-bristol/coconut/tree/master/PDF .

For the lecture notes, you can also use this direct link: https://gitlab.com/david-bristol/coconut/raw/master/PDF/coconut-all.pdf?inline=false

Please see the following pages for more information:

  * [Unit materials and schedule of lectures](schedule.md)
  * [Calculations you should learn and revise](calculations.md)
  * [Exercises, assessment, coursework, exams](assessment.md)

Additional notes
================

There are also notes on 

  * [Probability theory](probabilities.md)
  * [Using lua as a calculator for polynomials and other things](lua.md)
