# Working with probabilities

Many standard questions ask for some kind of relationship between two random
variables, e.g. how likely is it that X was 1 given that Y is 2.

If you have the joint distribution of the random variables available, the first
thing to do is usually create a table of it, with the marginal distributions
in an extra row resp. column. The following example should be read as the first
variable (X) defining the rows and the second (Y) the columns, so for example
P(X=1 and Y=0) = 1/2.

```
p_XY  |   0     1 |
-----------------------
   0  | 1/8   1/4 | 3/8
   1  | 1/8   1/2 | 5/8
-----------------------
      | 1/4   3/4 |
```

Note that all entries in the table itself (not including the margins) must sum
to one. The marginal entries are the sums of the row resp. column that they
stand on.

For some further calculations, it is useful to have all fractions in the same
denominator. If this is easy to do, you can just write the numerators in the
table itself - the convention is then to place the denominator after a / sign
in the bottom right corner. Here is the same table as above with this convention:

```
p_XY  |   0     1 |
---------------------
   0  |   1     2 |  3
   1  |   1     4 |  5
----------------------
      |   2     6 | /8
```

To compute the conditional distribution X|Y, divide each entry in the table by
the sum of all entries in its column. This is where it is particularly useful
to have the denominator in the bottom right, as you can then simply ignore it.

```
p_X|Y |   0     1
------------------
    0 | 1/2    1/3
    1 | 1/2    2/3
```

You will get different denominators and it is rarely worth normalising them.
The sum of each column must still be one, but the row-sums can be whatever
they want to be.

To compute the conditional Y|X, where you are conditioning on the variable that
used to come first, there is an extra step. Because the convention is that you
use the first variable for the rows, you should transpose the joint
distribution table and then proceed as for the X|Y conditional.

```
p_Y|X |   0     1
-----------------
    0 | 1/3   1/5
    1 | 2/3   4/5
```

If you do not have the joint distribution to start with, you will often have
one marginal distribution and the matching conditional probability distribution.
For example:

```
p_Y|X |   0     1        p_X
-----------------        ----------
    0 | 1/3   1/5          0 |  3/8
    1 | 2/3   4/5          1 |  5/8
```

In this case, because `p_YX = p_X p_Y|X` we multiply each entry in the
conditional distribution with the marginal one in the correct row or column.
In this case, the conditional table has values of X in the columns, so the
first column needs to be multiplied with the first entry of `p_X`.

This gets you `p_YX` (which you can transpose if you want `p_XY`).

# Entropies

Two important formulas are `h(x) = - x * log2(x)` with `h(0) = 0` and
`h2(x) = h(x) + h(1-x)`. The entropy of a binary random variable (that is, one
that can take one of exactly two values) is `H(X) = h2(p)` where `p` is the
probability of either of the two events.

If your calculator only does logs to base 10 but not base 2, use
`log2(a) = log10(a)/log10(2)` or just divide by the constant
`log10(2) = 0.6931472`.

A safe way to proceed is to write out the probability tables as above and then
to compute the entropies from there. This often involves adding extra rows or
columns to the margins to hold the `h(.)` values. For example, to compute
`H(X|Y=1)` in the example above you could add a column to the `p_X|Y` table
then sum its values:

```
p_X|Y |   0     1  | h(p_X|Y(x, 1))
-------------------|----------------
    0 | 1/2    1/3 | h(1/3) = 0.5283
    1 | 1/2    2/3 | h(2/3) = 0.3900
------------------------------------
                    H(X|Y=1)  0.9183
```

For computing mutual information `I(x,y)`, one can make a table of the same
dimensions as the joint probability table and record the entries
`pXY log2 (pXY / (pX pY))` in that, then sum the whole table.

At this point, the use of spreadsheet software may be useful to visualise the
whole process.

