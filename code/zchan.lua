-- data for Z-channel capacity plot

function l(x) return math.log(x)/math.log(2) end

function h(x)
    return -x*l(x) - (1-x)*l(1-x)
end

function c(x, y)
    return h((1-x)*(1-y)) - (1-x)*h(y)
end

STEP = 1/15
x=0
y = 0.5
while x <= 1 do
    print('(' .. x .. ',' .. c(x, y) .. ')')
    x = x + STEP
end

