/* Search for isos of GF(2^8). */

#include <stdio.h>
#include <string.h>

typedef unsigned char u8;

u8 mulm(u8 a, u8 b, u8 m) {
    u8 r = 0x00;
    int carry = 0;
    while (a) {
        if (a & 0x01) { r ^= b; }
        carry = b & 0x80;
        b <<= 1;
        a >>= 1;
        if (carry) { b ^= m; }
    }
    return r;
}

/* set up a function table for f with f(x) = fx. */
void make_table(u8 fx, u8 m, u8 table[8]) {
    u8 t = fx;
    int i;

    table[0] = 1;
    table[1] = fx;
    for (i = 2; i < 8; i++) {
        t = mulm(t, fx, m);
        table[i] = t;
    }
}

/* Map an element x with the function described by the table f. */
u8 map(u8 x, u8 f[8]) {
    u8 r = 0x00;
    int i;

    for (i = 0; i < 8; i++) {
        if (x & (0x01<<i)) { r ^= f[i]; }
    }
    return r;
}

void display(u8 x, char s[255]) {
    static const char init[255] =
    /*   0123456789ABCDEF ... */
        "  X^7 + X^6 + X^5 + X^4 + X^3 + X^2 + X   + 1   ";
    int i;
    int j;

    strcpy(s, init);
    for (i = 0; i < 8; i++) {
        if (!(x & (0x01 << (7 - i)))) {
            for (j = 6 * i; j < 6*i + 6; j++) { s[j] = ' '; }
        }
    }
}

void scan(u8 p, u8 q) {
    u8 c = 0x00;
    u8 f[8];

    u8 t, u, x, y;

    char s[255];

    while(1) {
        make_table(c, q, f);

        /* Scan through all pairs of elements and check for mismatches. */
        t = 0x00;
        u = 0x00;

        while (1) {
            while (1) {
                x = mulm(t, u, p);
                x = map(x, f);
                y = mulm(map(t, f), map(u, f), q);

                if (x != y) {
                    printf("c = %02x failed for %02x * %02x.\n", c, t, u);
                    t = 0x01; /* block success code below */
                    goto T_LOOP;
                }

                if (!(++u)) { goto U_LOOP; }
            }

            U_LOOP:
            if (!(++t)) { goto T_LOOP; }
        }
        T_LOOP:

        if (!t && !u) {
            /* if both counters are back at 0 we have a success */
            display(c, s);
            printf("c = %02x succeeded : f(X) = %s.\n", c, s);
        }

        if (!(++c)) { break; }
    }
}

int all_zero(u8 p[8]) {
    int r = !(p[0] && p[1] && p[2] && p[3] && p[4] && p[5] && p[6] && p[7]);
    return r;
}

/*
Divide a linear factor out of a polynomial in GF(2^8) represented by the
irreducible polynomial m.
p - the polynomial coefficients. Modified in place.
q - the linear factor (dividing by (X + q)).
r - the result polynomial (to be written).
m - the modulus.
 */
void polydiv(u8 p[8], u8 q, u8 r[8], u8 m) {
    int i;

    /* x^8 coeff. */
    r[0] = 0x01;
    p[0] ^= q;

    /* Coeffs 7 to 1 */
    for (i = 0; i < 7; i++) {
        r[i+1] = p[i];
        p[i] = 0x00;
        p[i+1] ^= mulm(q, r[i+1], m);
    }
    /* any remainder is in p[7] */

    return;
}

int main(int argc, char **argv) {

    /*
    u8 p = 0x1b;
    u8 q = 0x1d;

    scan(p, q);
    */

/*
    int i;
    u8 t0 = 0x04;
    u8 t = t0;
    char s[255];
    for (i = 0; i < 16; i++) {
        display(t, s);
        printf("i=%2i  t=%s\n", i, s);
        t = mulm(t, t, q);
    }
*/

    u8 p[8] = { 0x00, 0x00, 0x00, 0x01, 0x01, 0x00, 0x01, 0x01 }; // AES poly
    u8 q = 0x03; // X + 1;
    u8 m = 0x1d; // target poly.
    u8 r[8];
    int i;
    char s[255];

    polydiv(p, q, r, m);
    printf("Polydiv output.\n");
    for (i = 0; i < 8; i++) {
        display(p[i], s);
        printf("p[%i] = %02x / %s\n", i, p[i], s);
    }
    for (i = 0; i < 8; i++) {
        display(r[i], s);
        printf("r[%i] = %02x / %s\n", i, r[i], s);
    }

    printf("Done.\n");

    return 0;
}

