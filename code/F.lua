-- lua functional tools

local F = {}

F.map = function(f, list)
    local t = {}
    for _, v in ipairs(list) do table.insert(t, f(v)) end
    return t
end

F.map2 = function(f, x, y)
    local t = {}
    assert(#x == #y, 'map2: lists must be same length.')
    for i = 1, #x do
        table.insert(t, f(x[i], y[i]))
    end
    return t
end

F.each = function(f, list)
    for _, v in ipairs(list) do f(v) end
end

F.collect = function(f, init, list)
    local t = init
    for _, v in ipairs(list) do t = f(t, v) end
    return t
end

F.ntimes = function(x, n)
    local t = {}
    for i = 1, n do
        table.insert(t, x)
    end
    return t
end

F.id = function(x) return x end

return F

