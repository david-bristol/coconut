-- matrix multiplication in lua
-- set the modulus constant before doing any operations

modulus = 5

function mul(a, b)
    local n_rows = #a
    local n_k = #(a[1])
    local n_cols = #(b[1])
    assert(#b == n_k, "size mismatch")

    local m = {}
    for row = 1, n_rows do
        local d = {}
        for col = 1, n_cols do
            local s = 0
            for k = 1, n_k do
                s = (s + a[row][k] * b[k][col]) % modulus
            end
            table.insert(d, s)
        end
        table.insert(m, d)
    end
    return m
end

function print_matrix(a)
    for i = 1, #a do
        print('[' .. table.concat(a[i], ", ") .. ']')
    end
end

-- invert a matrix with Gauss elimination
function inv(m)
    local function modinv(n)
        for i = 1,modulus-1 do
            if ((i*n) % modulus) == 1 then return i end
        end
        assert(false, "no inverse for " .. n)
    end
    
    -- can be used inline in an expression
    local function case(c, a, b)
        if c then return a else return b end
    end
    
    local n_rows = #m
    local n_cols = #(m[1])
    assert(n_rows == n_cols, "matrix not square")
    local n = n_rows
    
    -- create the double-width matrix
    local M = {}
    local row
    local col
    for row = 1, n do
        local item = {}
        for col = 1, 2*n do
            if col <= n then
                table.insert(item, m[row][col])
            else
                table.insert(item, case(row+n==col,1,0))
            end
        end
        table.insert(M, item)
    end
    
    print_matrix(M)
    
    -- general-purpose linear row operation
    -- row[r3] = c1 * row[r1] + c2 * row[r2]
    local function A(c1, r1, c2, r2, r3)
        for col = 1, 2*n do
            M[r3][col] = (c1 * M[r1][col] + c2 * M[r2][col]) % modulus
        end
        print()
        print_matrix(M)
    end
    
    -- clear below the diagonal
    for col = 1, n-1 do
        -- swap if there's a zero in M[row][col]
        if M[col][col] == 0 then
            local is_zero = 1
            for row = col+1, n do
                if M[row][col] ~= 0 then
                    -- swap ri/rj by ri = ri + rj; rj = ri - rj; ri = ri - rj
                    A(1, row, 1, col, row)
                    A(1, row, modulus-1, col, col)
                    A(1, row, modulus-1, col, row)
                    is_zero = 0
                end
            end
            assert(is_zero == 0, "column of zeroes found, singular matrix")
        end
        -- set the diagonal element to 1
        A(0, 1, modinv(M[col][col]), col, col)
        -- and clear the rows below
        for row = col + 1, n do
            if M[row][col] ~= 0 then
                A(1, col, modulus - M[row][col], row, row)
            end
        end
    end
    -- clear above the diagonal
    for col = 2, n do
        for row = 1, col-1 do
            if M[row][col] ~= 0 then
                A(1, row, modulus - M[row][col], col, row)
            end
        end
    end
    
    -- check we've got it right and create the result matrix
    local r = {}
    for row = 1, n do
        local item = {}
        for col = 1, n do
            assert(M[row][col] == case(row==col,1,0), "error at " .. row .. "," .. col)
            table.insert(item, M[row][col + n])
        end
        table.insert(r, item)
    end
    return r
end

A = { {1, 3}, {2, 3}, {4, 1} }
B = { {1, 1, 1, 1}, {1, 2, 3, 4} }

C = mul(A, B)

-- print_matrix(A)
-- print()
-- print_matrix(B)
-- print()
-- print_matrix(C)


T = { {1, 4, 0, 0},
      {0, 1, 0, 0},
      {0, 0, 1, 0},
      {0, 0, 0, 1} }

T2 = { {1, 0, 0, 0},
       {4, 1, 0, 0},
       {0, 0, 1, 0},
       {0, 0, 0, 1} }

S = { {2, 4, 0, 0},
      {4, 1, 0, 0},
      {0, 0, 1, 0},
      {0, 0, 0, 1} }

SS = {{1, 1, 0, 0},
      {1, 2, 0, 0},
      {0, 0, 1, 0},
      {0, 0, 0, 1}}

