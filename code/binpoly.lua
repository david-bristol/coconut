bit = require "bit"

function tobin(x)
    local n = x
    local s = ''
    while n ~= 0 do
        if bit.band(n, 1) == 1 then s = '1' .. s else s = '0' .. s end
        n = bit.rshift(n, 1)
    end
    if s == '' then s = '0' end
    return s
end


function pb(x)
    local n = x
    local s = ''
    while n ~= 0 do
        if bit.band(n, 1) == 1 then s = '1' .. s else s = '0' .. s end
        n = bit.rshift(n, 1)
    end
    if s == '' then s = '0' end
    print(bit.tohex(x), s)
end


function mulx(a)
    local x = bit.lshift(a,1)
    if x > 255 then
        x = x % 256
        x = bit.bxor(x, 0x1b)
    end
    return x
end

function mul(a, b)
    local n = 0
    local x = a
    local y = b
    while x > 0 do
        if bit.band(x,1) == 1 then
            n = bit.bxor(n, y)
        end
        y = bit.lshift(y, 1)
        if y > 255 then y = bit.bxor(y, 0x011b) end
        x = bit.rshift(x, 1)
    end
    return n
end

for i = 2, 255 do
    local j = 1
    local x = i
    while x > 1 do
        x = mul(x, i)
        j = j + 1
    end
    print(bit.tohex(i), tobin(i), j)
end

