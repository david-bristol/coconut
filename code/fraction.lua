-- arithmetic with fractions

META = {
    __add = function(a, b)
        if type(b) == 'number' then return frac(a.n + b * a.d, a.d) end
        if type(b) == 'table' then return frac(a.n * b.d + a.d * b.n, a.d * b.d) end
        assert(false, 'unknown operand')
    end,
    __mul = function(a,b)
        if type(b) == 'number' then return frac(a.n * b, a.d) end
        if type(b) == 'table' then return frac(a.n * b.n, a.d * b.d) end
        assert(false, 'unknown operand')
    end,
    __div = function(a,b)
        if type(b) == 'number' then return frac(a.n, a.d * b) end
        if type(b) == 'table' then return frac(a.n * b.d, a.d * b.n) end
        assert(false, 'unknown operand')
    end,
    __tostring = function(a)
        return a.n .. '/' .. a.d
    end
}

function gcd(a,b)
    while a ~= 0 do
        a, b = (b%a), a
    end
    return b
end

function frac(a,b)
    assert(b ~= 0, "divide by 0")
    local g = gcd(a,b)
    if g > 1 then
        a = math.tointeger(a / g)
        b = math.tointeger(b / g)
    end
    local t = { n = a, d = b }
    setmetatable(t, META)
    return t
end

-- main --


