-- Lagrange interpolation example over F_5

-- to implement modular arithmetic
function reduce(x) return x % 5 end
function inv(x) return reduce(x*x*x) end

-- vanilla map
function map(xs, f)
    local ys = {}
    for _, x in ipairs(xs) do
        table.insert(ys, f(x))
    end
    return ys
end

-- print a poly in the usual notation
function disp_poly(p)
    return p[1] .. '+' .. p[2] .. '.X+' .. p[3] .. '.X^2'
end

-- compute L_{i,n} coefficients for the given xs.
-- currently computes up to c_2:
-- c_0 = x_1 * ... * x_k (skip x_i)
-- c_1 = x_1 + ... + x_2 (skip x_i)
-- c_2 = 1
function lagrange_poly(n, xs, i)
    assert(n == 2, 'only n=2 implemented')
    local denom = 1
    local c0 = 1
    local c1 = 0
    local c2 = 1
    for k = 0, n do
        if i ~= k then
            denom = reduce(denom * (xs[i+1] - xs[k+1]))
            c0 = reduce(c0 * (-xs[k+1]))
            c1 = reduce(c1 - xs[k+1])
        end
    end
    return map({c0,c1,c2}, function(x) return reduce(x*inv(denom)) end)
end

-- Compute the Lagrange polynomials for a list of x values.
function lagrange(n, xs)
    local polys = {}
    for i = 0, n do
        table.insert(polys, lagrange_poly(n, xs, i))
    end
    return polys
end

-- inner product of a coefficient vector and some polys
function inner_product(ys, ps)
    assert(#ys == #ps, 'length mismatch')
    local acc = {0, 0, 0}
    for i = 1, #ys do
        for k = 1, #ps[i] do
            acc[k] = reduce(acc[k] + ys[i] * ps[i][k]) 
        end
    end
    return acc
end

-- Interpolate a polynomial from a list of points.
function interpolate(ps)
    local n = #ps - 1
    local xs = map(ps, function(pt) return pt[1] end)
    local ys = map(ps, function(pt) return pt[2] end)
    local Ls = lagrange(n, xs)
    for i = 0, n do
        print(disp_poly(Ls[i+1]))
    end
    local poly = inner_product(ys, Ls)
    print(disp_poly(poly))
end

-- main --

interpolate { {0,2}, {1,4}, {3,3} }

