
function mul1(a, b, c, d)
    return (a*c+b*d) % 7, (b*c+a*d-b*d) % 7
end

function mul2(a, b, c, d)
    return (a*c-b*d) % 7, (a*d+b*c) % 7
end

function f(x, y)
    return (x + 3 * y) % 7, (2 * y) % 7
end

for u = 0, 6 do
    for v = 0, 6 do
        for x = 0, 6 do
            for y = 0, 6 do
                -- f(u, v) ** f(x, y)
                local a, b = f(u, v)
                local c, d = f(x, y)
                local p, q = mul2(a, b, c, d)
                -- f((u, v) * (x, y))
                local g, h = mul1(u, v, x, y)
                local r, s = f(g, h)

                if p ~= r or q ~= s then
                    print(u, v, x, y, p, q, r, s)
                end
            end
        end
    end
end


for u = 0, 6 do
    for v = 0, 6 do
        local f = function(x, y) return (x+u*y) % 7, (v*y) % 7 end
        
        local check = function()
            for x = 0, 6 do
                for y = 0, 6 do
                    for z = 0, 6 do
                        for w = 0, 6 do
                            -- (x, y) * (z, w)
                            local p, q = mul1(x, y, z, w)
                            local xx, yy = f(x, y)
                            local zz, ww = f(z, w)
                            local r, s = mul1(xx, yy, zz, ww)
                            local a, b = f(p, q)
                            if a ~= r or b ~= s then
                                return false
                            end
                        end
                    end
                end
            end
            return true
        end
        if check() then
            print("iso at u=" .. u .. ", v=" .. v)
        end
    end
end

