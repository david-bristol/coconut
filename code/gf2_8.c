#include<stdbool.h>

typedef unsigned char u8;

u8 mul(u8 a, u8 b)
{
    u8 x, y, r;
    bool carry;
    x = a;
    y = b;
    r = 0x00;
    while (x)
    {
        if (x & 0x01) { r ^= y; }
        carry = y & 0x80;
        y <<= 1;
        x >>= 1;
        if (carry) { y ^= 0x1b; }
    }
    return r;
}

#include<stdio.h>
int main(int argc, char** argv)
{
    u8 x = 0x03;
    int i;
    for (i = 0; i < 256; i++)
    {
        x = mul(x, 0x03);
        printf("%02x\n", x);
    }
    return 0;
}


