-- GF(5)[X,Y]/(X^2-X+1,Y^3+Y-1)
-- G = GF(5^6) by X^6 = -X^4+X^3-X^2-2

F = require "F"

-- representation

function add(x,y) return F.map2(function(a,b) return (a+b) % 5 end, x, y) end

c6 = [3, 0, 4, 1, 4, 0]
c7 = [0, 3, 0, 4, 1, 4]
c8 = [2, 3, 1, 3, 2, 0]
c9 = [0, 2, 3, 1, 3, 2]
c10= [1, 2, 1, 3, 1, 0]

function s(l, x) return F.map(function(y) return (l * y) % 5 end, x) end

function mulG(x,y)
    local a0 = (x[1] + y[1]) % 5
    local a1 = (x[1]*y[2]+x[2]*y[1]) % 5
    local a2 = (x[1]*y[3]+x[2]*y[2]+x[3]*y[1]) % 5
    local a3 = (x[1]*y[4]+x[2]*y[3]+x[3]*y[2]+x[4]*y[1]) % 5
    local a4 = (x[1]*y[5]+x[2]*y[4]+x[3]*y[3]+x[4]*y[2]+x[5]*y[1]) % 5
    local a5 = (x[1]*y[6]+x[2]*y[5]+x[3]*y[4]+x[4]*y[3]+x[5]*y[2]+x[6]*y[1]) % 5
    local a6 = (x[2]*y[6]+x[3]*y[5]+x[4]*y[4]+x[5]*y[3]+x[6]*y[2]) % 5
    local a7 = (x[3]*y[6]+x[4]*y[5]+x[5]*y[4]+x[6]*y[3]) % 5
    local a8 = (x[4]*y[6]+x[5]*y[5]+x[5]*y[4]) % 5
    local a9 = (x[5]*y[6]+x[6]*y[5]) % 5
    local a10 = (x[6]*y[6]) % 5
    
    local r = [a0, a1, a2, a3, a4, a5]
    r = add(r, s(a6, c6))
    r = add(r, s(a7, c7))
    r = add(r, s(a8, c8))
    r = add(r, s(a9, c9))
    r = add(r, s(a10,c10))
    return r
end

-- basis 1, X, Y, XY, Y^2, XY^2 --
f = [
    [1, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 0],
    [0, 0, 0, 1, 0, 0],
    [0, 0, 0, 0, 1, 0],
    ]
]
-- TODO This "basis" is not symmetrical as u=2,v=2 -> X^2 but u=3,v=1 -> Y. --
function mulF(x, y)
    local r = [0, 0, 0, 0, 0, 0]
    for u = 1, 6 do for v = 1, 6 do
        r = add(r, s(x[u]*y[v], f[TODO]))
    end end
end

