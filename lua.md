# A lua calculator for CNuT-related calculations

If you have a machine with the lua language installed - and this includes most
Linux distributions - then you can use my lua coconut script as a little
"calculator" to help you with finite fields.

# Lua basics

Start the interpreter with `lua` on the command line. `Control-D` on a line on
its own exits the interpreter again.

Typing an expression prints its value to the terminal. `nil` is the null value.
For example (`>` is the prompt and everything after that on the same line is
what you type):

```
> 1 + 2
3
> a
nil
> a = 1
> a
1
```

In lua, an assignment is not an expression, so `a = 1` gives you the prompt back
immediately without printing any value.

# Loading the script

Download `cnut.lua` from the main directory of the repository. From the folder
in which you have placed this script, run `lua -i cnut.lua`. The `-i` means
"after running this command, go to interactive mode" and the command is to load
the script.

# Coding/Information Theory functions

  * `log(x)` computes the logarithm to base 2.
  * `h(x)` computes the entropy function `h(x) = - x log(x)` and deals with the
    special case `h(0) = 0` correctly.
  * `h2(x)` computes the binarly entropy function `h2(x) = h(x) + h(1-x)`.

# Polynomials

The function `poly` creates a polynomial object. It takes a lua table of
coefficients with the least significant one first, and due to how lua syntax
works it takes curly instead of round braces. So `a = poly {1, 2, 3}` stores
the polynomial `1 + 2X + 3X^2` into `a`.

The usual addition, subtraction and multiplication functions are defined on
polynomials (via operator overloading). The way overloading works, you
can do "polynomial + polynomial" and "polynomial + integer" and
"integer + polynomial", but "integer + integer" just gives you an integer:

```
poly{1} + poly{2} -- OK, gives polynomial
poly{1} + 2       -- OK, gives polynomial
2 + poly{1}       -- OK, gives polynomial
1 + 2             -- gives you an integer
```

Exponentiating a polynomial with a positive integer is also defined.
So you can do this:

```
> a = poly{1, 2}
> a
(1 + 2 X)
> a + 1
(2 + 2 X)
> a * a
(1 + 4 X + 4 X^2)
> a^2
(1 + 4 X + 4 X^2)
> a^3
(1 + 6 X + 12 X^2 + 8 X^3)
```

The variable `X` (capital X) is initialised to `poly {0, 1}` so this works too:

```
> X
(1 X)
> X * X
(1 X^2)
> X ^ 2
(1 X^2)
> X ^ 4
(1 X^4)
> (X^2 + X + 1)^2
(1 + 2 X + 3 X^2 + 2 X^3 + 1 X^4)
> (X^2 + 2) * (3*X + 1)
(2 + 6 X + 1 X^2 + 3 X^3)
```

# Polynomials modulo an integer

If you set the global variable `modulus`, all future polynomial operations work
modulo this number.

```
> a = poly {1, 2, 2}
> a
(1 + 2 X + 2 X^2)
> b = poly {2, 2}
> b
(2 + 2 X)
> a + b
(3 + 4 X + 2 X^2)
> modulus = 3
> a + b
(1 X + 2 X^2)
> -a
(2 + 1 X + 1 X^2)
```

# Polynomials modulo other polynomials

If you set the global variable `modpoly` to a polynomial, as well as setting the
`modulus`, then polynomial multiplication also reduces modulo the polynomial.

For example, here we operate in `GF(2^3)` with the modulus `X^3 + X + 1`.

```
> modulus = 2
> modpoly = poly{1, 1, 0, 1}
> X^3
(1 + 1 X)
> X^4
(1 X + 1 X^2)
> X^5
(1 + 1 X + 1 X^2)
> X^6
(1 + 1 X^2)
```

# Matrices

You can construct a matrix with

    a = matrix({{1,2},{2,1}})

where a matrix is represented as a list of lists (technically a "table of tables") and a list is written with curly braces.
If you print the above matrix (type `a` on its own in lua and press enter) you see it in its usual form:

    [ 1  2]
    [ 2  1]

The `+` operation works on matrices of the same size, and the `*` operation on compatible matrices:

```
> a=matrix({{1,2,3},{4,5,6}})
> a
[ 1  2  3]
[ 4  5  6]
> b=matrix({{1,1,1},{1,1,1}})
> b
[ 1  1  1]
[ 1  1  1]
> a+b
[ 2  3  4]
[ 5  6  7]
> c=matrix({{1,0},{0,1}})
> c
[ 1  0]
[ 0  1]
> c*a
[ 1  2  3]
[ 4  5  6]
```

The function `mat_inv` inverts a matrix, printing what it is doing to the terminal as it goes along. It only works if `modulus` is set.
It also returns the inverted matrix, so you can check by multiplying back again (recommended as I haven't tested it as much as I'd like).

```
> modulus=5
> a=matrix({{1,1,1},{2,4,1},{4,1,1}})
> b=mat_inv(a)
[1, 1, 1, 1, 0, 0]
[2, 4, 1, 0, 1, 0]
[4, 1, 1, 0, 0, 1]

Setting diagonal element 1 to 1.

row1 = 0 x row1 + 1 x row1
[1, 1, 1, 1, 0, 0]
[2, 4, 1, 0, 1, 0]
[4, 1, 1, 0, 0, 1]

Clearing row 2 col 1

row2 = 1 x row2 + 3 x row1
[1, 1, 1, 1, 0, 0]
[0, 2, 4, 3, 1, 0]
[4, 1, 1, 0, 0, 1]

Clearing row 3 col 1

row3 = 1 x row3 + 1 x row1
[1, 1, 1, 1, 0, 0]
[0, 2, 4, 3, 1, 0]
[0, 2, 2, 1, 0, 1]

Setting diagonal element 2 to 1.

row2 = 0 x row1 + 3 x row2
[1, 1, 1, 1, 0, 0]
[0, 1, 2, 4, 3, 0]
[0, 2, 2, 1, 0, 1]

Clearing row 3 col 2

row3 = 1 x row3 + 3 x row2
[1, 1, 1, 1, 0, 0]
[0, 1, 2, 4, 3, 0]
[0, 0, 3, 3, 4, 1]

Setting diagonal element 3 to 1.

row3 = 0 x row1 + 2 x row3
[1, 1, 1, 1, 0, 0]
[0, 1, 2, 4, 3, 0]
[0, 0, 1, 1, 3, 2]

Clearing row 1 col 2

row1 = 1 x row1 + 4 x row2
[1, 0, 4, 2, 2, 0]
[0, 1, 2, 4, 3, 0]
[0, 0, 1, 1, 3, 2]

Clearing row 1 col 3

row1 = 1 x row1 + 1 x row3
[1, 0, 0, 3, 0, 2]
[0, 1, 2, 4, 3, 0]
[0, 0, 1, 1, 3, 2]

Clearing row 2 col 3

row2 = 1 x row2 + 3 x row3
[1, 0, 0, 3, 0, 2]
[0, 1, 0, 2, 2, 1]
[0, 0, 1, 1, 3, 2]

> a*b
[ 1  0  0]
[ 0  1  0]
[ 0  0  1]
```
