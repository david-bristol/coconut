Irreducible polynomials of degree 2
-----------------------------------

There is a general theorem that if p is a prime number then for every degree n > 0, there exists an irreducible polynomial of degree n in `GF(p)[X]`. This shows that at a finite field with `p^n` elements exists, which is one half of the classification theorem.

In this page we look at the case n = 2. To start with,

  - All elements of degree 0 (that is, the field elements except 0) are units, since we are working over a field.
  - Since we are working over a field, the degree formula for multiplication of two polynomials A, B is `deg(AB) = deg(A) + deg(B)`. To be more precise, a field is an integral domain, and the polynomial ring over an integral domain remains an integral domain, so there are no zero-divisors that could spoil anything.
  - Recall that the definition of an irreducible polynomial P is a polynomial that is (1) not a unit and (2) if we write P=AB, then exactly one of the factors A and B is a unit.
  - Polynomials of degree 1 or greater over GF(p) cannot be units. If P were a unit, then there would exist a Q such that PQ = 1, but then `deg(1) = 0 = deg(P) + deg(Q)` which contradicts `deg(P) > 0` since Q cannot have a negative degree.
  - This shows that all polynomials of degree 1, that is `a+bX` where b is nonzero, are irreducible since writing such a polynomial as AB forces one of the two factors to have degree 1 and the other degree 0 - but the degree-0 one is then a unit.

This brings us to the case of degree 2. We are going to show that there is an irreducible polynomial of the form `a+bX+X^2` with quadratic coefficient 1. If we consider all polynomials of this form, there are `p^2` of them, each one described by the pair (a, b) of coefficients in GF(p).

If such a polynomial P is reducible, e.g. not irreducible, then we must be able to write P=AB in such a way that neither A nor B is a unit. They cannot both be units as that would make P a unit, therefore both A and B must have degree 1.

A degree-2 polynomial with two degree-1 factors and leading coefficient 1 can also be written (X+u)(X+v), so we can again enumerate all p^2 possibilities as pairs (u, v).

The point here is that two different (u, v) representations can give the same (a, b) one since `(X+u)(X+v) = X^2 + (u+v)X + uv`, so we have `b = u+v` and `a=uv` but these formulas are symmetric in the roles of u and v, so swapping the two does not change the outcome.

For example, u = 0, v = 1 gives `X(X+1) = X + X^2` but so does u=1, v=0.

In other words:

  - We can enumerate all reducible polynomials (of degree 2, with leading coefficient 1) by making a table of all p^2 possibilities for u and v.
  - But this gives us fewer than p^2 polynomials in total, since e.g. the combinations u=0, v=1 and u=1, v=0 give the same polynomial. So does any other pair where u and v are swapped.
  - Therefore there are fewer than p^2 distinct reducible polynomials of degree 2 with leading coefficient 1.
  - But there are exactly p^2 distinct polynomials (not necessarily reducible) of degree 2 with leading coefficient 1, namely all possible (a, b) combinations.
  - So at least one of these must be irreducible.
