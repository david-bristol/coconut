Fixed points
============

## 1.

Compare the following:

  * ` y = x^2 = x + 2` is an **explicit** definition of y in terms of x.
  * `x^2 + y^2 = 1` is an **implicit** definition of y in terms of x.

An explicit definition is nice because you can stick in x, calculate and get y out. An implicit definition is more powerful however: the second equation defines a circle, where y is not a function of x since there are points directly above each other like (0, 1) and (0, -1).

## 2. 

Consider the equation `X^2 - 2 = 0`. We know of course that the solutions are the square roots (positive and negative) of 2. However, what if we actually want to calculate this value?

To do this, we can use Newton's method. First, express the thing you want to calculate as a function so that the solution is zero of the function. In our case, the function is simply `f(x) = x^2 - 2` and we're looking for an x with `f(x) = 0`.

Newton's method approximates the solution with a sequence. Pick an initial value, such as `x_0 = 1`. Then repeatedly set `x_{i+1} = x_i - f(x_i)/f'(x_i)` where `f'` is the derivative of f, in this case `f'() = 2x`.

Run through a little script, this gives us

    x_0 = 1
    x_1 = 1.5
    x_2 = 1.4166666666667
    x_3 = 1.4142156862745
    x_4 = 1.4142135623747
    x_5 = 1.4142135623731
    x_6 = 1.4142135623731
    x_7 = 1.4142135623731
    x_8 = 1.4142135623731
    x_9 = 1.4142135623731

Newton's method converges quite fast: after 5 steps, the result is already as accurate as it's ever going to get in a double-precision floating point number.

How can we tell if we've hit a solution exactly, or how close we are? The answer is that if `x` is the exact solution, then `f(x) = 0`, and we can use `|f(x)|` as a measure of how close we are to the solution. If we compute `f(x_5)` above we get `1.42e-14`, so the error is already on the order of `10^{-14}`.

## 3.

We can rewrite Newton's approximation for the square root of 2 with a function `d(x) = x - f(x)/f'(x)` to get the equation `x_{i+1} = d(x_i)`.
This d is also called the update function, because it's what you use to update each approximation to the result.

In this formulation, we know that we've hit the solution when `d(x) = x` and we can estimate the error as `|x - d(x)|`. The equation `d(x) = x` is interesting because what we're looking for is not a zero but a **fixed point** of the update function. Put another way: if you apply the update function, and your value doesn't change, then you've hit the solution.

In summary: we can turn an implicit definition of a value (the square root of 2, via `X^2 - 2 = 0`) into an explicit procedure by producing a sequence of values defined by an update function. If we find a fixed point of the update function, then that must be the solution.

## 4.

What happened to the other solution, the negative square root of 2?
A more general question is whether a particular function can have more than one fixed point, or indeed whether it has any at all. There are lots of *fixed point theorems* in different areas of mathematics that tell you when fixed points exist. For example, `f(x) = x + 1` is never going to have one, but `f(x) = x - (x^2-2)/2x` has two.
(In Newton's approximation method, where you start your `x_0` determines which solution you'll find.)

In the following examples, which come from the algebra side of mathematics rather than the analysis side, our functions may have more than one fixed point but there's a way to order the fixed points. This way we can define precisely what we're looking for, namely the **least fixed point** of a function.

## 5.

Theory of Computation time: give a context-free grammar for all words over the alphabet {a, b} that contain an even number of 'a' characters and no 'b' characters. Here's one grammar:

```
A -> Aaa | epsilon
```

Recall that this means you start with the *non-terminal* symbol capital-A and can repeatedly replace a non-terminal by applying any of its rules. A word is in the language defined by this grammar if there is a sequence of rules starting from A and ending with the word. Epsilon is the empty string. For example, `aaaa` is in the language because `A -> Aaa -> Aaaaa -> aaaa` (in the last step we replaced A with epsilon).

However, this grammar is an *implicit* definition of the language (of all words with an even number of 'a' characters), since A appears both on the left and on the right of the rule `A -> Aaa`.

We could avoid the implicit definition by creating another sequence:

  * `L_0` is the language containing only the empty string.
  * `L_1` is the language containing `L_0` and all words of the form `Aaa` where `A` is a word from `L_0`.
  * ...
  * `L_{n+1}` is the language containing all words from `L_n` and all words of the form `Aaa` where `A` stands for a word from `L_n`.

In this sequence, each language contains the previous one but it grows bigger all the time (`L_n` contains the new word `a^{2n}`).
We could now set `L` to be the union of all `L_n` for `n` a natural number (including zero). This works, but it requires us to understand the concept of a union of infinitely many sets.

To construct this formally, the technique is to express the sequence with an update function: `d(L) = L UNION {Aaa | a in L}` and so `L_{n+1} = d(L_n)`. If we ever hit a language `L` with `d(L) = L`, we are done. This will not of course happen for any finite `n` in this case. But mathematically, if we knew that `d` had a *single* fixed point, then we could define `L` simply as the fixed point of `d`. Since we don't know this, we instead define the language we're looking for to be the **least fixed point** of the update function, which gives us exactly what we want.

## 6.

Start with the group `Z_{12}`. What is the subgroup `<3>`, e.g. the subgroup generated by the element 3?

The intuitive definition is that you start with the "building blocks" `0`, `3`, `+` and `-` and the subgroup is all the elements you can construct with these blocks, e.g. `3+3+3` would be a member.

Formally we can express it like this:

  * `<3>_0` is the set containing `3`.
  * `<3>_1` is the set containing `<3>_0` as well as `a+b` and `(-a)` for any elements `a, b` in `<3>_1`.
  * ...
  * `<3>_{n+1}` is the set containing `<3>_n` as well as their inverses and sums of two elements in `<3>_n`.
  * `<3>` is the least fixed point of this sequence.

Note that the sequence members are sets, not necessarily groups, but the "limit" must be a group because all sums and inverses (and the neutral element) must be in it. That is exactly what the definition of fixed point provides. 

This gets us `<3>_0 = {3}`, `<3>_1 = {9, 3, 6}` (since `9 = (-3)` and `6 = 3+3`), `<3>_2 = {0, 3, 6, 9}` since `0 = 3 + (-3)`. But, `<3>_3 = {0, 3, 6, 9}` too since we're "out of new elements", so if we define the update function as `d(S) = S UNION {(a+b) | a in S} UNION {(-a) | a in S}` then we get `d(<3>_2) = <3>_2` again. This is the least fixed point.

## 7.

We summarise so far: the "least fixed point" construction is a formal way of avoiding infinite unions and implicit definitions.

The construction involves defining an update function `d` and then defining the thing we want to construct as the least fixed point of `d` (the exact rules for when this is allowed are beyond this document). 

Sometimes, the construction terminates after a finite number of steps; sometimes it produces something infinite but still well-defined out of a sequence of finite objects.

In the history of mathematics, around the early 20th century the brightest minds realised that many techniques work "most of the time" but not always, and they wanted to put mathematics on a more solid foundation. The mathematician-philosophers Bertrand Russell and Alfred North Whitehead wrote a book called *Principia Mathematica* trying to develop everything formally from the ground up, and famously taking 360 pages until they could formally show `1+1=2`.

The idea was to rebuild mathematics on top of *Set Theory*, proving everything from a set of Axioms. At first, the approach was simply to agree that "everything is a set", but Russell then hit a contradiction: *does the set of all sets that do not contain themselves, contain itself?* As a result of this and other problems, mathematicians realised that some "things" cannot be sets. Instead, you start with the empty set (or rather with the axiom "There is a set called the empty set") and you are only allowed to call things sets if they can be constructed from the empty set using the axioms. For example, this is Zermelo's construction of the natural numbers:

  * 0 is the empty set `{}`.
  * 1 is the set containing the empty set, `{{}}`.
  * 2 is the set containing the set containing the empty set, `{{{}}}.
  * ...
  * `d(n)` is the set containing `n`.

When addition is defined, you can also call `d(n)` by the name `n+1`.
The notation `succ(n)` for "successor" is also common for this function.

The idea is of course that the natural numbers are the least fixed point of this construction, and that is indeed the most formal definition of the natural numbers that we have.

The axioms for sets one uses nowadays are those developed by Zermelo and Fraenkel, called *ZF set theory*. However, while these axioms let you show that every individual number exists (e.g. is a set), in the original form you couldn't show that the natural numbers as a whole are a set, because there's nothing that allows you to apply them infinitely many times.

(Compare: once you can do addition of two numbers, you can add any finite list of numbers, but addition of a series of infinitely many numbers isn't always allowed, because a sequence doesn't always converge. If you just allowed any set that you could build by using the axioms infinitely often, you'd get Russell's set of all sets back again.)

The ZF axioms that mathematicians use today explicitly include the following:

**Axiom of infinity**: there is a set `I` which contains the empty set, and for any set `x in I` the set `{x}` (that is, the set containing the set `x`) is also in `I`.

This axiom basically allows us to take the fixed point in this particular case, and so to define the natural numbers.

## 8.

Back to programming. To compute the gcd of two integers, you can use the recursion `gcd(a, b) = gcd(b, a % b)` to write the following function (in lua syntax):

```
function gcd(a, b)
    if b == 0 then
        return a
    else
        return gcd(b, a % b)
    end
end
```

This works fine in most programming languages, but to get the recursion to work we need the function gcd to be able to call itself. This means that the compiler must be able to create instructions to "call gcd" while it's still compiling gcd, so in a sense this is an implicit definition that the compiler solves for us.

In pure functional programming, where we want to prove things about code as much as run it, we have to define a function explicitly first before using it. The workaround for recursion is to use something called **continuation-passing**: instead of a function calling itself, it takes a function as an extra parameter and calls that when it's done:

```
function gcd_with_cont(a, b, cont)
    if b == 0  then
        return a
    else
        return cont(b, a % b)
    end
end

function gcd(a, b)
    return gcd_with_cont(a, b, gcd_with_cont)
end
```

Here `cont` is the continuation. The call to `gcd_with_cont` in `gcd` is fine because `gcd_with_cont` is already defined at this point, and using `cont` above is fine because it's a parameter, not a function name itself.

In other words, we can do recursion without a function ever having to call itself, as long as *you can pass a function to itself as a parameter*.

We can interpret this in terms of fixed points. We could set up a sequence of functions

  * `gcd1(a, b)` is only defined when b is 0, and returns a.
  * `gcd2(a, b)` returns a if b is 0, otherwise it returns `gcd1(b, a%b)`. This function is not defined for all numbers either, but it's defined for a few more than `gcd1`.
  * ...
  * `gcd_{n+1}(a, b)` returns a if b is 0, otherwise it returns `gcd_n(b, a%b)`

The update function `d` we get here is the function that takes a function f as input and returns a new function that "unrolls" the gcd one more step. In lua syntax:

```
function d(f)
    return function (a, b)
        if b == 0 then return a
        else return f(b, a % b) end
    end
end
```

A fixed point of `d` is any function that satisfies `d(f) = f`, at least semantically - e.g. the code may not be identical, but for any inputs `a, b` we have `(d(f))(a, b) = f(a, b)`. The original gcd function is such a fixed point. Although it looks like `d` produces an infinite sequence of distinct functions, this is only true if a function cannot call itself, which is exactly what recursive gcd does.

If we look at the structure of a fixed point `f`, then it must obey the equations
  * `f(a, 0) = a`
  * `f(a, b) = f(b, a % b)` if not covered by the cases above.

which is exactly the definition of gcd.

## 9.

We can make the above example a bit more generic in Haskell.
A function that takes a function as input and returns the least fixed point is called a **fixpoint combinator**:

```
file: gcd.lhs

Recursive gcd, the usual way:

> gcdr a 0 = a
> gcdr a b = gcdr b (a `mod` b)

If we can't have function refer to itself, we first define

> gcd0 f a 0 = a
> gcd0 f a b = f b (a `mod` b)

Here's the fixpoint combinator. It takes a function f: (a -> a)
and returns a point a such that (f a == a).

> fix :: (a -> a) -> a
> fix f = f (fix f)

And so we can define

> gcd = fix gcd0

```

If you load this file, you can run e.g. `Main.gcd 2 3` to get 1 (the `Main.` is needed because Haskell already has a built-in gcd function).

The fixpoint combinator definition should be read as follows: `fix f` is the point such that `f` applied to `fix f` gives `fix f` again. Put another way, `fix f` is the point `a` such that `f a = a`. Technically, it's the least such point.

You might complain that the definition of `fix` is itself recursive. This is true, and has to do with how the type system works: a non-recursive combinator is not a problem to run, but it is a problem to typecheck (its type expression would be infinite). You can cheat a bit in Haskell by making a recursive datatype instead:

```
From: https://stackoverflow.com/questions/4273413/y-combinator-in-haskell

> newtype Mu a = Mu (Mu a -> a)
> y f = (\h -> h $ Mu h) (\x -> f . (\(Mu g) -> g) x $ x)
```

But one look at that code should be enough to convince you *not* to do that.

How can a recursive definition `fix f = f (fix f)` ever work? There are different fixpoint combinators that work in different ways - the so-called **Y combinator** is perhaps the most famous (that horrible bit of code above) - but the `fix` one works because Haskell is lazily evaluated.

In general, if you try and compute `fix f` for any function `f` you'd get

```
  fix f
= f (fix f)
= f (f (fix f))
= f (f (f (fix f)))
...
```

But let's consider the constant function `f x = 2`. In this case

```
  fix f
= f (fix f)
```

at which point we're applying `f` to "something", but we already know that the result will be 2 whatever the "something". So Haskell doesn't bother evaluating `fix f` again and just returns 2, which is the fixed point.

If you tried `fix (\x -> x + 1)` which genuinely doesn't have an integer fixed point, the program would go into an infinite loop in theory, and crash with a stack overflow in practice.

Similarly, for the gcd:

```
  gcd 2 3
= fix gcd0 2 3
= gcd0 (fix gcd0) 2 3
= fix gcd0 3 (2 `mod` 3)
= fix gcd0 3 1
= gcd0 (fix gcd0) 3 1
= fix gcd0 1 (3 `mod` 1)
= fix gcd0 1 0
= gcd0 (fix gcd0) 1 0
= 1
```

In the lines where the first function is `fix`, we replace `fix gcd0` with `gcd0 (fix gcd0)`. In the lines starting `gcd0`, we use the definition of `gcd0` which applies the function in its first argument (`fix gcd0`) to the modified parameters.
In the second-to-last line, because `gcd0 something 1 0 == 1`, Haskell doesn't evaluate the "something" and so the fixpoint combinator terminates.

Note that we're doing the same steps (if you look at the numbers on the right) as the more traditional way to evaluate this:

```
  gcd (2, 3)
= gcd (3, 1)
= gcd (1, 0)
= 1
```

(Conclusion: it's magic.)
