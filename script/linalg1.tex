\ifdefined\booklet
\else
\documentclass{scrartcl}

\include{header}

\setuplecture{7}{Linear algebra I}

\begin{document}

\titlestuff

\emph{In this lecture: vector spaces --- linear independence and bases ---
linear maps --- application to finite fields}

\emph{Learning outcomes.}
After this lecture and revision, you should be able to:
\begin{itemize}
\item Define vector spaces.
\item Multiply and (where possible) invert matrices, especially over finite
fields.
\end{itemize}
\fi

\section{Linear algebra}

The basic structure of linear algebra is the \hi{vector space}.
Vector spaces appear in lots of different places. The most important notion for
working with vector spaces is that of a \hi{linear function}, which we have
the opportunity to investigate properly here.

It is sometimes important to distinguish row- and column-vectors. Most of the
time we want column vectors, but they take more space to write down inside a
text, so we introduce the following notation: $(1, 2)$ is a row vector and
$(1, 2)^T$ (T = transpose) is a column vector.

\subsection{Vector spaces}

The new thing about vector spaces is that we construct a structure with two
different kinds of elements, scalars and vectors. We start with a field and
call its elements \emph{scalars}. Then we build a structure of \emph{vectors}
over this field.

A vector space is a group in which you can additionally \emph{scale} vectors by
multiplying them with scalars. This gives some meaning to the idea that we would
like to be able to write $a + a + a$ as $3 \times a$ even if the structure that
contains $a$ does not have an element called $3$.

In general, you can add vectors to vectors and you can multiply scalars with
vectors but you cannot necessarily multiply vectors with other vectors.

\needspace{8\baselineskip}
Let's look at a formal definition:

\begin{definition}
Start with any field $\mathbb F$. A vector space over $\mathbb F$ is a
structure $(V, +, \times)$ where $+: V \times V \to V$ and $\times: \mathbb F
\times V \to V$ satisfying the following laws:

\begin{enumerate}
\item $(V, +)$ is an Abelian group.
\item Field and scalar multiplication associate: for any field elements $f, g$
and any vector $\vec a$ we have $(fg) \times \vec a = f \times (g \times \vec a)$.
Here $fg$ is multiplication in $\mathbb F$.
\item Field multiplication distributes over vector addition: for any vectors
$\vec a, \vec b \in V$ and any field elements $f, g \in \mathbb F$ we have $f
\times (\vec a + \vec b) = f \times \vec a + f \times \vec b$ and $(f +_{\mathbb
F} g) \times \vec a = f \times \vec a + g \times \vec a$. We marked the field
addition with $+_{\mathbb F}$ to distinguish it from vector addition here.
\end{enumerate}
\end{definition}

We use the convention that we write vectors with a line over them, e.g. $\vec
a$ to distinguish them from field elements.

\needspace{2\baselineskip}
\textbf{Examples.}
We have encountered many vector spaces already:

\begin{itemize}
\item Any field is automatically a vector space over itself; vector addition
and scalar multiplication are just field addition and multiplication. This is
one of those cases where you can multiply vectors with each other.

\item The most common notion of a vector is a tuple or sequence of elements.
For any field $\mathbb F$, the vector space $\mathbb F^n$ consists of vectors
of length $n$ with componentwise addition and the scalar multiplication $s
\times (v_1, \ldots, v_n) := (sv_1, \ldots, sv_n)$.

\item By the same logic, the polynomials over a field form a vector space, as
do the polynomials over a field modulo some fixed polynomial. It is thus
possible to interpret $GF(p^n)$ as the $n$-dimensional vector space $GF(p)^n$,
``forgetting'' about the multiplication of polynomials.
\end{itemize}

The following is also a vector space over the finite field $\mathbb F_2$.
Let $V = \{\heartsuit, \diamondsuit, \clubsuit, \spadesuit\}$
with the following addition table:
\[
\begin{array}{l|llll}
%   00           10             01          11    
+ & \heartsuit & \diamondsuit & \clubsuit & \spadesuit \\
\hline
\heartsuit   & \heartsuit   & \diamondsuit & \clubsuit    & \spadesuit \\
\diamondsuit & \diamondsuit & \heartsuit   & \spadesuit   & \clubsuit \\
\clubsuit    & \clubsuit    & \spadesuit   & \heartsuit   & \diamondsuit \\
\spadesuit   & \spadesuit   & \clubsuit    & \diamondsuit & \heartsuit
\end{array}
\]
For scalar multiplication, $0 \times \vec v = \heartsuit$ and $1 \times \vec v
= \vec v$ for any vector $\vec v \in V$.

\subsection{Linear independence and bases}
Where a vector space is, a basis (plural: bases) is not far away. A basis plays
a similar role to a set of generators of a group, but the additional field
multiplication that turns a group into a vector space gives us much more to
work with. Specifically, linear combinations:

\begin{definition}[linear combination]
For a finite set $\{\vec v_i\}_i$ of vectors, a linear combination is a sum
$\sum_i c_i \times \vec v_i$ with coefficients $c_i$ in the field $\mathbb F$.
\end{definition}

If the index set is something like $I = \{1, 2, \ldots, n\}$ then we can write
a linear combination as $c_1 \times \vec v_1 + \ldots + c_n \times \vec v_n$.

A linear combination of vectors where all coefficients are zero (the neutral
element of field addition) is automatically the zero vector (the neutral
element of vector addition). A set of vectors is linearly independent if this
is the only linear combination of them that is zero; another way of saying this
is that no vector in the set can be written as a linear combination of the others.

\begin{definition}[linear (in)dependence]
A set $\{\vec v_i\}_i$ of vectors is linearly independent if no linear
combination of the vectors $\sum_{i} c_i \times \vec v_i$ with coefficients in
$\mathbb F$ gives the zero vector $\vec 0$ (neutral element of vector addition), unless
all coefficients are already zero ($0$, the neutral element of the field's
addition).
\\
A set of vectors that is not linearly independent is called
linearly dependent.
\end{definition}

And finally, a basis is a finite set of linearly independent vectors (in a
particular order) that generates the entire space.

\begin{definition}[basis]
A basis of a vector space $V$ is a finite list $(\vec v_1, \ldots, \vec v_n)$
of vectors that is linearly independent and generates $V$ as a group under
addition, i.e. $V = \langle \vec v_1, \ldots, \vec v_n \rangle$.
\end{definition}

In other words, every vector $\vec w$ in the space can be written as a linear
combination of the basis vectors: $w = w_1 \vec v_1 + \ldots + w_n \vec v_n$.
In fact, if a vector space has a basis then any two bases of the space have the
same number of elements (which we call the dimension of the space) and for any
vector $v$ and any basis in a fixed order, there is exactly one way (one tuple
of coefficients) to write $W$ as a linear combination of the basis vectors.

\begin{proposition}
Any two bases of a vector space have the same number of elements. If a vector
space has a basis with $n$ elements, we say that the space has dimension $n$.
\end{proposition}

And finally, every vector space has a basis. This proposition is only really
mathematically interesting to discuss in the infinite case but it is the start
of most constructions in linear algebra: given any vector space $V$, we can
simply assume that a basis $B$ is given as well.

\begin{proposition}
Every vector space has a basis.
\end{proposition}

There is more to this list of propositions than it might first seem. The first 
time one teaches linear algebra, one usually teaches that a vector \emph{is} a 
list of numbers, called coordinates (and usually written vertically instead of 
horizontally). Vectors can then be used to work with points, lines and other 
objects in 2D/3D spaces.

From a theoretical point of view, we start with a given field and some vectors 
as abstract mathematical objects. For any particular basis --- and we have just 
established that we can always pick a basis --- there is exactly one way to 
write any particular vector $\vec w$ as a list of coordinates $(w_1, \ldots, 
w_n)$ relative to this particular basis, namely if $n$ is the size of the basis 
and the basis elements are the vectors $\vec{b_1}, \ldots, \vec{b_n}$ then 
there is exactly one list of coordinates satisfying the equation 
$\vec w = \sum_{i=1}^n w_i \vec{b_i}$. In other words, a vector \emph{has} a
list of coordinates in any particular basis, even though it \emph{is} an
abstract mathematical object.

\textbf{Example.}
In the vector space example with the four suits,
$\vec b_1 = \diamondsuit, \vec b_2 = \clubsuit$ is a basis.
The possible linear combinations are

\begin{tabular}{lll}
$0 \times \diamondsuit + 0 \times \clubsuit = \heartsuit$ & & 
$1 \times \diamondsuit + 0 \times \clubsuit = \diamondsuit$ \\
$0 \times \diamondsuit + 1 \times \clubsuit = \clubsuit$ & &
$1 \times \diamondsuit + 1 \times \clubsuit = \spadesuit$
\end{tabular}

So each element of $V$ can be expressed as a linear combination of these two
vectors and the only linear combination that is $\heartsuit$, which is the
neutral element of $+$, is the one in which all coefficients are 0.

We can use this basis to define coordinates for the individual vectors, 
by mapping $c_1 \times \diamondsuit + c_2 \times \clubsuit$ to $(c_1, c_2)^T$:
\[
\heartsuit = \vv{0}{0}, \diamondsuit = \vv{1}{0}, \clubsuit = \vv{0}{1},
\spadesuit = \vv{1}{1}
\]

This shows that our example is isomorphic to the vector space $(\mathbb F_2)^2$
with the usual addition and scalar multiplication.

\begin{diamondsec}
The basic definitions of linear algebra (linear independence, basis etc.) can
also be defined for infinite sets, but the exact definition is a bit subtle. We
will not need to worry about this too much in this course.

A linear combination for an infinite set $V$ of vectors is a sum where only a
finite number of coefficients are non-zero.

An infinite set $V$ is linearly independent if no finite sum of elements in
$V$ with coefficients in $\mathbb F$ gives the zero vector, unless all
coefficients are zero. This is equivalent to saying that every finite subset of
$V$ is linearly independent.

An infinite set $W$ of vectors is a basis of a vector space $V$ if (1) it is
linearly independent --- that is, every finite subset of $W$ is linearly
independent in the usual sense and (2) every element in $v \in V$ can be
written as a \emph{finite} linear combination of elements in $W$. This
definition is required to make the theorem ``every vector space has a basis''
true even in the infinite-dimensional case, assuming the Axiom of Choice.
\end{diamondsec}

\subsection{Example: working with bases}

Here are two examples of working with bases. For the first one, we take the field
$\mathbb Q$ of rational numbers (a.k.a. fractions) and the vector space $\mathbb Q^3$.

\begin{itemize}
\item Given the vectors $\vec{v_1} = \vvv{1}{2}{0}$ and $\vec{v_2} = \vvv{1}{4}{2}$,
which vector(s) $\vec{v_3}$ if any make $(\vec{v_1}, \vec{v_2}, \vec{v_3})$ into
a basis?
\end{itemize}

We know that $\mathbb Q^3$ has dimension $3$ over $\mathbb Q$, with the usual basis
\[
\vec{e_1} = \vvv{1}{0}{0}, \quad
\vec{e_2} = \vvv{0}{1}{0}, \quad
\vec{e_3} = \vvv{0}{0}{1}
\]

Every basis of this vector space will contain exactly three linearly independent
vectors. Once a set of vectors is linearly dependent, adding more vectors will
never make it linearly independent again; conversely every set of linearly
independent vectors of dimension less than $3$ can be extended to a basis in a
$3$-dimensional vector space. So the first thing we want to check is if
$\vec{v_1}$ and $\vec{v_2}$ are linearly indepdendent.

In any vector space of any dimension $d > 1$, if we have exactly two vectors
$\vec{v_1}, \vec{v_2}$ then they are linearly independent if and only if there
are no two field elements $c_1, c_2$ such that
$c_1 \times \vec{v_1} + c_2 \times \vec{v_2} = \vec{0}$
and $c_1, c_2$ are not both $0$.
If there are such values $c_1, c_2$ with $c_2 = 0$ then this means $c_1 \neq 0$
and so $\vec{v_1} = 0$.
If the equation is satisfied for $c_2 \neq 0$ then since $c_2$ is a field
element, we can rewrite this equation as
$\vec{v_2} = \frac{-c_1}{c_2} \vec{v_1}$
which means that $\vec{v_2}$ must be a scalar multiple of $\vec{v_1}$.

For the two particular vectors in our example, they are clearly not multiples
of each other so they are linearly independent. The subspace (subgroup of the
vector space when seen as a group) generated by our vectors is 
\[
\left\{
c_1 \times \vec{v_1} + c_2 \times \vec{v_2} \mid c_1, c_2 \in \mathbb Q
\right\}
\]
that is, the space of all linear combinations of these vectors. This is a
$2$-dimensional subspace of $\mathbb Q^3$. Any vector $\vec{v_3}$ that is not in
this subspace will extend $\vec{v_1}, \vec{v_2}$ to a basis. In this example we
could simply pick $\vec{v_3} = \vec{e_3}$. Let's check that this works: writing out
\[
c_1 \times \vvv{1}{2}{0} + c_2 \times \vvv{1}{4}{2} + c_3 \times \vvv{0}{0}{1} =
\vvv{0}{0}{0}
\]
we get the system of linear equations
\[
\left| \begin{array}{rrrcl}
c_1 & + c_2 & & = & 0 \\
2 c_1 & + 4 c_2 & & = & 0 \\
&  2 c_2 & + c_3 & = & 0
\end{array} \right|
\]

Subtracting twice the top equation from the middle one gives $c_2 = 0$, which
quicly shows that $c_1 = 0$ and $c_3 = 0$ too. So these three vectors are
indeed linearly independent, and hence a basis.

Note that there is nothing specific to $\mathbb Q$ in this line of reasoning.
The exact same analysis would work if we were working over say the finite field
$\mathbb F_7$, or indeed any other finite field that has elements called $2$ and
$4$.

\begin{itemize}
\item Write the vector $\vec a = \vvv{2}{3}{5}$ as a linear combination of
$\vec{v_1}, \vec{v_2}, \vec{v_3}$ from above.
\end{itemize}

Writing out the equation system we get
\[
\left| \begin{array}{rrrcl}
c_1 & + c_2 & & = & 2 \\
2 c_1 & + 4 c_2 & & = & 3 \\
&  2 c_2 & + c_3 & = & 5
\end{array} \right|
\]

Subtracting twice the first equation from the second gives $2 c_2 = (-1)$ so
$c_2 = (-1/2), c_1 = 5/2$ and $c_3 = 6$. In other words, in the basis
$\vec{v_1}, \vec{v_2}, \vec{v_3}$
the vector $\vec{a}$ has coordinates $\vvvl{5/2}{-1/2}{6}$.
We will adopt the notation $()^T$ (read: ``transpose'') to write a column vector
inline in a line of text to save some space.

We can answer the exact same question over the finite field $\mathbb F_7$.
Again, we subtract twice the first equation from the second to get $2 c_2 = 6$
which means $c_2 = 3$. Note that $6 +_7 1 = 0$ so ``$(-1) = 6$'', and 
$2 \times_7 4 = 1$ so ``$4 = 1/2$'' and therefore ``$3 = -1/2$''.
Substituting $c_2 = 3$ gives $c_1 = 6$ and $c_3 = 6$, so the coordinates of
$\vec{a}$ in the basis $\vec{v_1}, \vec{v_2}, \vec{v_3}$ are $\vvvl{6}{3}{6}$.

We can summarise this as follows:

\begin{proposition}
Given a vector $\vec a$ in the usual basis $\vec{e_1}, \ldots, \vec{e_n}$,
to compute the coefficients of $\vec a$ in a basis $\vec{b_1}, \ldots, \vec{b_n}$
set up and solve a system of equations
\[
\sum_{i=1}^n c_i \times \vec{b_i} = \vec{a}
\]
where $c_i$ are the free variables.
\end{proposition}

Looking ahead, there is actually a better way to do this, especially if you want
to transform more than one vector into the $\vec{b_i}$ basis. First, note that
the steps in solving the linear equations involve operations (e.g multiply
equation 2 with 3, add equation 1 to equation 4 etc.) that are dictated only
by the numbers on the left-hand side of the equations, which have nothing to do
with $\vec a$ (and everything to do with the basis of $\vec{b_i}$). So to
transform a different vector $\vec{a}'$ you would perform the exact same steps,
only the numbers on the right would change.

Secondly, note that it is very tempting to write the above equation as
$\vec{c} \times B = \vec{a}$; if we can find a way to define what $B$ means.
In this case, what we want to do is compute the inverse of $B$ once, because
then to transform any $\vec{a}'$ into the new basis we just compute
$\vec{a} \times B^{(-1)}$.

\subsection{Subspaces}

Constructing a subspace from a vector space works just like constructing a
subgroup from a group.

\begin{definition}[subspace]
Let $(V, +, \times)$ be a vector space over some field. A subset
$W \subseteq V$ is called a subspace if it forms a vector space under the same
operations, that is $(W, +, \times)$ is itself a vector space (over the same
field).
\end{definition}

Since $(V, +, \times)$ is a vector space, we already know that e.g. $+$ is
associative and $\times$ distributes over $+$. While we know that $+$ has a
neutral element in $V$, one thing we need to check is that this neutral element
is also in $W$. Indeed,

\begin{proposition}
A subset $W$ of a vector space $(V, +, \times)$ is a subspace exactly when the
following conditions hold:
\begin{itemize}
\item The neutral element of $+$ is a member of $W$.
\item For any vectors $\vec w, \vec z$ in $W$, the sum $\vec w + \vec z$ is also in $W$.
\item For any vector $\vec w \in W$ and any scalar $c$ in the base field,
we also have that $c \times \vec w$ is an element of $W$.
\end{itemize}

In fact, this can be reduced to a single condition: \\
\emph{any linear combination
of vectors in $W$ is also an element of $W$}.
\end{proposition}

What do subspaces look like? First of all, since a subspace is a vector space,
it has a dimension. If $W$ is a subspace of $V$ then any set of vectors that is
linearly independent in $W$ must also be linearly independent in $V$ and vice
versa, so $\dim W \leq \dim V$.

In the usual 2D / 3D examples of vector spaces over the reals, subspaces are the
origin (of dimension 0), lines through the origin (dimension 1), planes through
the origin (dimension 2) and the whole 3D space (dimension 3). 

Over a finite field, we cannot get the same pictures but we can count points.
A $n$-dimensional vector space over a field $\mathbb F$ with $q$ elements has
$q^n$ elements, so all subspaces must have exactly $q^m$ elements for some $m \leq n$.
We still get the ``line'' property that if for example $(a, 0, 0)$ is in some
subspace $W$ for any $a \neq 0$ (in the field) then for every element $b$ in
the field, $(b, 0, 0)$ is in $W$ too.

\subsection{Linear maps}
A vector space homomorphism is a function $f: V \to W$ between two vector
spaces over the same field $\mathbb F$ that preserves vector addition and
scalar multiplication. This is a very important concept, so we give it another
name: we call such a function a linear map.

\begin{definition}[linear]
If $V$ and $W$ are two vector spaces over a field $\mathbb F$, we call a
function $f: V \to W$ linear if for any $\vec x, \vec y$ in $V$ and any $a \in
\mathbb F$ we have
\begin{itemize}
\item $f(\vec v + \vec w) = f(\vec v) + f(\vec w)$.
\item $f(a \times \vec v) = a \times f(\vec v)$.
\end{itemize}
\end{definition}

The reader should understand by now which operation symbols refer to
$V$-oper\-ations and which ones refer to $W$-operations.

If $V$ is a vector space with basis $B = (\vec b_1, \ldots, \vec b_n)$ then a
linear map $f: V \to W$ can be computed on any vector from its values on the
basis alone.  Namely, if you know $f(\vec b_1), \ldots, f(\vec b_n)$ and want
to compute $f(\vec v)$ then you can write $\vec v$ in exactly one way as $\vec v =
c_1 \times \vec b_1 + \ldots + c_n \times \vec b_n$. That is, $\vec v$ has
coefficients $(c_1, \ldots, c_n)$ in basis $(\vec{b_1}, \ldots, \vec{b_n})$.

We can compute
$f(\vec v) = c_1 \times f(\vec b_1) + \ldots + c_n \times f(\vec b_n)$, in other
words to compute $f(\vec v)$ it is enough to know the coefficents of $\vec v$
in any basis, and the values of $f$ applied to the elements of that basis.

\textbf{Example.}
Consider the vector space $(\mathbb F_5)^3$. Suppose we know that
a particular linear function $f: (\mathbb F_5)^3 \to \mathbb (\mathbb F_5)^2$ has

$f(\vvvl{1}{0}{0}) = \vv{2}{0}$,
$f(\vvvl{0}{1}{0}) = \vv{1}{1}$ and
$f(\vvvl{0}{0}{1}) = \vv{0}{0}$.

What is $f(\vvvl{3}{1}{3})$?

Answer: It is $3 \times \vv{2}{0} + 1 \times \vv{1}{1} + 3 \times \vv{0}{0} \equiv_5
\vv{2}{1}$.

\textbf{Example.}
In the same vector space, suppose we have a function 
$f: (\mathbb F_5)^3 \to \mathbb \mathbb F_5$
nd we know that
$f(\vvvl{1}{0}{1}) = 2$,
$f(\vvvl{2}{1}{0}) = 4$ and
$f(\vvvl{4}{0}{3}) = 1$.
What is $f(\vvvl{3}{1}{3})$ now?

The three vectors on which we are given the value of $f$ form a basis.
There are two ways to answer this question: either we transform our target
vector into this basis, or we compute $f$ on the usual basis. Let's take the
second approach. Writing $\vvvl{1}{0}{1} = 1 \times \vec{e_1} + 1 \times \vec{e_3}$
etc. we get the following equations, where we go from the left to the middle by 
using that $f$ is linear:
\[ \left| \begin{array}{lclcl}
f(\vec{e_1} + \vec{e_3}) & = & f(\vec{e_1}) + f(\vec{e_3}) & = & 2 \\
f(2 \vec{e_1} + \vec{e_2}) & = & 2 f(\vec{e_1}) + f(\vec{e_2}) & = & 4 \\
f(4 \vec{e_1} + 3 \vec{e_3}) & = & 4 f(\vec{e_1}) + 3 f(\vec{e_3}) & = & 1
\end{array} \right| \]
setting $c_1 = f(\vec{e_1})$ etc. this is just a system of three linear equations
in coeffiecients $c_1, c_2, c_3$:
\[ \left| \begin{array}{lllcl}
c_1 & & + c_3 & = & 2 \\
2 c_1 &+ c_2 &  & = & 4 \\
4 c_1 & & + 3 c_3 & = & 1
\end{array} \right| \]
We can solve this e.g. by adding equations 1 and 3 to get $4 c_3 = 3$ (remember,
$5 = 0$) and so $c_1 = 0, c_2 = 4, c_3 = 2$.
Then $f(\vvvl{3}{1}{3}) = 3 \times 0 + 1 \times 4 + 3 \times 2 \equiv_5 0$.

We could do the same procedure if the range of $f$ was e.g. $(\mathbb F_5)^3$,
we would then end up with three times as many coefficients and equations.

\subsection{Matrices}

Suppose we have a linear function $f: V \to W$. We can imagine this as some kind
of box where you can feed an element of $V$ in and an element of $W$ comes out.
As long as the function is linear, we should be able to figure out what it will
do on any element just by trying out what it does on the elements of some basis.
So let $B = (\vec{b_1}, \ldots, \vec{b_n})$ be a basis of $V$.

If we have a basis $P = (\vec p_1, \ldots, \vec p_m)$ of $W$ as well, you can
compute the coefficients of the images of the basis elements under $f$: there
are unique coefficients $(a_{1, 1}, \ldots, a_{1, m})$ such that $f(\vec b_1) =
a_{1, 1} \times \vec p_1 + \ldots + a_{1, m} \times \vec p_m$ and the same for
the other basis elements.  In other words, a linear map between a
$n$-dimensional vector space $V$ and a $m$-dimensional vector space $W$ can be
specified as a $n \times m$ rectangle of coefficients in the field $\mathbb F$:

\[
f: V \to W \quad \leftrightarrow \quad
\left(
\begin{array}{lll}
a_{1, 1} & \ldots & a_{1, n} \\
\vdots & \ddots & \vdots \\
a_{m, 1} & \ldots & a_{m, n}
\end{array}
\right)
\]

We call such a rectangle of coefficients a matrix. Matrices form a vector space
which we write $\mathbb F^{m \times n}$ for the space of matrices with $m$ rows
and $n$ columns as in the example above. Matrix addition is component-wise;
scalar multiplication with a field element just multiplies all matrix
components with the field element.

Applying a linear map to a vector becomes matrix-vector multiplication. Informally,
to compute the element in the $j$-th row of $M \vec{v}$ you take the $j$-th row
of the matrix and multiply it component-wise with the vector, then sum the
results. More formally:

\begin{definition}
Over any field $\mathbb F$, if $M$ is a matrix of dimension $m \times n$ and
$\vec{v}$ is a vector in $\mathbb F_n$ then the matrix-vector product $M \vec{v}$
is defined as the vector $w$ in $\mathbb F^m$ with coefficients
\[
w_j = \sum_{i=1}^n m_{i,j} \times v_i
\]
for $j = 1, \ldots, m$.
\end{definition}

For example, over $\mathbb F_3$,
\[
\m{
2 & 1 \\
0 & 1
} \vv{1}{2} =
\vv{2 \times 1 + 1 \times 2}{0 \times 1 + 1 \times 2} =
\vv{1}{2}
\]

\subsection{Matrix multiplication and inversion}

If we consider linear maps from a space to itself, we can compose them: for $f,
g: V \to V$ we can from the map $fg$ that takes $\vec v$ to $f(g(\vec v))$. If
both $f$ and $g$ are linear, so is $fg$ (exercise). We can use this to define
matrix multiplication:

\begin{definition}
Over any field $\mathbb F$, if $M$ is a matrix of dimension $a \times b$
and $N$ is a matrix of dimension $b \times c$ then $P=MN$ is the matrix of
dimension $a \times c$ where
\[
p_{i,j} = \sum_{s=1}^c a_{i,s} \times b_{s, j}
\]
for $i$ in $1, \ldots, a$ and $j$ in $1, \ldots, b$.
\end{definition}

The usual way to remember this rule is that $(MN)_{i,j}$ is computed from the
$i$-th row of $M$ and the $j$-th column of $N$.

If we restrict ourselves to square matrices, we get a ring since matrix
multiplication is distributive over matrix addition. However, this is an example
of a \emph{non-commutative ring} since matrix addition commutes but matrix
multiplication does not.
The neutral element of
matrix multiplication is called the identity matrix; it represents the identity
function and has $1$ on the diagonal (top left to bottom right) and $0$ elsewhere.
The netural element of matrix addition is the matrix with $0$ entries everyhere.

Since multiplication does not commute, we're never going to get a ``field of
matrices'' (except in dimension $1 \times 1$, which is just the base field).
But we can still ask if matrix multiplication has inverses.

It's easy to find zero-divisors. The matrix that has $0$ everhwhere except the
first row multiplied with a matrix that has $0$ everywhere except the first
column will give the zero matrix, for example.

Generalising again for a moment to non-square matrices, we can define the rank:

\begin{definition}[row and column rank]
{\ }

\begin{itemize}
\item The row-rank of a matrix is the maximal number of linearly indepdendent rows
that it has.

\item The column-rank of a matrix is the maximal number of linearly indepdendent
columns that it has.
\end{itemize}
\end{definition}

For example, in a matrix with row-rank 3, there is some set of
3 rows that are linearly independent, even though other sets of 3 rows might
be dependent, but all sets of 4 or more rows are linearly dependent.

We almost never need to talk about row- and column-ranks however:

\begin{theorem}
For every matrix over a field, the row-rank equals the column-rank. This quantity
is simply called the \emph{rank}.

A square matrix of size $n \times n$ is invertible if and only if its rank is $n$.
\end{theorem}

Intuitively, in a $n \times n$ matrix of full rank (e.g. the rank is $n$), the
rows (resp. columns) form a basis of $\mathbb F^n$ and you can always convert
from one basis to another and back again. However, if the rows are linearly
dependent, then there must be two or more vectors that are both mapped to the
zero vector: the zero vector itself and the vector with the coefficients $c_i$
causing the linear dependence. So there can be no inverse of the linear function
represented by this matrix, as it is not injective.

\subsection{Equation systems are matrices}

\textbf{Example.}
Over $\mathbb F_7$, express the vector $\vec a$ in the basis
$\vec{b_1}, \vec{b_2}, \vec{b_3}$ where
\[
\vec a    = \vvv{2}{3}{5}, \qquad
\vec{b_1} = \vvv{1}{2}{0}, \quad
\vec{b_2} = \vvv{1}{4}{2}, \quad
\vec{b_3} = \vvv{0}{0}{1}
\]

We are looking for coefficients $c_1, c_2, c_3 \in \mathbb F_7$ such that
$c_1 \times \vec b_1 + c_2 \times \vec b_2 + c_3 \times \vec b_3 = \vec a$.
We could write this out as an equation system, using $b_{ij}$ for the $j$th
component of the basis vector $\vec b_i$, and placing the coefficients on the
right of the products which is the more usual way of writing equation systems:
\[ \left| \begin{array}{rcl}
b_{11} c_1 + b_{21} c_2 + b_{31} c_3 = a_1 \\
b_{12} c_1 + b_{22} c_2 + b_{32} c_3 = a_2 \\
b_{13} c_1 + b_{23} c_2 + b_{33} c_3 = a_3
\end{array} \right| \]
What we would like to do is write this as $B \times \vec c = \vec a$ and then
invert $B$ to get $\vec c = B^{(-1)} \times \vec a$.

The basis matrix $B$ is the matrix with the basis vectors in its \emph{columns}.
\[
B = \m{
1 & 1 & 0 \\
2 & 4 & 0 \\
0 & 2 & 1
}
\]

Inverting a matrix over a finite field follows the exact same algorithm as
over the reals. First, pair the matrix up with an identity matrix on the right:

\[
\left(
\begin{array}{lll|lll}
1 & 1 & 0 & 1 & 0 & 0 \\
2 & 4 & 0 & 0 & 1 & 0 \\
0 & 2 & 1 & 0 & 0 & 1
\end{array}
\right)
\]

Now repeatedly perform elementrary row operations (multiply a row with a
non\-zero coefficient, add a multiple of one row to another, swap two rows)
until you get an identity matrix on the left; the matrix on the right
will be the inverse that you want.

This algorithm will succeed if and only if the matrix has full rank, which is
equivalent to the columns (and also the rows) being a basis of the vector space.

To reduce the number of arithmetic and sign errors, when inverting over finite
fields it is recommended to add the inverse rather than subtract, e.g. instead
of subtracting twice the first row from the second, add 5 times the first row
to the second. This gets us the following inverse:
\[
\left(
\begin{array}{lll|lll}
1 & 0 & 0 & 2 & 3 & 0 \\
0 & 1 & 0 & 6 & 4 & 0 \\
0 & 0 & 1 & 2 & 6 & 1 
\end{array}
\right)
\]

And we can finally compute the coefficients of $\vec a$ in basis $B$:
\[
\left(
\begin{array}{lll}
2 & 3 & 0 \\
6 & 4 & 0 \\
2 & 6 & 1 
\end{array}
\right)
\times \vvv{2}{3}{5} =
\vvv{6}{3}{6}
\]

\ifdefined\booklet
\else
\end{document}
\fi
