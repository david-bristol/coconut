\ifdefined\booklet
\else
\documentclass{scrartcl}

\include{header}

\setuplecture{8}{Linear algebra II}

\begin{document}

\titlestuff

\noindent\emph{Learning outcomes:} After this lecture and revision, you should
be able to
\begin{itemize}
\item Compute inner products in vector spaces over finite fields and explain
the differences to real-numbered vector spaces.
\item Interpret polynomial spaces over fields and finite fields as vector
spaces and operations on them as linear maps.
\item Evaluate and interpolate polynomials using the Vandermonde and Lagrange
bases.
\end{itemize}
\fi

\section{Linear algebra, part II}

Vectors are elements that you can add among themselves and multiply with field
elements: if $V$ is a vector space over field $\mathbb F$ then there is an
addition $+: V \times V \to V$ and a field multiplication $\times: \mathbb F
\times V \to V$ (and the addition/multiplication in the field itself). In
general, you cannot multiply vectors to get more vectors.

\subsection{Inner products and orthogonality}

\newcommand{\p}[2]{\langle #1 \mid #2 \rangle}
\newcommand{\pp}[1]{\left\langle #1 \right\rangle}

What you can do on any vector space of the form $\mathbb F^n$ is introduce a
product $V \times V \to \mathbb F$ that gets you back to the field, called the
\hi{inner product}:
\[
\p{\vec v}{\vec w} = \sum_{i=1}^{n} v_i \times w_i
\]

This product has some useful properties:

\begin{proposition}
The inner product has the following properties for all vectors $\vec v, \vec w, 
\vec z$ and field elements $a$.

\begin{itemize}
\item Symmetry: $\p{\vec v}{\vec w} = \p{\vec w}{\vec v}$.
\item Linearity: $\p{\vec v + \vec z}{\vec w} = \p{\vec v}{\vec w} + \p{\vec
z}{\vec w}$ and $\p{a \times \vec v}{\vec w} = a \times \p{\vec v}{\vec w}$.
\end{itemize}
\end{proposition}

From the definition we see that $\p{\vec v}{\vec 0} = 0$ where $\vec 0$ is a
vector all of whose components are the field zero $0$. If the inner product
between any two vectors is zero, we call them orthogonal:

\begin{definition}[orthogonal]
In a vector space with an inner product, two vectors $\vec v$ and $\vec w$ are
called orthogonal if $\p{\vec v}{\vec w} = 0$.
\end{definition}

The all-zero vector is orthogonal to every vector, including itself.

\subsection{Orthogonal and orthonormal bases}

What about vectors in a basis? First, the definition that we are after:

\begin{definition}
A basis $\vec b_1, \ldots, \vec b_n$ of a vector space is called an
\hi{orthogonal basis} if
for all $i, j \leq n$ with $i \neq j$ we have $\p{\vec b_i}{\vec b_j} = 0$.

If we also have that for all $i \leq n$, $\p{\vec b_i}{\vec b_i} = 1$ then the
basis is called an \hi{orthonormal basis}.
\end{definition}

\textbf{Example.}
In the vector space $\mathbb R^2$, the usual basis $\vec e_1, \vec e_2$ below is
orthonormal, as is the basis $\vec a_1, \vec a_2$ (the second basis is still two
vectors of length 1 at right angles to each other, but rotated by 30 degrees).
\[
\vec e_1 = \vv{1}{0}, \vec e_2 = \vv{0}{1}, \qquad
\vec a_1 = \vv{1/2}{\sqrt{3}/2}, \vec a_2 = \vv{-\sqrt{3}/2}{1/2}
\]

The inner product allows us to recover individual components from a vector.
In the usual basis where $\vec e_i$ is 1 at the $i$th component
and 0 everywhere else, for any vector $\vec v = (v_1, \ldots, v_n)^T$ and any
$i$ we have $v_i = \p{\vec v}{\vec e_i}$ since the inner product ``kills'' all
other components of the vector. This technique works whenever the basis in
question is orthonormal, even if it is not the standard one, and over every
base field including finite fields. But it does not work over arbitrary,
non-orthonormal bases. Can we always find an orthonormal basis?

Over the real numbers, we can always turn a basis into an orthonormal basis 
using the \emph{Gram-Schmidt} process. 
The first step in this process
is to define the length (more precisely: \hi{norm}) of a vector $\vec v$ as
$\|\vec v\| := \sqrt{\p{\vec v}{\vec v}}$.
Then we can normalise a vector $\vec v$ by computing
$(1/\|\vec v\|) \times \vec v$.

Over a finite field, this will not always work. Consider the vector space
$(\mathbb F_3)^2$ and the vector $\vec v = (2, 1)^T$. The inner product 
$\p{\vec v}{\vec v}$ is 2, so we would have to divide by a square root of 2 ---
but in $\mathbb F_3$, we have $0^2 = 0, 1^2 = 1, 2^2 = 1$ so there is no element
$x$ satisfying $x^2 = 2$. Field elements with no square root are called
\hi{quadratic non-residues} and over a finite field with odd characteristic,
half the nonzero elements will be quadratic non-residues; this fact has some
applications in cryptography.

Note that for a vector space $(\mathbb F_q)^n$ we can always \emph{pick} an
orthonormal basis, for example the usual one where $\vec e_i$ is 1 at the $i$th
component and 0 everywhere else. But this will not help us find an orthogonal
basis of e.g. a 2-dimensional subspace of a 3-dimensional vector space.

Can we at least get an orthogonal basis? Gram-Schmidt says that to make a vector
vector $\vec v$ orthogonal to a vector $\vec u$ while keeping the space that
these two vectors span the same, you compute
\[
    \vec v - \frac{\p{\vec v}{\vec u}}{\p{\vec u}{\vec u}} \times \vec u
\]
The problem here is that the denominator could be zero. Over the reals, for
a vector $\vec v$ the inner product with itself is $(v_1)^2 + \ldots + (v_n)^2$
which can only be 0 if all components are 0, but over $(\mathbb F_3)^3$ the
inner product of $(1, 1, 1)^T$ with itself is zero. This shows again that
defining a norm of a vector over a finite field is not a good idea: it does not
work the way we would expect it to. From this example we can see that the
one-dimensional subspace of $(\mathbb F_3)^3$ with the basis
$\vec b_1 = (1, 1, 1)^T$ cannot have an orthogonal basis (the only other nonzero
vector in it is $(2, 2, 2)^T$ which also has an inner product of 0 with itself).

The problem is that it is hard to define multiplication on ``lists of things''
without getting into cases where the product of two nonzero things ends up being
zero.

\subsection{Classification of vector spaces over finite fields}

%Inner products allow us to clarify what we mean by a \hi{component} of a vector.
%In general, a vector space over a field is just a set of elements together with
%some operations and conditions, but these elements do not have to be ``lists of
%field elements''. For example, here is a vector space $(V, +, \times)$ over
%$\mathbb F_2$: let $V = \{\heartsuit, \diamondsuit, \clubsuit, \spadesuit\}$
%with the following addition table:
%\[
%\begin{array}{l|llll}
%%   00           10             01          11    
%+ & \heartsuit & \diamondsuit & \clubsuit & \spadesuit \\
%\hline
%\heartsuit   & \heartsuit   & \diamondsuit & \clubsuit    & \spadesuit \\
%\diamondsuit & \diamondsuit & \heartsuit   & \spadesuit   & \clubsuit \\
%\clubsuit    & \clubsuit    & \spadesuit   & \heartsuit   & \diamondsuit \\
%\spadesuit   & \spadesuit   & \clubsuit    & \diamondsuit & \heartsuit
%\end{array}
%\]
%For scalar multiplication, $0 \times \vec v = \heartsuit$ and $1 \times \vec v
%= \vec v$ for any vector $\vec v \in V$.

%In this vector space, $\vec b_1 = \diamondsuit, \vec b_2 = \clubsuit$ is a basis.
%The possible linear combinations are
%$0 \times \diamondsuit + 0 \times \clubsuit = \heartsuit$,
%$1 \times \diamondsuit + 0 \times \clubsuit = \diamondsuit$,
%$0 \times \diamondsuit + 1 \times \clubsuit = \clubsuit$,
%$1 \times \diamondsuit + 1 \times \clubsuit = \spadesuit$.
%So each element of $V$ can be expressed as a linear combination of these two
%vectors and the only linear combination that is $\heartsuit$, which is the
%neutral element of $+$, is the one in which all coefficients are 0.

The following theorem classifies vector spaces over finite fields; we add the
qualifier \emph{finite-dimensional} explicitly to avoid complaints from people
who know about the details of infinite-dimensional cases which do not concern us
in this unit.

\begin{theorem}
Every finite-dimensional vector space $V$ over a finite field $\mathbb F$ is
isomorphic to $(\mathbb F^n, +, \times)$ with the usual addition and
multiplication, for exactly one value $n \in \mathbb N$.
\end{theorem}

To see this, suppose that $V$ is a vector space over a finite field. We know
that $V$ must have a basis and that all bases ov $V$ have the same number of
elements, call this number $n$ and call the basis vectors $\vec b_1, \ldots,
\vec b_n$. We further know that each vector $\vec v \in V$ can be written in a
unique way as $\vec v = (v_1, \ldots, v_n)$ such that
$\vec v = \sum_{i=1}^n v_i \times \vec b_i$.

Our isomorphism is simply the function that maps $(v_1, \ldots, v_n)$ to the
same vector in $\mathbb F^n$, although in this vector space the same list of
coefficients represents the vector 
$\sum_{i=1}^n v_i \times \vec e_i$
for the usual basis $\vec e_1, \ldots, \vec e_n$.
This function is linear and bijective: it is injective because if two vectors
have the same representation in $\mathbb F_n$ then they also have the same
coefficients back in $V$ an the function is surjective because for every vector
in $\mathbb F_n$ there is a preimage in $V$, namely the vector with the same
coefficients. This proves the theorem.

From this, we can determine how many elements such a vector space can have:

\begin{corollary}
The number of elements in such a vector space must be of the
form $q^n$ where $q = |\mathbb F|$. Since the number of elements in a finite
field must be a prime power ($q = p^k$ for some prime $p$ and $k \in \mathbb N$),
the number of elements in a vector space over a finite field must be a prime
power too.
\end{corollary}

\subsection{Polynomials as vectors}

For any field $\mathbb F$, we can view the set of polynomials (in one variable)
$\mathbb F[X]$ as a vector space with the usual polynomial addition and scalar
multiplication.
This vector space has infinite dimension, however every element (polynomial)
has a finite degree. The standard basis for this vector space is $\vec{e_0} =
(1, 0, 0, \ldots) = 1$, $\vec{e_1} = (0, 1, 0, 0, \ldots) = X$, $\vec{e_2} =
(0, 0, 1, 0, 0, \ldots) = X^2$ and so on (we start the numbering at $0$ to
match up with the dimensions of the polynomials).

The coefficients of a polynomial are simply its coordinates as a vector in this
space under the standard basis, e.g. $1 + 2 X + 3 X^2 = (1, 2, 3)$. We will see
that it is convenient to write polynomials as row vectors instead of column
vectors.

Remember that a polynomial of degree $d$ is given by $d+1$ coefficients, or
$d+1$ points $(x, y)$ through which the polynomial passes. In fact, any
combination of points and coefficients will do as long as there are $d+1$ of
them in total, and all of this works over any field.

For example, consider the finite field $\mathbb F_5$ and fix the degree $d = 2$.
A polynomial of degree $2$ has $2 + 1 = 3$ coefficients so we are working
in a vector space of dimension $3$, namely $(\mathbb F_5)^3$.

To evaluate a polynomial with coefficient vector $\vec c$ at a point $x$,
we map the point to a vector $\vec x = (x^0, x^1, \ldots, x^n)$.
These are exponents, not just indices, and we define $0^0$ to be $1$ because
this is useful, and because I am a mathematician so I am allowed to do that.
We can then compute $\vec c \times \vec x$
to get $c_0 + c_1 x + \ldots + c_n x^n$.

Note that we are technically taking a matrix product here, since $\vec c$
is a row vector so we can view it as a matrix of dimension $1 \times (n+1)$ 
and $\vec x$ is a column vector, which we can view as a matrix of dimension
$(n + 1) \times 1$ so the product should be a matrix of dimension $1 \times 1$
which we can pretend to be a scalar.
If we allow row vectors as well as column vectors in inner products, then the
evaluation of the polynomial with coefficient vector $\vec c$ at point $x$ is
$\p{\vec c}{\vec x}$. Actually, in a more formal mathematics course one would
define the inner product between two column vectors as the matrix product with
the left vector transposed: $\p{\vec a}{\vec b} := \vec a^T \times \vec b$.

\begin{diamondsec}
To be really formal, over a field with a notion of conjugation (in particular
anything involving the complex numbers), we would conjugate as well as
transpose the left vector.
\end{diamondsec}

To evaluate a polynomial at multiple points at once, we can form a matrix with
the vectors for each point:
\[
    M = 
\left( \begin{array}{llll}
1        & 1        &  \cdots & 1        \\
x_1      & x_2      &  \cdots & x_m      \\
(x_1)^2  & (x_2)^2  &  \cdots & (x_m)^2  \\
\vdots   & \vdots   &  \ddots & \vdots   \\
(x_1)^n  & (x_2)^n  &  \cdots & (x_m)^n  \\
\end{array} \right)
\]

This is called a \hi{Vandermonde matrix}. To evaluate, we compute
$\vec c \times M$ which gives us the vector $(p(x_1), \ldots, p(x_m))$
where $p(x_i)$ is the polynomial evaluated at the point $x_i$.
Vandermonde matrices have a useful property:

\begin{proposition}
A square Vandermonde matrix with all points distinct is an invertible matrix.
\end{proposition}

This proposition is the linear algebra version of the statement that you can
interpolate a polynomial of degree $d$ from its values at any $d+1$ points.
Interpreting polynomials as vectors, the proposition says that any $n+1$
vectors of the form $(1, x, x^2, \ldots)$ for $n+1$ distinct points, and of
length $n+1$, form a basis for the vector space of polynomials up to degree $n$.

\subsection{Interpolation}

Suppose we know that a polynomial $p$ over $\mathbb F_5$
of degree-bound $2$
has $p(0) = 2, p(1) = 4$ and $p(3) = 3$.
What are its coefficients?

In the language of linear algebra, we have just been told that
$\vec c \times M = (2, 4, 3)$ where $M$ is the Vandermonde matrix
\[
\m{
0^0 & 1^0 & 3^0 \\
0^1 & 1^1 & 3^1 \\
0^2 & 1^2 & 3^2
}
=
\m{
1 & 1 & 1 \\
0 & 1 & 3 \\
0 & 1 & 4
}
\]
so we just have to invert this matrix (which we know is possible because it is
a square Vandermonde matrix on distinct points) and then we can compute
$\vec c = (2, 4, 3) \times M^{(-1)}$. Since matrix multiplication is not
commutative, we need to multiply the inverse matrix from the right here.

In terms of bases, since we have to multiply with the inverse of a basis matrix
$B$ to convert from the usual basis into the basis defined by the columns of
$B$, this means that \emph{the Vandermonde matrix is the inverse of the basis
matrix for the basis defined by evaluation at given points}.

The inverse of our matrix, and the polynomial's coefficients are
\[
    M^{(-1)} =
\m{
1 & 2 & 2 \\
0 & 4 & 2 \\
0 & 4 & 1
}
\qquad
(2, 4, 3) \times M^{(-1)} =
(2, 2, 0)
\]
and indeed, the polynomial $p(X) = 2 + 2X$ has $p(0) = 2$, $p(1) = 4$ and
$p(3) = 3$. It turns out that the quadratic term is zero, these three points
lie on a ``straight line''.

In particular, we note that interpolating a polynomial from given points is a
\emph{linear operation in the values provided}.

So what about that Lagrange interpolation formula?

\subsection{Lagrange interpolation}

Lagrange's interpolation formula finds the unique polynomial of degree-bound
$n$ that passes through $n+1$ given points. It is often taught in analysis
classes over the real numbers (and sometimes also the complex numbers) but it
works over any field.

\begin{definition}[Lagrange interpolation]
The Lagrange interpolating polynomial of degree-bound $n$ for a set of points
$(x_i, y_i)_{i=0}^n$ is the polynomial $p(X)$ defined as
\[
p(X) = \sum_{i=0}^n y_i \times L_{i,n}(X)
\quad \text{ where } \quad
L_{i,n}(X) = \prod_{k \neq i} \frac{X - x_k}{x_i - x_k} 
\]
\end{definition}

We are doing \emph{linear} algebra here, so we look for linear functions in
this formula. The product over $k$ goes from $0$ to $n$ leaving out the value
$i$, so there are $n$ terms in the product. Writing the variable $X$ as a
capital letter makes clear that the $x_i$ and $x_k$ are just harmless consants
and so $L_{i,n}$ is a polynomial of degree-bound $n$ (actually, degree exactly
$n$) in one variable $X$. We can also see from the formula that $L_{i,n}(X)$
is zero if $X$ is one of the $x_k$, excluding $x_i$.

We need a general fact about polynomials here:

\begin{theorem}
A polynomial of degree $n \geq 0$ that is not the all-zero polynomial has at
most $n$ zeroes over any field.
\end{theorem}

So $L_{i,n}(X)$ is ``the'' polynomial of degree-bound $n$ that has its zeroes
exactly at the $n$ values $x_k (0 \leq k \leq n, k \neq i)$. This polynomial is
only unique up to units, that is nonzero elements in the field (for example,
multiplying a polynomial by $2$ does not change where the zeroes are). The
reason for this extra degree of freedom is that asking for a polynomial to have
$n$ particular zeroes is giving $n$ conditions, but it takes $n+1$ conditions
to fully determine a polynomial of degree-bound $n$. The Lagrange polynomial
$L_{i,n}(X)$ has the extra condition that it takes the value $1$ at $x_i$:

\begin{proposition}
The polynomial $L_{i,n}(X)$ constructed from a set of distinct points
$\{x_0, x_1, \ldots, x_n\}$ is the unique polynomial of degree-bound $n$
that satisfies the following conditions:
\begin{itemize}
\item $L_{i,n}(x) = 0$ for $x \in \{x_0, x_1, \ldots, x_n\}$ and $x \neq x_i$.
\item $L_{i,n}(x_i) = 1$.
\end{itemize}
\end{proposition}

In linear algebra terms, this gives us the following fact:

\begin{corollary}
For any of distinct points $\{x_0, x_1, \ldots, x_n\}$,
their Lagrange polynomials $L_{i,n}(X)$ are a basis of the space of polynomials
of degree-bound $n$.
\end{corollary}

Let's prove this. All we need to show is that these $n+1$ polynomials are
linearly indepdendent, since a set of $n+1$ linearly indepdendent vectors is
automatically a basis in a vector space of dimension $n+1$. The proof is by
contradiction.

Suppose there are some coefficients $c_0, \ldots, c_n$ such that
$p(X) = c_0 \times L_{0,n}(X) + \ldots + c_n \times L_{n,n}(X)$ is the all-zero
polynomial. Evaluating $p(x_0)$, since $L_{0,n}(x_0) = 1$ and $L_{i,n}(x_0) = 0$
for all $i > 1$ we have $p(x_0) = c_0$. But we assumed that $p(x) = 0$ for all
$x$, so $c_0 = 0$. By the same argument on the remaining points, $c_i = 0$ for
all $i$, therefore the Lagrange polynomials are linearly indepdent.

The Lagrange interpolation formula turns out to be nothing more than evaluating
the polynomial in the basis defined by the Lagrange interpolation polynomials.
The usual way to compute $p(x)$ for a particular $x$ is to evaluate the
interpolation polynomials at $x$, that is compute the values
$\ell_i = L_{i,n}(x)$ and then finally to compute a sum-of-products, which
is nothing more than the inner product
\[
    p(x) = \p{(y_0, y_1, \ldots, y_n)}{(\ell_0, \ell_1, \ldots, \ell_n)}
    = \sum_{i=0}^n y_i \times \ell_i
\]
But you can also compute the coefficients of the Lagrange interpolation
polynomials (for a particular set of $x_i$) to get a matrix representing this
basis, then you can multiply the vector of $y_i$ with this matrix to get
the coefficients of $p$. This way, Lagrange interpolation is nothing more than
a basis transformation.

\textbf{Example.}
Let's take the same field $\mathbb F_5$ and the same polynomial as above, that
is we know that our polynomial has a degree-bound of $2$ and passes through
the points $(0, 2)$, $(1, 4)$ and $(3, 3)$ --- these are the $(x_i, y_i)$ pairs.

Let's compute the first Lagrange polynomial:
\[
L_{0,2}(X) = \frac{(X-1)(X-3)}{(0-1)(0-3)} = 2(X-1)(X-3) = 4 + 2X + 2X^2
\]
The others are $L_{1,2}(X) = 0 + 4X + 2X^2$ and $L_{2,2}(X) = 0 + 4X + X^2$.
This makes the original polynomial
\[
p(X) = 2 \times L_{0,2}(X) + 4 \times L_{1,2}(X) + 3 \times L_{2,2}(X) 
= 2 + 2X
\]
which is what we got above.

\needspace{4\baselineskip}
\begin{diamondsec}
The Vandermonde basis and Lagrange basis are two different bases in which there
are known formulas to interpolate polynomials. Over a finite field, it does not
matter which one we use as we are doing exact computations all the time.
Over the real numbers, but working with floating-point numbers as approximations,
the Lagrange basis is more \emph{numerically stable}, that is less susceptible
to small rounding errors causing large mistakes in the result.
\end{diamondsec}

\ifdefined\booklet
\else
\end{document}
\fi
