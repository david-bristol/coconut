\ifdefined\booklet
\else
\documentclass{scrartcl}
\include{header}
\setuplecture{14}{Hamming and Reed-Solomon Codes}
\begin{document}
\titlestuff
\fi

\section{Hamming and Reed-Solomon Codes}

Recall that a linear code is a code where the encoding function is a linear map
$\mathbb F^k \to \mathbb F^n$ over some field $\mathbb F$ (where $n \geq k$).

\subsection{Generalised Hamming codes}

The $(7, 4)$ Hamming code encodes $4$ data bits as a $7$-bit codeword (or $8$
bits for the SECDED version). We can expand this idea to any power of 2:

\begin{definition}
For any $m \geq 1$, the $(2^m-1, 2^m - m-1)$ generalised Hamming code (resp. 
$(2^m, 2^m-m-1)$ SECDED hamming code) encodes up to $2^m - m - 1$ data bits 
with $m$ parity bits for a total of $2^m-1$ (resp. $2^m$) bits according to the 
following rules (this gives a non-systematic encoding):
\begin{itemize}
\item The minimal distance is $d = 3$ (resp. $d = 4$ for the SECDED version)
for any value of $m$.
\item The $i$-th bit is a parity bit if $i$ is a power of 2, otherwise a data bit.
\item The parity bits are set such that the sum of all bits whose index $i$
has a $1$ at the $j$-th position when written out in binary, equals zero.
\item For the SECDED version, an additional initial bit 0 is set such that the
sum of all bits in the codeword is zero.
\end{itemize}
\end{definition}

Let's look at the $(7, 4)$ Hamming code under this definition.

{
\let\oldp\p
\let\p\relax
\newcommand{\p}{\text{parity}}
\renewcommand{\d}{\text{data}}
\newcommand{\s}{\phantom{000}}
\[ \begin{array}{r|rrrrrrrr}
\text{bit} & \s 000 & \s 001 & \s 010 & \s 011 & \s 100 & \s 101 & \s 110 & \s 111 \\
\text{type} & \text{(parity)} & \p & \p & \d & \p & \d & \d & \d
\end{array} \]
\let\p\oldp
}

The equations that define a valid codeword are linear equations each with one
parity and $2^{m-1} - 1$ data bits (not shown here: the SECDED version has an
extra equation that says the sum of all bits must be zero).

\[ \left| \begin{array}{rcl}
p_{001} + d_{011} + d_{101} + d_{111} & = & 0 \\
p_{010} + d_{011} + d_{110} + d_{111} & = & 0 \\
p_{100} + d_{101} + d_{110} + d_{111} & = & 0
\end{array} \right| \]

To show that the minimum distance is $d = 3$, we observe that there cannot be
any codewords of weight $1$ or $2$. A codeword of weight $1$ would falsify at
least one of the equations, since every bit is contained in at least one
equation. A codeword of weight $2$ cannot have just two parity bits set as this
would falsify two equations; every parity bit appears in exactly one equation.
So a hypothetical codeword of weight $2$ would have at least one data bit set.
If it has exactly one data bit set, since the binary expansion of the index of
every data bit contains at least two $1$s this would mean at least two parity
bits need to be set as well making the weight at least $3$; if the codeword has
two data bits set then since there must be an index at which one data bit has a
$0$ in its binary expansion and the other a $1$ (otherwise the to bits would
have the same index) then at least one parity bit must be set too. So the weight
is again at least $3$.

\subsection{Decoding Hamming codes}

There is an algorithm for decoding Hamming codes that is more efficient than
computing the parity-check matrix, and also performs the error correction.
Take a received word and check if all the parity bits are correct. If so, you
have a codeword and can take the data bits; if not, then add the indices of the
incorrect parity bits. The sum gives you the index of the incorrect bit, under
the assumption that at most one error occurred.

\textbf{Example.}
You receive the word 0110001. The equation $p_1 + d_3 + d_5 + d_7 = 0$ holds;
$p_2 + d_3 + d_6 + d_7 = 0$ does not hold and $p_4 + d_5 + d_6 + d_7 = 0$ does
not hold either. Therefore the mistake must be bit $2 + 4 = 6$, so the codeword
should be 0110011 and the data bits should be 1011.

\begin{exercise}
Generalised Hamming code:
\begin{itemize}
\item $(\star)$ Decode the codewords 1110000 and 1011010 in the $(7, 3)$
Hamming code, assuming at most one error occurred in each word. Which errors,
if any, occurred in these words?
\item $(\star)$ For $m = 2$ we get a $(3, 1)$ Hamming code. We have encountered 
this particular code under another name already, what is it?
\item $(\star\star)$ Show that for every $m$, the generalised Hamming code 
(non-SECDED version) contains a codeword of weight $3$.
\item $(\star\star)$ Describe the decoding procedure for the SECDED version of 
the generalised Hamming code.
\end{itemize}
\end{exercise}

\needspace{5\baselineskip}
\subsection{Reed-Solomon Codes}

The Singleton bound gives us $d \leq n - k + 1$ for a linear code that turns
$k$-bit messages into $n$-bit codewords. Codes whose minimum distance $d$ is as
high as this bound allows are called \hi{Maximum Distance Separable} or
\hi{MDS codes}. One example of MDS codes are the Reed-Solomon Codes which are
used among other things on CDs (and later variants such as DVDs) and in QR
codes, 2-dimensional barcodes for use with mobile phone applications.

Pick a finite field $\mathbb F$ of size at least $n$. Call this size $q$. Pick 
$n$ distinct points $x_1, \ldots, x_n$ in $\mathbb F$. To encode a message
$m = m_0 m_1 \ldots m_{k-1}$, view it as a polynomial
\[
M = m_0 + m_1 X + m_2 X^2 + \ldots m_{k-1} X^{k-1}
\]
and compute the encoding
\[
E(m) = (M(x_1), M(x_2), \ldots, M(x_n))
\]

The generator matrix of the $(n, k, d = n-k+1)_q$ Reed-Solomon code is a 
Vandermonde matrix, transposed to have $k$ rows and $n$ columns:
\[
G = \left( \begin{array}{llll}
1 & 1 & \cdots & 1 \\
x_1 & x_2 & \cdots & x_n \\
(x_1)^{2} & (x_2)^{2} & \cdots & (x_n)^{2} \\ 
\vdots & \vdots & \ddots & \vdots \\
(x_1)^{k-1} & (x_2)^{k-1} & \cdots & (x_n)^{k-1} \\ 
\end{array} \right)
\]

Let's compute the minimum distance of this code. Over a finite field, a nonzero 
polynomial of degree-bound $k-1$ can have at most $k-1$ zeroes. This means that 
for a nonzero message $m$, at most $k$ of the values in $E(m)$ can be zero so 
at least $n-k+1$ of the values in the codeword $c = E(m)$ are nonzero for any
nonzero message $m$. Since this code is linear, it must have minimum distance at
least $d \geq n - k + 1$, but by the Singleton bound $d$ cannot be any higher so
this inequality must be exact.

\subsection{Reed-Solomon decoding}

One can test if there were any errors during transmission by interpolating a
degree-bound $k-1$ polynomial from the first $k$ received values and then
testing if the remaining values lie on this polynomial; if they do, then 
(assuming there were not more errors than this code can detect) the coefficients
of the polynomial give the original message back.

Correcting errors with a Reed-Solomon code is possible (these codes are used in
practice for exactly that purpose, after all) but requires more advanced
techniques. In practice, this is done in specialised hardware (for example in
CD players). The theory behind this would require another 5--10 credit points
of time to explain properly, and so is out of scope for the CNuT unit.

\begin{exercise}
\emph{Reed-Solomon Codes:}

\emph{For the following exercises, we work over the finite field $\mathbb F_5$.}

$(\star)$ Compute a generator matrix $G$ for the Reed-Solomon Code with $k = 2$,
$n = 4$ and $x_1 = 1, x_2 = 2, x_3 = 3, x_4 = 4$.

$(\star)$ With this generator matrix, encode the words $23$, $10$ and $44$.

$(\star)$ Convert the generator matrix $G$ of this code into systematic
form and then compute a parity-check matrix $H$ for this code.

$(\star)$ Of the following three words, two are codewords and one is not.
Find the two codewords: $4203$, $0314$, $0122$.

$(\star\star)$ Decode the two codewords that you found.

$(\star\star)$ For the word above that is not a codeword, find the closest
codeword and decode that.

\emph{And finally, some thinking exercises:}

$(\star)$ We could try and make a Reed-Solomon Code over $\mathbb F_2$
with $n = 4, k = 2, d = 3$, but we have already seen that such a code cannot
exist! What will go wrong?

$(\star\star)$ What happens to the minimum distance of a Reed-Solomon Code if
you pick the point $x_1$ to be the zero point of the finite field?

\end{exercise}


\ifdefined\booklet
\else
\end{document}
\fi
