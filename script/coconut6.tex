\ifdefined\booklet
\else
\documentclass{scrartcl}

\include{header}

\setuplecture{6}{Finite fields}

\begin{document}

\titlestuff

\emph{In this lecture: finite commutative rings without zero divisors are
fields --- classification of finite fields --- irreducible polynomials ---
construction of finite fields $GF(p)$ and $GF(p^n)$ ---
computing in finite fields --- isomorphisms between finite fields}

\emph{Learning outcomes.}
After this lecture and revision, you should be able to:
\begin{itemize}
\item Decide whether or not a finite field of a given size exists.
\item Construct a finite field of any size where one exists.
\end{itemize}
\fi

\section{Finite Fields}

A field is a structure in which you can add, subtract, multiply and divide
(except by zero). Today we are going to look at finite fields. Recall that
$\mathbb Z_{256}$ is not a field because of zero divisors. Can we construct a
field with $256$ elements? We can, but not with the usual addition modulo
$256$. The first question we have to answer to get our field with $256$
elements is, when is a finite ring a field?

\subsection{Finite commutative rings}

Take any ring $(R, +, \times)$. We know that we can classify all elements as
zero, unit, zero divisor or neither. But if our ring is finite, the ``neither''
case cannot happen.

Let's pick an element $a$ in a finite ring that is neither zero nor a zero
divisor. This means that if we look at the sequence $a, a \times a, a \times a
\times a, \ldots$, we can never hit zero. We can obviously write this as $a,
a^2, a^3, \ldots$. But if the ring is finite, at some point an element has to
repeat so we get an equation of the form $a^k = a^{k + m}$ for positive
integers $k, m$ from which we conclude that $a^m = 1$ (you can cancel $a^k$
since $a$ is not a zero divisor, therefore neither is $a^k$).
Whether or not the ring is commutative, the associative law implies that powers
of $a$ commute with each other. Therefore $a \times a^{m-1} = 1$ and $a^{m-1}
\times a = 1$, so $a^{m-1}$ really is the inverse of $a$ under multiplication.

In other words, the only way a nonzero ring element can be neither a unit nor a
zero divisor is if the ring is infinite (for example, $3$ in $\mathbb Z$). In a
finite ring such as $\mathbb Z_n$, as soon as a nonzero element is not a zero
divisor it automatically has an inverse. This proves the following proposition:

\begin{proposition}
A finite commutative ring without zero divisors is a field.
\end{proposition}

We know that the rings $(\mathbb Z_n, +, \times)$ satisfy these
conditions exactly when $n$ is a prime, except for the special case $(\{0\}, +,
\times)$.
(For a non-prime $n$, the structure $(\mathbb Z^\times_n, \times)$ is a group
but such a $\mathbb Z^\times_n$ is no longer a group under addition so we
cannot construct a ring this way, let alone a field.)
This means that for each prime $p$, we can construct a field with $p$ elements.

\subsection{Classification of finite fields}

Finite fields are relatively ``rare'' objects. The following theorem describes
exactly which ones exist:

\begin{theorem}[classification of finite fields]
For every prime $p$ and every positive integer $n$, there is exactly one finite
field with $p^n$ elements up to isomorphism. These are the only finite fields.
\end{theorem}

The field with $p^n$ elements can be written either $\mathbb F_{p^n}$ or
$GF(p^n)$ and pronounced ``Galois Field'' after the French mathematician E.
Galois. The power operator is always left in the description, i.e. one writes
$GF(2^8)$ not $GF(256)$.

We will investigate the meaning of ``up to isomorphism'' in more detail later on.

\subsection{Prime fields}

The simplest finite fields are those for power $n = 1$: these are just the
fields $GF(p) = (\mathbb Z_p, +_p, \times_p)$ with the usual addition and
multiplication modulo $p$ that we constructed above. Every other attempt to
construct a finite field of order $p$ will produce one isomorphic to the above
construction since if we forget about multiplication, we have a group of prime
order $p$ and such a group can only have subgroups of order $1$ (which is just
the neutral element) and $p$ itself, which makes the group cyclic.

\subsection{Irreducible polynomials}

Here is the general idea to construct a field with $p^n$ elements. Start with
the field $GF(p)$ and form the polynomial ring $GF(p)[X]$. This has infinitely
many elements. Then, take this ring modulo a polynomial of degree $n$ to get a
ring of $p^n$ elements (sequences of $n$ elements from $\mathbb Z_p$). As long
as we do not end up introducing any zero divisors, since this ring is finite
and commutative it is automatically a field.

How do we prevent zero divisors? When going from $\mathbb Z$ to $\mathbb Z_n$
(viewed as rings), zero divisors are exactly the non-unit elements that divide
$n$ so we don't get any if we pick $n$ to be a prime.

I've said before that in Algebra, polynomials are just ``another kind of
number''. So if we pick a polynomial to divide by that is a ``prime
polynomial'', we should not get any zero divisors for exactly the same reason.

We have to quickly get some terminology out of the way. In general algebra,
there are two distinct concepts ``prime'' and ``irreducible''. What we actually
need to avoid zero divisors are irreducible moduli; both the integers and
polynomial rings over fields are special cases where prime and irreducible turn
out to be the same thing (technically, so-called unique factorisation domains).
While ``prime number'' is the common term for the integers, when talking about
polynomials it is more usual to talk about ``irreducible polynomials'' which we
will do from now on. For the purposes of this course, it is not too inaccurate
to imagine ``irreducible'' to mean ``something like prime''

\begin{definition}[irreducible polynomial]
A polynomial $p$ over a field is irreducible if it is not a unit and cannot be
decomposed into a product of non-units, i.e. if $p = ab$ then either $a$ or $b$
is a unit.
\end{definition}

The definition of irreducible applies not only to polynomial rings over fields
but to any integral domain, though we will only be using it for polynomials.

\begin{exercise}
$(\star)$ If $p = ab$ for an irreducible $p$, why can't both $a$ and $b$ be
units?
\end{exercise}

In $\mathbb Z$ (which is an integral domain), the irreducible elements are
exactly those numbers $p$ which are not $1$ or $-1$ where $p = ab$ implies that
one of $a$ or $b$ is $1$ or $-1$, in which case the other must be $p$ or $-p$
--- these are exactly the usual prime numbers, of course.

\subsection{Finding irreducible polynomials}

Let's take $GF(7)$ as an example field (the rest of this section works equally
well with any finite field) and look at polynomials of low degrees in
$GF(7)[X]$.

\begin{itemize}
\item The polynomial $0$ is the zero element, so it is not irreducible ($0$ is
not a unit).
\item Polynomials of degree $0$ are sequences $(c_0)$ where $c_0$ is a non-zero
field element. All of these are units and therefore not irreducible (the same
reason that $1$ is not a prime).
\item Polynomials of degree $1$ can be written $aX + b$ for field elements $a,
b$ with $a \neq 0$. In the polynomial ring over a field without any polynomial
``modded out'', a polynomial of degree $1$ or higher cannot be a unit (there is
no ``$1/X$'' to remove the $X$). Since all non-zero non-units have degree of at
least $1$, if $a, b$ are two such polynomials then $ab$ has degree at least $2$
so all polynomials of degree $1$ are irreducible.
\item Degree $2$ is where it starts to get interesting.
Certainly, any polynomial that is the product of two degree-$1$ polynomials is
not irreducible. Since we can always multiply a polynomial through with the
inverse of the leading coefficient, let's consider only polynomials of the form
$X^2 + b X + c$. If such a polynomial factors, it will be without loss of
generality into the form $(X + u)(X + v)$ which gives $b = u + v$ and $c = uv$.
In a finite field, we could in principle make a table with columns $u, v, u +
v, uv$ for all values of $u$ and $v$. To check if a polynomial is irreducible,
we see if the pair $(b, c)$ appears in the $(u+v, uv)$ columns anywhere --- if
so, we have factored the polynomial, otherwise it is irreducible.

A simple counting argument now shows that there must always be an irreducible
polynomial of degree $2$ in a field of the form $GF(p)$. There are $p$ possible
values each for $b, c$ so there are $p^2$ quadratic polynomials with leading
coefficient $1$. Similarly, since our table runs through $p$ values each of $u$
and $v$, there are $p^2$ rows in the table. The argument is that if any two
rows repeat an $(u+v, uv)$ pair then at least one of the possible $(b, c)$
pairs cannot appear in the table at all. And indeed, for any distinct values of
$u$ and $v$, the rows starting $(u, v)$ and $(v, u)$ will have the same sum and
product. For example $(0, 1, 1, 0)$ and $(1, 0, 1, 0)$ both have the same sum
of $1$ and product of $0$.

\begin{center}
\begin{tabular}{llll}
$\mathbf{u}$ \phantom{$\mathbf{=u + v}$} &
$\mathbf{v}$ \phantom{$\mathbf{=u + v}$} &
$\mathbf{b = u + v}$ & $\mathbf{c = uv}$ \\
\midrule
0 & 0 & 0 & 0 \\
{\hbox to 0pt{\hspace{-24pt}$\Rightarrow$}}0
  & 1 & 1 & 0 \\
\ldots \\
0 & $p-$1 & $p-$1 & 0 \\
{\hbox to 0pt{\hspace{-24pt}$\Rightarrow$}}1
  & 0 & 1 & 0 \\
\ldots
\end{tabular}

\vspace{24pt}

\begin{minipage}{0.8\textwidth}\scriptsize
Table of all possible factorisations of quadratic polynomials, showing repeated
$b, c$ entries in two different rows.
\end{minipage}
\end{center}

As an example, the polynomial $X^2 + X + 6$ is irreducible over $GF(7)$.

\item The situation with polynomials of degree $3$ or higher is more complex
and we don't treat it here. Suffice it to say that irreducible polynomials of
any degree $>0$ always exist.
\end{itemize}

\begin{exercise}
$(\star\star)$ \emph{Irreducible polynomials.}
Find the the following irreducible polynomials:
\begin{enumerate}
\item Over $GF(2)$, all irreducible polynomials of degrees 2 and 3.
\item Over $GF(2)$, one irreducible polynomial of degree 4.
\item Over $GF(3)$, all irreducible polynomials of degree 2.
\item Over $GF(5)$, one irreducible polynomial of degree 3.
\end{enumerate}
\end{exercise}

\subsection{Example: $GF(7^2)$}

Let's look at some example finite fields. To construct $GF(7^2)$ we take
$\mathbb F_7[X]/(X^2+X+6)$, giving $49$ field elements which we can represent
as pairs $(a, b)$ or equivalently, as linear polynomials $(a+bX)$. Addition in
this field is just component-wise addition in $\mathbb F_7$. To multiply two
elements $(a, b)$ and $(c, d)$, viewing them as polynomials we get $bdX^2 + (bc
+ ad) X + ac$ which we have to reduce modulo $X^2+X+6$. So we factor out $bd$
and rewrite the product as
\[
bd \times (X^2 + X +6) + \left(bc+ad - bd\right) X +
\left(ac - 6 bd\right)
\]
for the linear and constant terms, we have ``telescoped'' out the required
factor $bd$. This gives us the following multiplication formula for this
particular representation of the field, using $-6 = 1$:
\[
(a, b) \times (c, d) = (ac+bd, bc+ad-bd)
\]

Remember the finite field with 9 elements from a couple of lectures ago?
We can now see that this is nothing else than $GF(3)[X]/(X^2+1)$ with the
element $A$ representing the value $X$. Indeed, when we talked about pairs
$(a, b)$ then such a pair represented $a + bX$.

%\subsection{Automorphisms of $GF(7^2)$}

%Let's find the automorphisms of $GF(7)[X]/(X^2+X+6)$, that is the functions $f$
%on this domain with $f(x+y) = f(x) + f(y)$, $f(0) = 0$, $f(1) = 1$ and $f(xy) =
%f(x)f(y)$. All that we need to determine an automorphism is $f(X)$, since for
%any element $(a, b)$ of the field we have $f(a+bX) = a + b \times f(X)$.

%We start with setting $f(X) = u + vX$ for variables $u, v$. Then we have $f(X)
%\times f(X) = (u+vX)(u+vX) = u^2 + 2uvX + v^2(X^2+X+6) - v^2(X+6) = (u^2+v^2) +
%(2uv-v^2)X$. However, we also have $f(X)\times f(X) = f(X^2) = f(-X-6) = (1-u) -
%vX$, giving us the equations $1-u = u^2+v^2$ and $-v = 2uv - v^2$. The last
%equation gives us two cases: either $v = 0$, which is definitely not an
%automorphism, or $v \neq 0$ in which case we divide by $v$ to get $-1 = 2u-v$
%and substitute to get the quadratic equation $1-u = u^2 + (2u+1)^2$ which gives
%us the solutions $u = 0, v = 1$ and $u = 6, v = 6$. Our automorphisms are
%$f_1(X) = X$ and $f_2(X) = 6 + 6X$, from which we find $f_1(a + bX) = a + bX$
%--- the identity function, which is not surprising --- and $f_2(a+bX) = (a+6b)
%+ 6bX$.

%\begin{exercise}
%$(\star)$ Why can $v = 0$ in the above calculation not yield an automorphism?
%\end{exercise}

%\subsection{Isomorphisms in $GF(7^2)$}

%We said that there is only one finite field $GF(p^n)$ up to isomorphism for
%each prime $p$ and positive integer $n$. Let's look at some examples of this
%too.

%For $GF(7^2)$, another representation of the same field comes from choosing a
%different irreducible polynomial, such as $Y^2 + 1$ which gives the
%multiplication $(a, b) \odot (c, d) = (ac-bd, ad+bc)$.

%Let's try and compute the isomorphisms
%\[f: GF(7)[X]/(X^2+X+6) \to GF(7)[Y]/(Y^2+1) \]
%We will use the symbol $X$ for elements in the first representation and
%the symbol $Y$ for elements in the second; this way the symbol name tells us
%which polynomial we have to use when reducing elements after multiplication.

%We know that $f(1, 0) = (1, 0)$ and thus that $f(a, 0) =
%(a, 0)$ for any field element $a \in GF(7)$ since $1$ is a generator of
%$(\mathbb Z_7, +)$. So all we need to find is $f(0, 1) = f(X)$ since $f(a + bX)
%= a + b \times f(X)$. Writing $f(X) = u + vX$ for variables $u, v$ ranging over
%$GF(7)$, for any element $(a, b)$ we have $f(a, b) = (a + ub, vb)$.
%Now look at the equation $f(a, b) \odot f(c, d) = f( (a, b) \times (c, d))$
%that any isomorphism must satisfy. Writing this out and combining terms gives
%the conditions $2uv = -v$ and $u^2 - v^2 = 1 - u$ which give $u = 3$ and $v = 2
%\vee v = 5$.  So we have two isomorphisms
%\[\begin{array}{l}
%f_1: (a, b) \mapsto (a + 3b, 2b) \\
%f_2: (a, b) \mapsto (a+3b, 5b)
%\end{array}\]
%We can describe both these isomorphisms by their action on the polynomial $(0,
%1)$ that represents the monomial $X$: $Y_1 = f_1(X) = 3+2X$ and $Y_2 = f_2(X) =
%3+5X$.

%\begin{exercise}
%$(\star\star)$ \emph{The field $GF(5^2)$.}
%\begin{enumerate}
%\item Find the explicit multiplication formula for the representation of
%$GF(5^2)$ modulo the irreducible polynomial $X^2 + 2X + 3$.
%\item Do the same for the irreducible polynomial $2Y^2 + 4Y + 1$.
%\item Find the isomorphisms from the first representation to the second.
%\end{enumerate}
%\end{exercise}

\subsection{Example: $GF(2^8)$}

For $GF(2^8)$, our field with $256$ elements, we take the irreducible
polynomial
\[ p(X) = X^8 + X^4 + X^3 + X + 1 \]
as an example. In addition to tuples and expressions with a variable $X$, we
have a third representation of $GF(2^8)$ as $8$-bit strings with the lowest
coefficient rightmost, i.e. the polynomial $X^3 + X + 1$ which is $(1, 1, 0,
1)$ as a tuple can be written \texttt{00001011}.  The operations on the
individual bits, as elements of $GF(2)$, are:

\begin{center}
\begin{tabular}{l|ll}
$+$ & 0 & 1 \\
\hline
0 & 0 & 1 \\
1 & 1 & 0
\end{tabular}
\qquad\qquad
\begin{tabular}{l|ll}
$\times$ & 0 & 1 \\
\hline
0 & 0 & 0 \\
1 & 0 & 1
\end{tabular}
\end{center}

These are, of course, the binary exclusive-or (XOR) and AND operations.
Addition of tuples is component-wise; for multiplication we could write out the
formula as for $GF(7^2)$ but this becomes cumbersome. Instead, we give an
example. First, we consider the multiplication of binary polynomials without
any polynomial modulus.
The special thing about working over $GF(2)$ is that $1 + 1 = 0$ so all
coefficients in our polynomials are either present or absent but we don't have
to worry about field multiplication too much.

Suppose we want to compute the product of the polynomials represented by the
bytes \texttt{10001010} and \texttt{00101101} in $\mathbb F_2[X]$.
Just like ``normal'' multiplication of bytes (as perfomed by the x86
\texttt{MUL} operation), the result will be a 2-byte value.
Writing these out,
\[\begin{array}{rcl}
\texttt{10001010} & = & X^7 + X^3 + X \\
\texttt{00101101} & = & X^5 + X^3 + X^2 + 1
\end{array}\]

we can factor out the second operand and write this multiplication in the form
\[
X^7 (X^5 + X^3 + X^2 + 1) + X^3 (X^5 + X^3 + X^2 + 1) + X (X^5 + X^3 + X^2 + 1)
\]
Each left-hand side of a product in this term is a monomial with coefficient
$1$ (the only nonzero element of the base field). But multiplying with $X^k$
like this is just shifting the right-hand factor to the left by $k$ bits. So we
can do polynomial multiplication by repeated addition in the usual longhand
way:

\[
\begin{array}{rcr}
\texttt{10001010} \times \texttt{00101101} & = & \texttt{0 0101101.} \\
 & + & \texttt{001 01101...} \\
 & + & \texttt{0010110 1.......} \\
\cline{2-3}
 & & \texttt{00010111 10110010}
\end{array}
\]
which is the polynomial $X^{12} + X^{10} + X^9 + X^8 + X^7 +X^5 +X^4 + X$.

To take such a polynomial modulo $X^8 + X^4 + X^3 + X + 1$, we write the two
bytes that make up the product as $(hi, lo)$ so the polynomial is actually $hi
\times X^8 + lo$. Since $lo$ is of degree at most $7$ it does not need to be
reduced any further. For $hi$, we write out the division with remainder by the
modulus polynomial $p(X)$ of all higher powers. For example, $X^8 = 1 \times
(X^8 + X^4 + X^3 + X + 1) + (X^4 + X^3 + X + 1)$.

\[\begin{array}{rrrr}
\textbf{power} & \textbf{q} & \textbf{r} & \text{binary }\textbf{r} \\
X^8 & 1 & X^4 + X^3 + X + 1 & \texttt{00011011} \\
X^9 & X & X^5 + X^4 + X^2 + X & \texttt{00110110} \\
X^{10} & X^2 & X^6 + X^5 + X^3 + X^2 & \texttt{01101100} \\
X^{11} & X^3 & X^7 + X^6 + X^4 + X^3 & \texttt{11011000} \\
X^{12} & X^4 + 1 & X^7 + X^5 + X^3 + X + 1 & \texttt{10101011} \\
X^{13} & X^5 + X + 1 & X^6 + X^3 + X^2 + 1 & \texttt{01001101} \\
X^{14} & X^6 + X^2 + X & X^7 + X^4 + X^3 + X & \texttt{10011010} \\
X^{15} & X^7 + X^3 + X^2 + 1 & X^5 + X^3 + X^2 + X + 1 & \texttt{00101111}
\end{array}\]

With this table, we can just add the $lo$ component of our product to the
remainders of all the powers present in the $hi$ component:

\[\begin{array}{rrr}
& \texttt{10110010} & (lo) \\
+ & \texttt{00011011} & X^8 \\
+ & \texttt{00110110} & X^9 \\
+ & \texttt{01101100} & X^{10} \\
+ & \texttt{10101011} & X^{12} \\
\cline{1-2}
& \texttt{01011000}
\end{array}\]

This gives us our result in $GF(2^8)$, represented with the irreducible
polynomial $X^8 + X^4 + X^3 + X + 1$, of $\texttt{10001010} \times
\texttt{00101101} = \texttt{01011000}$ or $(X^7 + X^3 + X)\times (X^5+X^3 + X^2
+ 1) = (X^6 + X^4 + X^3)$.

\subsection{Implementation of $GF(2^8)$ multiplication}

Here is binary multiplication in $\mathbb F_2[X]/p(X)$ written as \texttt{C}
code, where \texttt{u8} is an unsigned $8$-bit integer datatype and
\texttt{bool} is a boolean datatype (\texttt{int} would do fine as well):

\begin{verbatim}
/*
Multiply two values a, b in GF(2^8) represented by
p(X) = X^8 + X^4 + X^3 + X + 1  (0x1b).
*/
u8 mul(u8 a, u8 b)
{
    u8 x, y, r;
    bool carry;
    x = a;
    y = b;
    r = 0x00;
    while (x)
    {
        if (x & 0x01) { r ^= y; }
        carry = y & 0x80;
        y <<= 1;
        x >>= 1;
        if (carry) { y ^= 0x1b; }
    }
    return r;
}
\end{verbatim}

The result accumulates in \texttt{r}. Each pass through the loop looks at the
low-order bit of \texttt{x} with \texttt{x \& 0x01} and if set, adds the
current multiple of \texttt{b} (which is stored in \texttt{y}) to \texttt{r}.
Afterwards, we shift \texttt{x} one position to the right to get the next bit.
After each loop iteration, we shift \texttt{y} one position to the left,
representing a multiplication by the polynomial $X$. If this overflows, we have
to reduce \texttt{y} modulo $p(X)$ which has the binary representation
\texttt{0x1b = 00011011}.

\begin{diamondsec}
The \texttt{C} language does not offer a way to check for carries except with
an explicit variable (\texttt{carry = y \& 0x80} checks if the high bit of
\texttt{y} is set). In an assembler implementation, this could be handled much
better by a branch-if-carry instruction using the processor's carry flag. The
small number of constants and local variables involved would also suggest
implementing the entire algorithm in the processor's registers.
\end{diamondsec}

%\subsection{Automorphisms of finite fields}

%The group of automorphisms of a finite field can be found with the following
%theorem:

%\begin{theorem}
%The group of automorphisms of $GF(p^n)$ is isomorphic to the group $(\mathbb
%Z_n, +)$ and the Frobenius map $X \mapsto X^p$ is a generator of the
%automorphism group.
%\end{theorem}

%In a finite field represented as $GF(p)[X]/q(X)$, we can compute $f(X)$ for all
%the automorphisms by repeatedly applying the Frobenius map giving $X^p, X^{p^2},
%X^{p^3}, \ldots$ and reducing modulo $q(X)$.

%In our case, $p = 2$ and we compute the powers of the Frobenius map for
%representations of $GF(2^8)$ modulo $p(X) = X^8 + X^4 + X^3 + X + 1$ and $q(Y)
%= Y^8 + Y^4 + Y^3 + Y^2 + 1$.

%\begin{center}
%\begin{tabular}{lll}
%$n$ & mod $p(X)$ & mod $q(Y)$ \\
%\midrule
%0 & $X$ & $Y$ \\
%1 & $X^2$ & $Y^2$ \\
%2 & $X^4$ & $Y^4$ \\
%3 & $X^4+X^3+X+1$ & $Y^4+Y^3+Y^2+1$ \\
%4 & $X^6+X^4+X^3+X^2+X$ & $Y^6+Y^3+Y^2$ \\
%5 & $X^7+X^6+X^5+X^2$ & $Y^7+Y^4+Y^3+Y^2+1$ \\
%6 & $X^6+X^3+X^2+1$ & $Y^6+Y^4+Y^3+Y^2+Y+1$ \\
%7 & $X^7+X^6+X^5+X^4+X^3+X$ & $Y^7+Y^2+1$
%\end{tabular}
%\end{center}

%\subsection{Isomorphisms of $GF(2^8)$}

%Next, let's look for the isomorphisms of $GF(2^8)$ from the representation
%modulo $p(X) = X^8 + X^4 + X^3 + X + 1$ to another representation, for example
%$q(Y) = Y^8 + Y^4 + Y^3 + Y^2 + 1$ (this is another irreducible polynomial;
%note the $Y^2$ in place of the $X$).
%That is, we're looking for an isomorphism $f$ that maps field elements to other
%field elements such that $f(a \times b) = f(a) \odot f(b)$ where $\odot$ is
%multiplication modulo $q(Y)$ and $\times$ is multiplication modulo $p(X)$.
%Again, the value $f(X)$ will determine an isomorphism $f$ from the
%representation modulo $p(X)$ to the representation modulo $q(Y)$.

%If we find any one isomorphism from $GF(2)[X]/p(X)$ to $GF(2)[X]/q(Y)$ then we
%can get the whole set of isomorphisms by composing our one isomorphism with
%these automorphisms.

%To get an isomorphism $f$, it is enough to find $f(X)$ which completely
%determines the isomorphism. To find this, we have to briefly work with $p$ as a
%polynomial over $GF(2^8)$. So far, we have considered polynomials over $GF(2)$
%to represent elements of $GF(2^8)$, i.e. our polynomials had coefficients in
%$GF(2)$. Now, we consider polynomials with coefficients in $GF(2^8)$. This can
%seem confusing at first because we will need two variable symbols: one to
%represent the ``variable'' of the polynomial and one to represent elements of
%$GF(2^8)$. For example, if $a$ and $b$ are elements of $GF(2^8)$ then $f(X) =
%aX+b$ is a polynomial over $GF(2^8)$ with coeffcients $a, b$. Suppose that $a =
%Y^2 + 1$ and $b = 2Y$, so we are using the letter $Y$ to represent elements,
%then $f(X) = (Y^2+1)X + 2Y$.  $f$ is still a degree-one polynomial in one
%variable $X$; we just needed another variable symbol $Y$ to write some of the
%coefficients which are elements of $GF(2^8)$.

%In $GF(2^8)$ represented modulo $p$, we have $p(X) = 0$. So for any isomorphism
%$f$ out of this representation we also have $f(p(X)) = 0$. But since $f$ is an
%isomorphism we must also have $p(f(X)) = 0$. This means that $f(X)$, which is
%an element of the representation modulo $q$, must be a zero of the polynomial
%$p$ modulo $q$. So our recipe for finding isomorphisms is to factor the
%polynomial $p(X)$ in the representation modulo $q(Y)$, where we treat $X$ as a
%variable. In other words, we are looking for elements $b$ such that $(X - b)$
%divides $p(X)$ modulo $q(Y)$.

%\begin{theorem}
%A function $f$ is an isomorphism from $GF(z^n)$ represented modulo $p(X)$ to
%$GF(z^n)$ represented modulo $q(Y)$ if and only if $f$ commutes with addition
%and multiplication, $f(1) = 1$ and for $b = f(X)$, $(X - b)$ divides $p(X)$ as
%polynomials modulo $q(Y)$.
%\end{theorem}

%One of the isomorphisms has $b = Y + 1$.
%We can check this by computing $p(X) / (X - (Y+1))$ modulo $q(Y)$ and find

%\[
%\begin{array}{lrl}
%\multicolumn{3}{l}{(X^8 + X^4 + X^3 + X + 1) = (X - (Y+1))\times(} \\
%& 1 & X^7 \\
%& + (Y+1) & X^6 \\
%& + (Y^2+1) & X^5 \\
%& + (Y^3+Y^2+Y+1)&  X^4 \\
%& + Y^4 & X^3 \\
%& + (Y^5 + Y^4+1) & X^2 \\
%& + (Y^6 + Y^4 + Y + 1) & X \\
%& + (Y^7 + Y^6 + Y^5 + Y^4 + Y^2) \\
%) \pmod{q(Y)}
%\end{array}
%\]

%\newcommand{\x}[1]{\texttt{0x#1\,}}

%If the mixture of $X$ and $Y$ variables is confusing, we can also represent
%elements of $GF(2)[Y]/(Y+1)$ as two-digit hexadecimal numbers. In this case, $b
%= \x{03}$ and the above equation is
%\[
%\begin{array}{l}
%(X^8 + X^4 + X^3 + X + 1) = (X - \x{03})(X^7 + \x{03} X^6 + \x{05} X^5 +
%\x{0f} X^4 + \\ \x{10} X^3 + \x{31} X^2 + \x{53} X + \x{f4}) \pmod{q(Y)}
%\end{array}
%\]

\begin{exercise}
$(\star\star)$ \emph{Computation in $GF(2^8)$.}
Let $p(X) = X^8+X^4+X^3+X+1$.
\begin{itemize}
\item Compute $(X^7 + X + 1)(X^6 + X^3 + X) + (X^7 + X^2 + 1)$ in $GF(2^8)$
using the representiation modulo $p(X)$.
\item Solve the equation $X^3 + X + 1 = W\times(X^5 + 1)$ for $W$ in $GF(2^8)$
represented modulo $p(X)$. Note: $W$ is a polynomial, not an integer.
% \item Compute the powers of the Frobenius map in $GF(2^8)$ modulo $r(Z) = Z^8 +
% Z^7 + Z^2 + Z + 1$ (this is irreducible).
% \item Find an isomorphism from $GF(2)[Z]/r(Z)$ to $GF(2)[X]/p(X)$.
\end{itemize}
\end{exercise}

%\begin{exercise}
%$(\star)$ \emph{Computation in $GF(2^3)$.}
%Once you have mastered finite fields, you will be expected to solve exercises
%like this one for small enough fields almost as easily as arithmetic on
%integers. This exercise contains a lot of computations, each of which should be
%quick and easy.

%We consider the field $GF(2^3)$ in the representations modulo the irreducible
%polynomials $p(X) = X^3 + X + 1$ and $q(Y) = Y^3 + Y^2 + 1$.

%\begin{enumerate}
%\item Reduce $X^3$ and $X^4$ modulo $p(X)$ and $Y^3$ and $Y^4$ modulo $q(Y)$.
%\item How many elements does the group of automorphisms of $GF(2^3)$ have? What
%are these elements ``called''?
%\item Compute the Frobenius map for the representations modulo $p(X)$ and
%$q(Y)$. The quickest way to do this is to start with $\phi(X) = X^2$ and find
%$\phi(X^2)$, then express $\phi$ for an arbitrary field element as
%$\phi(aX^2+bX+c) = u X^2 + vX + w$, i.e. find $u, v, w$ in terms of $a, b, c$.
%\item Do the same for all powers of the Frobenius map, in both representations
%(hint: there aren't too many.)
%\item Find all the isomorphisms from the representation modulo $p(X)$ to the
%representation modulo $q(Y)$.
%Hint: how many are there? Find one isomorphism, then derive the others as
%follows: if $f$ is one isomorphism and $a$ is an automorphism of $GF(2^3)$
%represented modulo $p(X)$, then $g = f \circ a$ is an isomorphism too. So
%compute $f(a(X))$ to get the value of $g(X)$.
%\item $(\star\star)$ Find the explicit multiplication formulas in both
%representations. That is, for $(aX^2 + bX + c)(dX^2+eX+f) = (uX^2 + vX+w)$ find
%$u, v, w$ in terms of $a$ to $f$. Repeat the same for $Y$.
%\end{enumerate}
%\end{exercise}

\subsection{Roots of unity}

The multiplicative group $(\mathbb F^\times, \times)$ of a field $\mathbb F$
contains all elements except 0.
A theorem in Algebra says:

\begin{theorem}
A finite subgroup of the multiplicative group of a field is cyclic.
\end{theorem}

For finite fields, the whole group is finite so we get a corollary:

\begin{corollary}
The multiplicative group of a finite field is cyclic.
\end{corollary}

This means among other things that there is always an element $a \neq 0$ in any
finite field that generates the multiplicative group, e.g. $a, a^2, a^3 \ldots$
iterates over all elements except 0.

For a finite field $GF(p^n)$ with $p > 2$, since the size of the field is odd,
the multiplicative group has an even order $p^n - 1$. On the other hand, for
a group $GF(2^n)$ the multiplicative group has an odd order.

In any case, we can use the order of this group to invert elements in a finite field.

\begin{proposition}
In a finite field $GF(p^n)$, for any field element $a$ we have $a^{(p^n - 1)} = 1$.
This implies that $a^{(p^n-2)} = 1/a$.
\end{proposition}

Although $p^n-2$ may be a large number, the square-and-multiply algorithm can
perform this exponentiation efficiently.

\begin{definition}[root of unity]
A field element $x$ is called a $k$-th root of unity if $x^k = 1$ in the field,
for a positive integer $k$.
If $x^k = 1$ and $x^m \neq 1$ for all $1 \leq m < k$, we say that $x$ is a
primitive $k$-th root of unity. (In this case, $k$ is the order of $x$ in the
multiplicative group $(\mathbb F \setminus \{0\}, \cdot)$ of the field.)
\end{definition}

\begin{diamondsec}
In infinite fields, being a root of unity is a stricter condition than being
a unit, for example in the complex numbers $2$ is a unit but not a root of
unity; the complex roots of unity are all located on the unit circle.
\end{diamondsec}

In finite fields, all nonzero elements are roots of unity. Over the real 
numbers, a polynomial of degree $d$ cannot have more than $d$ roots and the same
holds over any finite field. A $k$-th root of unity $x$ satisfies $x^k = 1$ so
it must be a root of the polynomial $(X^k-1)$, which means that there cannot be
more than $k$ elements that are $k$-th roots of unity.

Further, Lagrange's theorem tells us that the order of a group element must
divide the order of the group, which further restricts what kinds of roots of
unity can occur in a finite field.

\textbf{Example.}
Consider the field $GF(3^2)$. Without picking any particular representation
(e.g. irreducible polynomial), we know that the multiplicative group has order
$8$ so there can be $1$st, $2$nd and $4$th roots of unity but there will be no
roots of of the polynomial $X^3 - 1$ except $1$. In fact, this particular
polynomial factors as $(X-1)^3$ modulo $3$.

%On the other hand, modulo $3$ we have $(X^5-1) = (X-1)(X^4 + X^3 + X^2 + X + 1)$
%and we know the left-hand side cannot have any roots except $1$, but the right
%factor on the right-hand side does not have a root at $1$. Therefore this
%polynomial must be irreducible modulo $3$.

\subsection{The Frobenius map}

In a finite field of characteristic $p$ (which means $GF(p^n)$ for some $n \geq 
1$), we know that $a^{(p^n)} = a$ for all elements $a \neq 0$. This means that 
the function $\phi(a) = a^p$ must be bijective: if you apply it $n$ times in 
sequence, you get the identity function back, so it must be injective, and an 
injective function whose domain and range are both finite and the same size 
must also be surjective. (The domain and range in this case are $\mathbb F$, 
and $\phi(0) = 0$.)

This function has the property that $\phi(a \times b) = (a \times b)^p = a^p 
\times b^p = \phi(a) \times \phi(b)$. So far, nothing suprising. Let's try the 
same with addition: \[ \phi(a + b) = a^p + p a^{p-1}b + \binom{p}{2} a^{p-2}b^2 
+ \ldots + b^p \] except that in a finite field with characteristic $p$, we 
have $p = 0 \bmod p$ so all terms with a $p$ factor (but not exponent) vanish, 
giving $(a+b)^p = a^p + b^p$. That is not a typing mistake: only over a finite
field, and only for the particular $p$ that represents its characteristic,
you can pull a $p$ in the exponent out of brackets of sums. This is important
enough to give it a name:

\begin{definition}
In a finite field of characteristic $p$, the function $\phi(a) = a^p$
is called the \hi{Frobenius automorphism}. It has the properties
$\phi(0) = 0, \phi(1) = 1$, $\phi(a\times b) = \phi(a) \times \phi(b)$
and $\phi(a + b) = \phi(a) + \phi(b)$ for all $a, b$ in the field.
\end{definition}

\ifdefined\booklet
\else
\end{document}
\fi
