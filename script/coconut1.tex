\section*{Preliminaries}

\begin{diamondsec}
More advanced material that may help interested students to further understand
the topics but can safely be skipped the first few times you read the notes is
marked by the $\diamond$ symbol and set in a smaller font, just like this.
\end{diamondsec}

\noindent\emph{Note on exercises.}
All exercises are graded with one to three stars according
to the following scheme.

\begin{description}
\item[$\star\phantom{\star\star}$]
A simple comprehension exercise. If you have understood the topic that this
question is about, you should be able to answer it in a matter of minutes or
even less than that.

\item[$\star\star\phantom{\star}$]
A standard exercise, may require a bit of thinking but should not take hours to
solve if you've understood what it's about. At the end of the course and after
revision, you should be able to solve two star exercises without too much
trouble.

\item[$\star\star\star$]
An advanced exercise. Three star exercises can require a lot of work or a
deeper understanding of the mathematical principles behind an idea. Leave these
for last. You are not meant to solve all of these, nor will you be able to
unless you have a lot of spare time.
\end{description}

\noindent
Before we start: in this course, the natural numbers $\mathbb N$ are the
numbers $0, 1, 2, 3, \ldots$ and the integers $\mathbb Z$ are the natural
numbers together with the negative whole numbers: $\ldots, -3, -2, -1, 0, 1, 2,
3, \ldots$

\section{Groups}

The first few lectures of this unit cover some of the fundamental
notions of algebra: groups, rings and fields.

\subsection{Motivation}

A group is something ``a bit like adding numbers'' --- there are elements which
are a bit like numbers and there is something you can do to them that is a bit
like adding.

There are a lot of things ``like numbers'', for example:
\begin{itemize}
\item Numbers, including variations such as integers, rational numbers, real
numbers, complex numbers etc.
\item Integer datatypes as used in most programming languages with a fixed
bit\-length. Actually, we'll mostly be sticking to unsigned integers in this
course.
\item Pairs, triples and other tuples or vectors of any of the above kinds of
numbers.
\end{itemize}

Let's recall some things we know about addition:

\begin{enumerate}
\item You can add numbers in any order and get the same result. If you're asked
to mentally add $5 + 3 + 7$, one way to do this if you know that
$3 + 7 = 10$ is can do the sum as $5 + (3 + 7) = 5 + 10 = 15$.

\item You can cancel additions on both sides of equations. If you know that
$x + 5 = 10 + 5$, you can write $x\ \cancel{+5} = 10\ \cancel{+5}$ to get
$x = 10$.
\end{enumerate}

Anyone writing an optimizing compiler will be spending a lot of time on such
rules to turn arithmetic expressions in programs into efficient machine code.
But there is a big difference between numbers as we know them and numbers as
most languages' int(eger) types use them: computer integers have a fixed size,
for example 64 bits, and integers can ``wrap around'': by computing $1 + 1 +
\ldots$, you eventually end up at $0$ again.

Let's look at $8$-bit unsigned integers. The smallest such value is $0$ and the
largest $2^{8} - 1 = 255$, represented by binary $1111\ 1111$.  There are many
differences to normal integers. For example, in $8$-bit integers it does not
make sense to speak of ``positive'' and ``negative'' numbers: $128 + 128 = 0$
so we can't call $128$ ``positive'' or ``negative'' according to the usual
rules (the sum of positive/negative numbers is again positive/negative).
Multiplying $8$-bit integers is even less like working with the usual integers.
For example, $16 \times 16 = 0$ so the product of two non-zero values can become
zero. Also, $16 \times 32 = 0$ so we can't cancel in multiplications any more:
$16 \times x = 16 \times y$ does not imply $x = y$.  Do the two rules we gave
above still hold for $8$-bit integers? For example, if we know $16 + x = 16 +
y$ in $8$-bit integers, can we still conclude that $x=y$? It turns out that we
can.  Mathematically, this difference between adding and multiplying in $8$-bit
integers can be expressed by saying that for the usual integers, both addition
and multiplication (excluding $0$) are group operations; for the $8$-bit
integers addition is a group operation but multiplication is not (whether or
not you exclude $0$).

A quick outlook on where we're going in the algebra section of this unit: 
can we construct operations on the set of $8$-bit
unsigned integers (or any other bitlength for that matter) such that we can
add, subtract, multiply and divide (except by $0$) according to more or less
the usual rules? It will turn out that we can and that there is ``essentially''
only one way to do this. What ``essentially'' means here we will also
investigate.

\subsection{Definition of a group}
Half of learning algebra is deciphering the notation. We give the definition of
a group and an alternative ``programmer's notation'' version.

\begin{definition}[group]
A group $\Grp = (G, +)$ consists of a set $G$ and an operation $+: G \times G
\to G$ that has the following properties.
\begin{description}
\item[associative] For any elements $g, h, k$ of $G$ we have $(g + h) + k =
g + (h + k)$.
\item[neutral element] There is an element $e$ of $G$ with the property that for
all elements $g$ of $G$ we have $e + g = g$ and $g + e = g$. We call such an
element a neutral element of the group $(G, +)$.
\item[inverses] For any element $g$ of $G$ there is an element $h$ of $G$ such
that $g + h = e$ and $h + g = e$ where $e$ is the neutral element. We
call $h$ the inverse of $g$ and write $h = (-g)$.
\end{description}
\end{definition}

An alternative definition of a group is a datatype or ``class'' \texttt{G} with
an equality operation\footnotemark \texttt{==} and a function with signature
(in C style notation) \texttt{ G add(G a, G b)} with the following properties.

\textbf{associative} For any elements \texttt{g,h,k} of \texttt{G} we have 
\texttt{add(add(g, h), k) == add(g, add(h, k))}.

\textbf{neutral element} There is a function \texttt{G neutral(void)} with the
property that for all \texttt{g} of type \texttt{G} we have \texttt{g ==
add(g, neutral())} and \texttt{g == add(neutral(), g)}.

\textbf{inverses} There is a function \texttt{G invert(G x)} such that for any
element \texttt{g} of type \texttt{G} we have \texttt{add(g,invert(g)) ==
neutral()} and \texttt{add(invert(g),g) == neutral()}.

\footnotetext{For the usual equality operation that we are used to this ``just
works''. The actual condition is that the equality operation must be an
equivalence relation, but this requires a bit of formalism to do correctly which
we'll sweep under the carpet for now.}

In this definition it is important that the functions \texttt{add},
\texttt{neutral} and \texttt{invert} are functions in the mathematical sense
and not ``procedures'', i.e. they maintain no state between calls and do not
have access to an environment such as a source of randomness.  Whether you
write the group operation as a function \texttt{add} or an infix operator $+$
is a matter of notation and does not change whether or not something forms a
group.

\subsection{Rules for groups}

These definitions immediately give the following rules for working in groups,
among many others.
\begin{proposition} \label{prop:grouprules}
In a group:

\begin{enumerate}
\item There can only be one neutral element. If two elements in a group both
have the properties of a neutral element, then they are equal.
\item An element can only have one inverse. If $g + h = e$ and $g + k = e$ then
$h = k$. The same holds if $h + g = e$ and $k + g = e$.
\item More generally, you can cancel in equations over groups: if
$g + h = g + k$ then $h = k$. Similarly, $h + g = k + g$ implies $h = k$ too.
\item The inverse of a sum is the sum of the inverses, the other way round:\\
$(-(g+h)) = (-h) + (-g)$. \\ In `CS' notation:
\texttt{invert(add(g, h)) = add(invert(h), invert(g))}.
\item Inverting an element twice gives the same element back: 
$(-(-g)) = g$.
\end{enumerate}
\end{proposition}

We will prove points 3 and 4. Points 1 and 2 are special cases of 3 and
point 5 is similar to point 4. First, point 3:

\begin{center}
\begin{tabular}{llrcl}
given                  & (1) & $g + h$ & $ = $ & $g + k$ \\
inverses               & (2) & \multicolumn{3}{l}{There exists an element $(-g)$} \\
add  $(-g)$            & (3) &  $(-g) + (g+h) $ & $=$ & $(-g) + (g + k)$ \\
associativity on left  & (4) & $((-g) + g) + h $ & $= $ & $(-g) + (g + k)$ \\
associativity on right & (5) & $((-g) + g) + h $ & $= $ & $((-g) + g) + k$ \\
inverses on left       & (6) & $e + h $ & $=$ & $((-g) + g) + k$ \\
inverses on right      & (7) & $e + h $ & $=$ & $e + k$ \\
neutral on left        & (8) & $ h $ & $= $ & $e + k$ \\
neutral on right       & (9) & $ h $ & $= $ & $k$
\end{tabular}
\end{center}

A word on the structure of this proof. The general format of the proof
is ``equation implies equation'', so we can start from the first equation
and do valid transformations until we get to the target equation. In the 
second step to get to line (3), we add $(-g)$ on both sides; but to be precise
we first have to use the inverses rule in line (2) to ensure that such an element exists.
In all the remaining steps, we are applying term transformations which we
can do on one side of the equation at a time.

In contrast, point 4 has the structure ``term is equal to term''.
We can't do equation transformations here because the equation is the thing
that we want to prove in the first place, we don't start with a given
equation. Instead, we do this:

\begin{center}
\begin{tabular}{ll}
 & $\phantom{=} ((-h) + (-g)) + (g + h)$ \\
associativity & $= (-h) + ((-g) + (g+h))$ \\
associativity & $= (-h) + (((-g) + g)+h)$ \\
inverses      & $= (-h) + (e + h)$ \\
neutral       & $= (-h) + h$ \\
inverses      & $= e$
\end{tabular}
\end{center}

So $(-h) + (-g)$, when added to $(g+h)$, gives the neutral element $e$.
But $(-(g+h))$ also gives $e$ when added to $(g+h)$ because that is the
definition of an inverse (represented here by a minus sign).
By point 2, this means the two terms must be equal, that is
$(-h) + (-g) = (-(g+h))$.

\begin{exercise} $({\star}{\star})$
\emph{Rules in groups.} Prove the rest of Proposition \ref{prop:grouprules}.
\end{exercise}

\emph{Note on associativity.}
If you are given a sum like $1 + 2 + 3$, do you stop to wonder if it's meant to
be $(1+2)+3$ or $1+(2+3)$? Probably not, you know that this ``doesn't matter''
--- which is just another way of saying that normal addition is associative.
However, $(1-2)-3$ is not the same as $1-(2-3)$.
Compilers need to be told things like this. Most programming languages have a
table of all their infix operators somewhere, each listed with its precedence
and whether it is left- or right-associative. In the C language, $+$ and $-$ are
left-associative so $a + b + c$ will be compiled as $(a + b) + c$ and $a - b-c$
is $(a-b)-c$ as you would expect.
Since $+$ on integers is associative, an optimizing compiler is free to
rearrange the sum. For the purpose of this unit, the terms
``left-associative'' and ``right-associative'' don't exist --- either an
operation is associative in which case it makes no difference, or an operation
is not associative and we will bracket it or not write it ``infix'' at all.

\subsection{Commutative groups}

Something that we have omitted to mention until now is that $a + b = b + a$
on normal and fixed-bitlength integers. This is not the same thing as
asssociativity --- it is a new property called commutativity and we will see
groups very soon that are not commutative.

\begin{definition}[commutative group]
A group $(G, +)$ is commutative if for all $g, h$ in $G$ we have
$g+h = h+g$.
\end{definition}

A group that is commutative is sometimes also called Abelian after the
mathematician N. H. Abel.

Note that in the proof of point 4 of Proposition \ref{prop:grouprules} above,
we were careful to say that $-(g+h)$ is the same as $(-h) + (-g)$ where
the two variables were the opposite way round --- not $(-g) + (-h)$.
In a commutative group this would not matter, but in a non-commutative
group this makes a difference.

The first example of a group, and one that you should always have at the
back of your head, is $(\mathbb Z, +)$ which is the usual integers with
addition as the operation. This is a commutative group, of course.

%\subsection{Some exercises}

\begin{exercise} $(\star)$
\emph{Basic groups.}
\begin{enumerate}
\item Why is $(\mathbb N, +)$ not a group, which group laws does it obey and
which ones not? (Note: a structure that obeys the same subset of group laws as
$(\mathbb N, +)$ is called a monoid.)
\item Why is $(\mathbb Q, \times)$ --- the rational numbers with multiplication
--- not a group?
\item Why can you not have a group with $0$ elements?
\item Can you have a group with one element?
\end{enumerate}
\end{exercise}

\begin{exercise} $(\star\star)$
\emph{More basic groups.}
\begin{enumerate}
\item What minor change do you have to make to the elements of 
$(\mathbb Q, \times)$ to turn it into a group?
\item Describe what a group with two elements must look like.
\item Which binary operation on the set $\{0,1\}$ of
bits is a group operation? What is its neutral element and what are 
the inverses in this group?
\end{enumerate}
\end{exercise}

\begin{exercise} $(\star\star)$
\emph{Basic properties of groups.}
Over the integers $\mathbb Z$, complete the following table, placing a tick or
a cross in each cell to indicate whether or not the operator has the property:

\begin{center}
\begin{tabular}{l|cccc}
& $+$ & $-$ & $\times$ & $\div$ \\
\hline
associative \\
commutative \\
has neutral el. \\
has inverses \\
is group operation
\end{tabular}
\end{center}
\end{exercise}

\begin{exercise}
$(\star\star)$ \emph{Addition tables.}
For a finite group, we can describe the group operation by means of an addition
table. Complete the following tables to make the operations into group
operations. Note: there is exactly one way to do it in each case. You may use
associativity to find this way but you do not have to check it everywhere!

\begin{center}
\begin{tabular}{l|llll}
$+$ & $\spadesuit$ & $\clubsuit$ & $\diamondsuit$ & $\heartsuit$ \\
\hline
$\spadesuit$ & \\
$\clubsuit$ & $\clubsuit$ \\
$\diamondsuit$ &  &  & $\spadesuit$ \\
$\heartsuit$ & & & & $\spadesuit$
\end{tabular}
\qquad
\begin{tabular}{l|lllll}
$+$ & A & B & C & D & E \\
\hline
A & & & B \\
B & \\
C &  & C & E & & D \\
D & & & A \\
E &
\end{tabular}
\end{center}

\begin{itemize}
\item $(\star\star)$ Which rules have you used to fill in the tables?
\item $({\star}{\star}{\star})$ How would you derive these rules from the definition
of a group?
\end{itemize}

\end{exercise}

\subsection{Permutation groups}

We will now look at some examples of non-commutative groups.

\begin{definition}[permutation]
A permutation on a finite set $S$ of elements is a bijective map $p$ from $S$ to
itself. If the elements of $S$ are $s_1, \ldots, s_n$ then a permutation $p$
can be written in the form

\[\left(\begin{array}{llll}
s_1 & s_2 & \ldots & s_n \\
p(s_1) & p(s_2) & \ldots & p(s_n)
\end{array}\right)\]
\end{definition}

In the rest of this lecture we will use $\{0, 1, \ldots, n-1\}$ as an example
set of size $n$. Indeed, to define permutations it makes no difference which
set of a given size we pick --- the elements are just ``labels'' for the
permutations to work on.

We can also draw permutations as diagrams.
For example, here are two permutations on the set $S = \{0, 1, 2, 3\}$:
\[p = \left(\begin{array}{llll}
0 & 1 & 2 & 3 \\
1 & 2 & 3 & 0
\end{array}\right) \quad
q = \left(\begin{array}{llll}
0 & 1 & 2 & 3 \\
1 & 0 & 3 & 2
\end{array}\right) \quad
\]

As diagrams, the permutations look like this:

\begin{center}
\begin{tikzpicture}[yscale=0.5]
\node (a0) at (0, 0)  {$0$};
\node (a1) at (0, -1) {$1$};
\node (a2) at (0, -2) {$2$};
\node (a3) at (0, -3) {$3$};
\node (b0) at (2, 0)  {$0$};
\node (b1) at (2, -1) {$1$};
\node (b2) at (2, -2) {$2$};
\node (b3) at (2, -3) {$3$};
\draw [->] (a0) to (b1);
\draw [->] (a1) to (b2);
\draw [->] (a2) to (b3);
\draw [->] (a3) to (b0);
\node at (1, -4) {$p$};
\end{tikzpicture}
\qquad
\begin{tikzpicture}[yscale=0.5]
\node (a0) at (0, 0)  {$0$};
\node (a1) at (0, -1) {$1$};
\node (a2) at (0, -2) {$2$};
\node (a3) at (0, -3) {$3$};
\node (b0) at (2, 0)  {$0$};
\node (b1) at (2, -1) {$1$};
\node (b2) at (2, -2) {$2$};
\node (b3) at (2, -3) {$3$};
\draw [->] (a0) to (b1);
\draw [->] (a1) to (b0);
\draw [->] (a2) to (b3);
\draw [->] (a3) to (b2);
\node at (1, -4) {$q$};
\end{tikzpicture}
\end{center}

What makes permutations into a group is the fact that they can be composed.
This is easiest to see if we take two permutation diagrams and glue the arrows
together in the middle. Unfortunately, there are two different ways we can do
this and while our way is perhaps the more common, both ways can be found in
different textbooks. For us, composing $p$ and $q$ means that we write the
diagram of $q$ on the left and that of $p$ on the right, then glue the arrows
together.

We also need a name for this operation. We choose the more standard $pq$
(rather than $p+q$). The choice of symbol to mean ``compose'', unlike the order
in which we compose things, is just a matter of notation and does not change
anything.

At the moment it might look ``backwards'' to use $pq$ to mean the object you
get when you compose with $q$ on the left. We will explain this in a moment but
first let's look at an example with the permutations $p$ and $q$ from above.
We compose two permutation diagrams by glueing the arrows together in the
middle:

\begin{center}
\begin{tikzpicture}[yscale=0.5]
\node at (-1, -1.5) {$pq =$};
\node (a0) at (0, 0)  {$0$};
\node (a1) at (0, -1) {$1$};
\node (a2) at (0, -2) {$2$};
\node (a3) at (0, -3) {$3$};
\node (b0) at (2, 0)  {};
\node (b1) at (2, -1) {};
\node (b2) at (2, -2) {};
\node (b3) at (2, -3) {};
\draw [->] (a0) to (b1.center);
\draw [->] (a1) to (b0.center);
\draw [->] (a2) to (b3.center);
\draw [->] (a3) to (b2.center);
\node at (1, -4) {$q$};
\node (c0) at (2, 0)  {};
\node (c1) at (2, -1) {};
\node (c2) at (2, -2) {};
\node (c3) at (2, -3) {};
\node (d0) at (4, 0)  {$0$};
\node (d1) at (4, -1) {$1$};
\node (d2) at (4, -2) {$2$};
\node (d3) at (4, -3) {$3$};
\draw [->] (c0) to (d1);
\draw [->] (c1) to (d2);
\draw [->] (c2) to (d3);
\draw [->] (c3) to (d0);
\node at (3, -4) {$p$};
\node at (5, -1.5) {$=$};
\node (e0) at (6, 0)  {$0$};
\node (e1) at (6, -1) {$1$};
\node (e2) at (6, -2) {$2$};
\node (e3) at (6, -3) {$3$};
\node (f0) at (8, 0)  {$0$};
\node (f1) at (8, -1) {$1$};
\node (f2) at (8, -2) {$2$};
\node (f3) at (8, -3) {$3$};
\draw [->] (e0) to (f2);
\draw [->] (e1) to (f1);
\draw [->] (e2) to (f0);
\draw [->] (e3) to (f3);
\node at (7, -4) {$pq$};
\end{tikzpicture}
\end{center}

This gives us the permutation
\[
pq = \left(\begin{array}{llll}
0 & 1 & 2 & 3 \\
2 & 1 & 0 & 3
\end{array}\right)
\]

\textbf{WARNING.} The order in which one writes a composed permutation varies
from textbook to textbook. We call this object $pq$. Some authors would call it
$qp$ instead. As we will see in a moment, this is important: $pq \neq qp$.

The reason that we take $pq$ to mean ``$q$ glued to $p$'' and not the other way
round is that permutations are formally functions on a set of elements: for the
permutations $p$ and $q$ in the example, we could try and evaluate $p(q(0))$.
Since $q(0) = 1$, this becomes $p(1)$ which is $2$.  In other words, to
calculate $p(q(0))$ we apply $q$ first, then $p$. This is actually the formal
definition of our way of composing permutations:

\begin{definition}[composition of permutations]
For two permutations $p$ and $q$ on the same set $X$, their composition $pq$ is
the permutation such that for all $x \in X$ we have $pq(x) = p(q(x))$.
\end{definition}

Whichever way round we define composition, we get a group:

\begin{definition}[symmetric group]
For any positive integer $n$, the symmetric group $S_n$ is the group whose
elements are the permutations of the set $\{0, 1, \ldots, n-1\}$ and the group
operation is composition.
\end{definition}

Let's check that this is actually a group (this is not a full proof). For
associativity, look at the diagrams of any three permutations: it does not
matter which two you glue together first as long as you keep the three diagrams
in the same order. The neutral element is the permutation that maps every
element to itself, sometimes written $id$ (for ``identity''). The inverse of a
permutation can be found by reversing all arrows in the diagram and taking the
mirror image.

Of course, one can define the group of permutations over any set $S$. But the
structure of this group depends only on the number of elements that $S$ has, so
for finite sets $S$ one can consider only the sets $\{0, 1, \ldots, n-1\}$
``without loss of generality''. 

% What we are actually doing is taking this set
% as representative element of the ``class of all sets with $n$ elements''.

Permutation groups are an example of non-commutative groups. (Easy exercise:
find $qp$ for the $p, q$ given above and check that $pq \neq qp$.)

\subsection{Cycles}

Another way to look at permutations is to consider cycles.

\begin{definition}[cycle]
On a set $A$, a cycle of length $k$ is a list of elements of $A$ with no
repetitions $(a_1, a_2, \ldots, a_k)$. The empty cycle (of length $0$) is
written $()$.
\end{definition}

A cycle defines a permutation by sending $a_1$ to $a_2$ etc., and $a_k$ back to
$a_1$.  For example, on the set $A = \{0, 1, 2, 3, 4, 5, 6, 7\}$ the cycle $(5,
1, 2)$ defines the permutation
\[\left(\begin{array}{llllllll}
0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 \\
0 & 2 & 5 & 3 & 4 & 1 & 6 & 7
\end{array}\right)\]

The empty cycle $()$ gives the identity permutation $id$.
The notation for cycles does not uniquely describe a permuation: $(5, 1, 2)$
and $(1, 2, 5)$ both give the same permutation. (Exercise: there is one other
way to write the same permutation as a cycle, which one?) However $(5 ,2, 1)$
describes a different permutation.

The interesting thing about cycles is that they can be used to build all
permutations.

\begin{proposition}\label{prop:cycles}
Any permutation $p \in S_n$ can be written as a composition of cycles with
disjoint elements. If we take the underlying set to be $\{0, \ldots, n-1\}$
then there is an ordering under which each permutation has exactly one normal
form as a composition of disjoint cycles:

\begin{itemize}
\item Shorter cycles come before longer ones.
\item For two cycles of the same length, the one with the smallest element
comes first.
\item Within each cycle, the smallest element is the first.
\end{itemize}
\end{proposition}

There are several useful rules for computing with cycles which we will use to
prove this proposition. The composition of two cycles is the composition of
their permutations (in the same order as the cycles are given), so the
expression $(0, 1)(2, 3)$ on the set $\{0, 1, 2, 3\}$ is the composition
\[
(0, 1)(2, 3) =
\left(\begin{array}{llll}
0 & 1 & 2 & 3 \\
1 & 0 & 2 & 3
\end{array}\right)
\left(\begin{array}{llll}
0 & 1 & 2 & 3 \\
0 & 1 & 3 & 2
\end{array}\right)
=
\left(\begin{array}{llll}
0 & 1 & 2 & 3 \\
1 & 0 & 3 & 2
\end{array}\right)
\]

\begin{enumerate}
\item If you rotate the elements in a cycle around by taking the last element
and placing it as the new first element, you get the same permutation.
Conversely, two cycles describe the same permutation if and only if one can be
rotated to give the other.

For example, $(1, 2, 3)$ and $(3, 1, 2)$ both describe the same permutation.

\item If $c$ and $d$ are two disjoint cycles (they do not share any elements)
then $cd = dc$.

In the example above, one can check that $(1, 0)(2, 3) = (2, 3)(1, 0)$.

\item If two cycles $c, d$ share exactly one element $x$, their composition is
the cycle obtained by rotating $x$ to be in the last place of $c$ and the first
place of $d$, and ``glueing together'' the $x$-es, dropping the middle
parentheses.

For example, to compute $(1, 2, 3)(2, 4)$ we write this as $(3, 1, 2)(2, 4)$
and glue the $2$-s to get $(3, 1, 2, 4)$.
\end{enumerate}

Rules $1$ and $2$ together say that if we have any way of representing a
permutation as a composition of disjoint cycles then we can reorder the cycles
and the elements within cycles to get the unique representation from
proposition \ref{prop:cycles}.

As an example, consider the set $A = \mathbb Z_8$ and the permutation
\[p = \left(\begin{array}{llllllll}
0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 \\
3 & 2 & 4 & 0 & 1 & 6 & 5 & 7
\end{array}\right)\]
Starting with $0$, we find $p(0) = 3$ and $p(3) = 0$ so we have our first
cycle $(0, 3)$. As we move through the elements we get the other cycles
$(1, 2, 4)$ and $(5, 6)$, which we can order to write
\[
p = (0, 3)(5, 6)(1, 2, 4)
\]

\begin{exercise}
\emph{Permutations --- the case $n=3$.}
\begin{enumerate}
\item $(\star)$ Write out all permutations on the set of three elements
$\{0, 1, 2\}$.
\item $(\star)$ Find two elements $p, q$ of $S_3$ such that $pq \neq qp$.
\item $(\star)$ Decompose all the elements of $S_3$ into cycles.
\item $(\star\star)$ Write out the ``addition table'' (composition table) for
$S_3$, representing the elements as cycles.
\item $(\star\star)$ You have just shown that $S_3$ is not commutative.
Conclude that for any $n \geq 3$, $S_n$ is not commutative either. What about
$S_1$ and $S_2$?
\end{enumerate}
\end{exercise}

\begin{exercise}
$(\star)$ \emph{Permutations in general.}
Consider
\[ p = \left( \begin{array}{lllll}
1 & 2 & 3 & 4 & 5 \\
3 & 1 & 2 & 5 & 4
\end{array}\right)
\quad
q = \left( \begin{array}{lllll}
1 & 2 & 3 & 4 & 5 \\
2 & 1 & 4 & 3 & 5
\end{array}\right)
\]

\begin{itemize}
\item Compute $pq$ and $qp$.
\item Decompose $p$ and $q$ into cycles.
\item Write the following as permutations: $(154)$, $(12)(54)$ and
$(1254)(4531)$.
\end{itemize}

\end{exercise}
