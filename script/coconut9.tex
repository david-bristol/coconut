\ifdefined\booklet
\else
\documentclass{scrartcl}

\include{header}

\setuplecture{9}{Homomorphisms}

\begin{document}

\titlestuff

\emph{In this lecture: homomorphisms --- isomorphisms --- automorphisms and
their group --- application to groups, rings and fields }

\emph{Learning outcomes.}
After this lecture and revision, you should be able to:
\begin{itemize}
\item Define homomorphisms and isomorphisms and give examples.
\item Check, for simple examples, whether two structures are isomorphic and
find an isomorphism if they are.
\item Determine the isomorphism class of finite Abelian groups.
\item Find integers with given remainders modulo different coprime moduli.
\item Compute the Frobenius map in a ring and the inverse of an element in a
finite field.
\end{itemize}
\fi

\section{Homomorphisms}

Imagine you're trying to solve a sudoku puzzle. Halfway through, you take out
your laptop and open a sudoku solver program to check whether one of your
guesses is correct. Except --- this sudoku is in a paper that tries to be
special and uses the letters A--I instead of the numbers 1--9, but your sudoku
solver only accepts numbers as input. Does this make any difference? Of course
not: you can map the letters to numbers, for example A=1, B=2 etc., then solve
the numeric sudoku and map back again: if the program says that a certain field
is a $3$, you can write a C in the original sudoku there.

The moral of this story is that you have a mapping that respects the structure
of a sudoku: you not only map the elements (letters) to other elements
(numbers) and back again but any statement that you can make about the original
``in the language of sudokus'' maps to a statement about the numeric version
and back again too. For example, ``the remaining field in row one of the top
left square must be an A or a B'' is a statement that translates to the numeric
version by replacing A and B with 1 and 2. Such a mapping lets us say that the
two versions of the sudoku are ``essentially the same, just written
differently''; mathematicians would say the two are isomorphic.

Apart from isomorphisms, there are more general structure-respecting mappings
that are not reversible: these are called homomorphisms.

\subsection{Group homomorphisms}

A homomorphism from a group $(G, +_G)$ to a group $(H, +_H)$ is a map from $G$
to $H$ that preserves the structure or language or groups. The language of
groups revolves around the noun ``neutral'' and the verbs ``add'' and
``invert''.  Note that to be pedantic, we are using different symbols for
addition in the two groups.

\begin{definition}[group homomorphism]
A function $f: G \to H$ is a group homomorphism between the groups $(G, +_G)$
and $(H, +_H)$ if it satisfies these three conditions.
\begin{itemize}
\item It preserves neutral elements: if $0_G$ is the neutral element of $(G,
+_G)$ and $0_H$ is the neutral element of $(H, +_H)$ then $f(0_G) = 0_H$.
\item It preserves addition. For any two elements $a, b$ of $G$ we have $f(a
+_G b) = f(a) +_H f(b)$.
\item It preserves inverses: for any element $a$ of $G$ with inverse $-a$,
$f(-a)$ is the inverse of $f(a)$.
\end{itemize}
\end{definition}

We have discussed one particular group homomorphism a lot: for any positive
integer $n$, the map $[\ ]$ from $(\mathbb Z, +)$ to $(\mathbb Z_n, +_n)$ is a
homomorphism. Indeed, $[0] = 0, [a+b] = [a] +_n [b]$ and $[a] +_n [-a] = 0$ so
$[-a]$ is the inverse of $[a]$.
This is one of the times when it really pays off to be pedantic with the
addition symbol. The statement $[a+b] = [a] +_n [b]$ is the same as $[a+b] =
[[a]+[b]]$ which we stated as a rule in lecture 1; it would be wrong to write
$[a+b] = [a] + [b]$ for the usual addition in $\mathbb Z$ however.
(Exercise: find a counter-example.)

\begin{exercise}
$(\star)$ \emph{Trivial homomorphisms.}
\begin{itemize}
\item For any $n > 0$, describe the group homomorphisms between $(\mathbb Z_n,
+)$ and $(\{0\}, +_0)$ in both directions, where $0 +_0 0 = 0$.
\item Check that for any groups $(G, +_G)$ and $(H, +_H)$ the map $f: G \to H$
that sends every element of $G$ to the neutral element of $(H, +_H)$ is a group
homomorphism.
\item Which other trivial homomorphism is there from any group $(G, +)$ to
itself?
\end{itemize}
\end{exercise}

\begin{exercise}
$(\star\star)$ \emph{Group homomorphisms and orders.}
\begin{itemize}
\item Explain why there are no non-trivial homomorphisms (that do not send
everything to the neutral element) from $(\mathbb Z_2, +_2)$ to $(\mathbb Z_3,
+_3)$ or back again.
\item What about $(\mathbb Z_2, +_2)$ to $(\mathbb Z_4, +_4)$ (and back again)?
\item Consider the following situation. $(G, +_G)$ and $(H, +_H)$ are groups
and $g \in G$ is an element of finite order $n > 0$, in particular
$\underbrace{g +_G \ldots +_G g}_{n \text{ times}} = 0_G$. Show that if $f: G
\to H$ is a homomorphism between these groups and $h = f(g)$ then
$\underbrace{h +_H \ldots +_H h}_{n \text{ times}} = 0_H$.
\item Show that group homomorphisms do not have to preserve the order of
elements: find an example of two groups as above, an element $g \in G$ and a
group homomorphism $f$ such that the order of $f(a)$ is not the same as the
order of $a$.
\end{itemize}
$({\star}{\star}{\star})$
The precise rule is that if $(G, +_G)$ and $(H, +_H)$ are groups and $f:
G \to H$ is a group homomorphism between them then the order of $f(g)$ divides
the order of $g$ for any $g \in G$. Prove this.
\end{exercise}

\subsection{Isomorphisms}

A homomorphism allows you to translate statements one way but not back. For
example, the statement ``adding any element to itself gives $0$'' holds in the
group $(\mathbb Z_2, +_2)$ but not in $(\mathbb Z, +)$ so there is no way to
translate all possible statements in group-language back again.
If a homomorphism does have an inverse, it is called an isomorphism.

\begin{definition}[isomorphism]
A homomorphism $f$ from $\Grp = (G, +_G)$ to $\mathbb H = (H, +_H)$ is called
an isomorphism if there is a homomorphism $s$ from $\mathbb H$ to $\Grp$ such
that $f$ and $s$ are inverses, i.e. for any $g \in G$ we have $s(f(g)) = g$ and
for any $h \in H$ we have $f(s(h)) = h$.

Two groups are called isomorphic if there is an isomorphism between them (being
isomorphic is an equivalence relation).
\end{definition}

For example, consider the following three groups.

\vspace{12pt}

\hfil
\begin{tabular}{c}
$\Grp = (\{0, 1, 2, 3\}, +)$ \\
\\
\begin{tabular}{l|llll}
$+$ & 0 & 1 & 2 & 3 \\
\hline
0 & 0 & 1 & 2 & 3 \\
1 & 1 & 2 & 3 & 0 \\
2 & 2 & 3 & 0 & 1 \\
3 & 3 & 0 & 1 & 2
\end{tabular}
\end{tabular}
\hfill
\begin{tabular}{c}
$\mathbb H = (\{A, B, C, D\}, \oplus)$ \\
\\
\begin{tabular}{l|llll}
$\oplus$ & A & B & C & D \\
\hline
A & A & B & C & D \\
B & B & C & D & A \\
C & C & D & A & B \\
D & D & A & B & C
\end{tabular}
\end{tabular}
\hfill
\begin{tabular}{c}
$\mathbb I = (\{0, 1, 2, 3\}, \boxplus)$ \\
\\
\begin{tabular}{l|llll}
$\boxplus$ & 0 & 1 & 2 & 3 \\
\hline
0 & 0 & 1 & 2 & 3 \\
1 & 1 & 0 & 3 & 2 \\
2 & 2 & 3 & 0 & 1 \\
3 & 3 & 2 & 1 & 0
\end{tabular}
\end{tabular}

\vspace{12pt}

The groups $\Grp$ and $\mathbb H$ are isomorphic: the isomorphism from $\Grp$
to $\mathbb H$ is the function $f$ with $f(0) = \text{A}, f(1) = \text{B}, f(2)
= \text{C}, f(3)=\text{D}$.
However, $\Grp$ and $\mathbb I$ are not isomorphic. For example, adding any
element to itself in $\mathbb I$ gives the neutral element but this is not true
in $\Grp$, so there cannot be any function $f$ from $\mathbb I$ to $\Grp$ that
preserves neutral elements and addition.

The notion of isomorphism is very general and powerful in mathematics. The way
we gave our definition, there is nothing specific to groups so once we know
what a ring or field homomorphism is, we have got a definition of ring and
field isomorphisms for free. The same idea could be carried over to other kinds
of structure but we would have to give a more abstract definition of what it
means for two homomorphisms to be inverses (since they are no longer
necessarily functions on sets) which we can happily leave to professional
mathematicians.

\subsection{Group isomorphisms}

There are only two ``really different'' groups with four elements, both of
which we have just seen: one which is ``essentially'' $(\mathbb Z_4, +_4)$ and
one in which every element added to itself is zero.
The notion of isomorphism allows us to make clear what we mean by this
statement: there are only two groups of order $4$ ``up to isomorphism''.
To define this formally, let $\sim$ be the equivalence relation ``is isomorphic
to'' for groups. (Exercise: check that this really is an equivalence relation.)
Then there are exactly two different equivalence classes that contain groups of
order $4$. Two groups of different orders cannot be isomorphic so these two
classes contain only groups of order $4$.

For prime numbers the classification is even simpler:

\begin{proposition}
For any prime number $p$, there is only one group of order $p$ up to
isomorphism.
\end{proposition}

Another way of putting this is that if we have any two groups of order $p$
where $p$ is a prime then they are automatically isomorphic. Sensibly, one
chooses $(\mathbb Z_p, +_p)$ as the representative element of this class of
group.

We can extend this classification to all finite groups but we need to introduce
one more concept to do this.

\begin{exercise}
$(\star\star)$ \emph{Isomorphisms preserve orders.}
Show that if $(G, +_G)$ and $(H, +_H)$ are groups and $f: G \to H$ is an
isomorphism then for every element $g \in G$, the order of $g$ is the same as
the order of $f(g)$.
\end{exercise}

\begin{exercise}
$(\star\star)$ \emph{Isomorphisms between additive and multiplicative groups.}
\begin{itemize}
\item Start with the group $(\mathbb Z^\times_7, \cdot)$ and consider the
subgroup $\langle 2 \rangle$. This subgroup has $3$ elements; find an
isomorphism between this group and $(\mathbb Z_3, +)$.
\item Find the other isomorphism between the above two groups (there are
exactly two).
\item There is one isomorphism $f$ from $(\mathbb Z_{10}, +)$ to the subgroup
$\langle 2\rangle$ of $(\mathbb Z^\times_{11}, \cdot)$ that has $f(1) = 2$. First,
find it.  Secondly, what is the obvious formula for this isomorphism?
\end{itemize}
\end{exercise}

\subsection{Cartesian products}

If $\Grp = (G, +_G)$ and $\mathbb H = (H, +_H)$ are two groups we can form a
further group by taking all the pairs of elements $(g, h)$ with $g \in G$ and
$h \in H$ and adding component-wise, i.e. the sum of $(g, h)$ and $(g', h')$
is $(g +_G g', h +_H h')$. Since the set of elements of this group is $G \times
H$, we call this group $\Grp \times \mathbb H$, the product of groups $\Grp$
and $\mathbb H$.

\begin{definition}[product of groups]
For a finite list of groups $\Grp_1, \ldots, \Grp_n$, the product group
$\Grp_1 \times \ldots \times \Grp_n$ is the group whose elements are tuples of
$n$ elements where the $i$-th element is in $\Grp_i$, with component-wise
addition. For the $n$-fold product of a group with itself we also write
$\Grp^n$.
\end{definition}

By now, we should all be able to find the formulas for the neutral element and
the inverse of elements in product groups.
Products of rings, fields and vector spaces work in much the same manner.

\begin{diamondsec}
We can even define a Cartesian product of functions. The idea is that if
$f: A \to B$ and $G: C \to D$ then $f \times g: A \times C \to B \times D$
namely $(f \times g)(a, c) := (f(a), g(c))$. When we have a single function
$f: A \to B$ then $f \times f: A \times A \to B \times B$ captures the idea of
applying a function component-wise.
(For set theory experts: the Cartesian product of functions is not identical,
but in bijection with the Cartesian product of the functions when viewed as sets.)
\end{diamondsec}

\subsection{Classification of finite Abelian groups}

Product groups let us describe all finite Abelian groups:

\begin{theorem}[classification of finite Abelian groups]
Every finite Abelian group $\Grp$ is isomorphic to exactly one group of the
form $\mathbb Z_{p_1^{m_1}} \times \ldots \times \mathbb Z_{p_k^{m_k}}$ where
the $p_i$ are primes and the $m_i$ positive integers.
\end{theorem}

\needspace{3\baselineskip}
For a group of order $n$, we have $n = p_1^{m_1} \cdot \ldots \cdot p_k^{m_k}$
in this decomposition. Groups of the same order can still differ in whether
prime powers are inside or outside the subscripts: the two non-isomorphic
groups of order $4$ are $\mathbb Z_{2^2} = \mathbb Z_4$ and $\mathbb Z_2 \times
\mathbb Z_2 = \left(\mathbb Z_2\right)^2$.

By this theorem, a cyclic group of composite order --- say, $(\mathbb Z_{15},
+)$ --- should be isomorphic to a product group $\mathbb Z_3 \times \mathbb
Z_5$.  Indeed, if we take any element $x$ of $\mathbb Z_{15}$, we can write it
as the pair $(x \pmod{3}, x \pmod{5})$ to get an element of $\mathbb Z_3 \times
\mathbb Z_5$ and for any such pair, there is exactly one element in $\mathbb
Z_{15}$ with the given decomposition, as in the table below.

\begin{center}
\begin{tabular}{llllllll}
0 & (0, 0) & \qquad\qquad & 5 & (2, 0) & \qquad\qquad & 10 & (1, 0) \\
1 & (1, 1) & & 6 & (0, 1) & & 11 & (2, 1) \\
2 & (2, 2) & & 7 & (1, 2) & & 12 & (0, 2) \\
3 & (0, 3) & & 8 & (2, 3) & & 13 & (1, 3) \\
4 & (1, 4) & & 9 & (0, 4) & & 14 & (2, 4) \\
\end{tabular}
\end{center}

\begin{exercise}
$(\star\star)$ \emph{Another decomposition.}
Decompose $(\mathbb Z_{12}, +)$ into $\mathbb Z_4 \times \mathbb Z_3$.
\end{exercise}


\subsection{The group of automorphisms}

Isomorphism is an equivalence relation. In particular, if $f$ is an isomorphism
from $\Grp$ to $\mathbb H$ and $g$ is an isomorphism from $\mathbb H$ to
$\mathbb K$ then the composition $gf$ (as a function on the underlying sets,
i.e. for each element $x$ of $\Grp$ we have $gf(x) := g(f(x))$, an element of
$\mathbb K$). Since we can compose and invert isomorphisms, can we make them
into a group? Not necessarily --- we cannot compose any two isomorphisms, only
ones with compatible domains. If $f: \Grp \to \mathbb H$ and $k: \mathbb K \to
\mathbb L$ then we cannot compose $f$ and $k$.  We can always compose
isomorphisms if they start and end at the same object though:

\begin{definition}[automorphism]
An isomorphism from a group (or ring, field) $\Grp$ to itself is called an
automorphism of $\Grp$. The automorphisms of any object $\Grp$ form a group
called $\text{Aut}(\Grp)$ with composition as the operation.
\end{definition}

$\text{Aut}(\Grp)$ is a group because function composition is associative, the
identity map that sends every element of $\Grp$ to itself is an automorphism
and forms the neutral element of $\text{Aut}(\Grp)$ and isomorphisms are
invertible by definition.

The definition of an automorphism group applies equally to rings, fields and
many other structures; the automorphisms themselves always form a group,
whichever structure one looks at.

\begin{exercise}
$(\star\star)$ \emph{Group automorphisms.}
\begin{itemize}
\item There is exactly one nontrivial automorphism from $(\mathbb Z_3, +)$ to
itself (that is neither the identity map nor sends everything to the neutral
element). Find it.
\item Find the automorphism groups of $(\mathbb Z_5, +)$ and $(\mathbb Z_6, +)$
as well. Hint: automorphisms preserve orders of elements, so they must preserve
generators of finite groups as well.
\item Find the automorphism group of $\mathbb Z_2 \times \mathbb Z_2$.
\end{itemize}
\end{exercise}

\subsection{Ring homomorphisms}

To adapt the notion of homomorphism to rings, we just have to take care of
multiplication as well.

\begin{definition}[ring homomorphism]
Let $\mathcal R = (R, +, \cdot)$ and $\mathcal S = (S, \oplus, \odot)$ be two
rings. A function $f: R \to S$ is a ring homomorphism if $f$ is a group
homomorphism from $(R, +)$ to $(S, \oplus)$ and these two conditions hold:
\begin{itemize}
\item For any $a, b \in R$ we have $f(a \cdot b) = f(a) \odot f(b)$.
\item We have $f(1_R) = 1_S$ where $1_R$ is the one (neutral element of
multiplication) of $\mathcal R$ and $1_S$ is the one of $\mathcal S$.
\end{itemize}
\end{definition}

If we look at a ring homomorphism from a ring $\mathcal R$ to itself, there is
very little freedom.  We know that for such a $f$, for all $r, s$ we have
$f(r+s) = f(r) + f(s)$ and $f(rs) = f(r)f(s)$ along with $f(1) = 1$.
So for any $k \in \mathbb Z$ and for the ring element
\[ r := \underbrace{1 + 1 + \ldots + 1}_{k \text{ times}} \]
we have $f(r) = f(1) + \ldots + f(1) = 1 + \ldots + 1 = r$, so ring
homomorphisms from a ring to itself cannot change multiples of $1$. For
example, in $\mathbb Z$, the identity function is the only ring homomorphism.

\textbf{WARNING}: A ring homomorphism is not the same as a linear function 
(which we will introduce later). A linear function must satisfy $f(ax) = af(x)$ 
rather than $f(ax) = f(a)f(x)$, which gives it a lot more freedom.

%In polynomial rings over finite fields, the interesting part of a ring
%homomorphism $f$ is therefore what it does to $X$ (which is not a multiple of
%$1$). Since $f(X^2) = f(X) \cdot f(X)$ and so on, the behaviour of a ring
%homomorphism on such a polynomial ring is determined by its behaviour on $X$,
%since we will see in a moment that all non-zero field elements are multiples of
%$1$.

We can construct a function $m: \mathbb N \to \mathcal R$ for any ring 
$\mathcal R$ that takes $n$ to
\[ m(n) := \underbrace{1_R + \ldots + 1_R}_{n \text{ times}} \]
where $1_R$ is the one of the ring. For $0$, we set $m(0) := 0_r$, that is
evaluating $m$ at the number zero gives the zero of the ring.
We can extend this function to $\mathbb Z$ by setting $m(a) := -m(-a)$ for
negative $a$, i.e. to evaulate on a negative integer you invert the integer to
get a positive one, compute $m$ on that and then invert back again in the ring.
This function $m$ is now a ring homomorphism $\mathbb Z \to \mathcal R$. If
$\mathcal R = \mathbb Z$ then this $m$-function is the identity. In fact,

\begin{proposition}
For any ring $\mathcal R$, there is exactly one ring homomorphism from
$\mathbb Z$ to $\mathcal R$ and it is the map
\[
m(z) := \left\{ \begin{array}{rl}
\underbrace{1_R + \ldots + 1_R}_{z \text{ times}} & z > 0 \\
0_R & z = 0 \\
-m(-z) & z < 0
\end{array} \right.
\]
where $1_R$ and $0_R$ are the one and zero elements of the ring.
\end{proposition}

With this homomorphism in place, we can define the characteristic of a ring. It
is a similarly important number for rings as the order is for groups, although
it does not always count elements.

\begin{definition}[characteristic]
The characteristic $\mathsf{char}(\mathcal R)$ of a ring $\mathcal R$ is the
smallest positive integer $z$ for which $m(z) = 0$, the zero of the ring. If no
such integer exists, the characteristic is $0$ (the zero of $\mathbb Z$).
\end{definition}

For example, $\mathsf{char}(\mathbb Z) = 0$ and $\mathsf{char}(\mathbb Z_n) =
n$. But, $\mathsf{char}(\mathbb Z_n [X]) = n$ too, so the characteristic does
not ``count'' elements that are not multiples of $1$.

%If $p$ is a prime number and $\mathcal R$ is a commutative ring of
%characteristic $p$, the
%map $\phi: \mathcal R \to \mathcal R, x \mapsto x^p$ has the property that
%$\phi(a)\phi(b) = \phi(ab)$ --- this is just the usual formula $a^p b^p = (ab)
%^p$ that holds in any commutative ring. But if we write out the expansion of
%$\phi(a+b) = a^p + b^p$, all intermediate terms gain a factor $p$ and vanish:
%we get $(a + b)^p = a^p + b^p$.

%\begin{proposition}[Frobenius map]
%In a commutative ring $\mathcal R$ with prime characteristic $p$, the map $\phi:
%x \mapsto x^p$ is a ring homomorphism, called the Frobenius map.
%\end{proposition}

\subsection{$\diamond$ More on finite Abelian groups}
\begin{diamondsec}
The formula $\mathbb Z_{15} \cong \mathbb Z_3 \times \mathbb Z_5$ is just a
special case of a theorem that is supposed to have originated in ancient China.

\begin{theorem}[Chinese remainder theorem]
Let $n_1, \ldots, n_k$ be positive integers such that no two of these have a
factor in common. Then for any integers $a_1, \ldots, a_k$ with $0 \leq a_i
\leq n_i$ for all $i$ there is exactly one integer $x$ satisfying all the
equations $x = a_i \pmod{n_i}$.
\end{theorem}

$\diamond$
The classification theorem for finite Abelian groups can be generalised to
finitely generated Abelian groups, where there is a finite set of elements
$x_1, \ldots, x_n$ such that $g = \langle x_1, \ldots, x_n \rangle$.
In this case, every such group is isomorphic to the direct product of a finite
Abelian group as above and a group $\mathbb Z^r$ for a unique value of $r$,
which is called the rank of the group.

$\diamond$
One can also give a classification theorem for all finite groups, dropping the
Abelian requirement. A theorem by Jordan and H\"older states that all finite
groups are composed of finite simple groups (the ``primes'' of the group world,
where every subgroup of a certain form has to be trivial). It remains to
classify all finite simple groups --- the result is one of the masterpieces of
Algebra, completed for the first time (assuming no mistakes) in 2008. A revised
version of the proof is being edited for publication and is expected to run to
several thousand pages.
\end{diamondsec}

\ifdefined\booklet
\else
\end{document}
\fi
