\section{Sets}

\emph{The material in this section is not assessed in CNuT directly,
but may be useful to understand some of the notation better that is
used throughout the unit.}

\subsection{Opening example}

Suppose that you want to write an equation-solving program in a language with
types. Let's restrict ourselves to equations in a single variable $x$ for now.
You define types (or classes) \textbf{Equation} and \textbf{Number} and
create a function with a signature as follows:

\begin{itemize}
\item In Haskell notation: \texttt{solve: Equation -> Number}.
\item In C/Java notation: \texttt{Number solve(Equation eq)}.
\end{itemize}

You implement this function to the point that when you input \texttt{x + 2 = 5}
it returns 3. What could possibly go wrong?

Consider the following equations:
\begin{itemize}
\item \texttt{(x-2)(x-3) = 0} has ``two solutions'', 2 and 3.
\item \texttt{2x + 1 = 2(x + 3)} has ``no solutions''.
\item \texttt{2(x + 1) = 2x + 2} has ``infinitely many solutions'', although
that is not very precise --- \texttt{x > 2} is an inequality with infinitely
many solutions too, but not all numbers are solutions to that inequality.
\end{itemize}

The programmer's first idea might be to have the equation-solving function
return a list of numbers as solutions. If there are no numbers that solve the
equation, then you can just return an empty list. But that is not what the
mathematician wants.

The lists \texttt{[1, 2]} and \texttt{[2, 1]} are not the same thing, and neither
are the same as the list \texttt{[1, 2, 2]}. But either 2 is a solution to an
equation, or it is not\footnotemark.
\footnotetext{For polynomial equations, it makes sense to talk about ``double
roots'' e.g. in the equation $(x-2)^2(x-3) = 0$, 2 is a double root and 3 is
a single one. But that is a special case.}

\subsection{Sets and elements}

What we really want instead of a list of numbers is a set of numbers. Written
\texttt{Set Number} in Haskell notation and \texttt{Set<Number>} in Java notation,
a set is a data type with one main function \texttt{contains} that takes a set,
an element (e.g. a number) in this case and returns true or false. A set data
type will typically also contain other functions, e.g. enumerating all its
elements, although the basic set type could very well be implemented for
infinite sets too:

\begin{verbatim}
// Java's java.util.Set interface actually contains a
// lot more methods than this.
interface Set<T> {
    bool contains(T elem);
}
class EvenNumbers implements Set<Integer> {
    bool contains(Integer elem) { return elem % 2 == 0; }
}
\end{verbatim} 

For a computer scientist, implementing a general-purpose set class well means
that it should take at most O(log $n$) time to run the contains operation on a
set with $n$ elements. Mathematical set theory does not care about implementation
details like this however.

Mathematically, a set $S$ is something from which you can form statements using
the operation $\in$. The statement $2 \in S$ can be read as any of the following
sentences, all of which mean the same:

\begin{itemize}
\item $2$ is in $S$. Alternatively: $S$ contains $2$.
\item $2$ is an element of $S$.
\item $2$ is a member of $S$.
\end{itemize}

Of course there is also $\notin$, pronounced ``is not a member of''. Some people
write the whole thing ``backwards'': $S \ni 2$ means ``$S$ contains $2$''.

\textbf{Warning:}
Our introductory example used \emph{typed} sets. Mathematically, sets do not
have types --- you can make a set containing an integer, another set and a
triangle if you want. You can have sets containing sets, and sets containing
sets containing sets and so on, but you cannot have a ``set of all sets''
otherwise your mathematics explodes. The full rules for exactly what you can and
can not do to sets are complicated enough that one could create a whole graduate
level masters programme in mathematics to teach set theory properly.

The usual way out of this problem is to fix a ``universe'' set such as the
integers or real numbers, and only allow elements of this set in other sets.
If you need sets of sets (which you often do), you only allow ``trees'' of sets
where all leaves are sets containing only elements of the universe set.

\subsection{Set equality}

Pick some set $\mathcal U$ called \textbf{universe}, e.g. the real numbers.
Everything in this set is called an \textbf{element}.

For a set $S$ and an element $e$, wither $e \in S$ or $e \notin S$. Two sets
$S, T$ are \textbf{equal} which we write $S = T$ if they contain the same elements,
that is for all $e \in \mathcal U$ we have $e \in S \iff e \in T$.

This brings us to the problem of how to write down a set. The first rule is that
we use curly braces $\{\ \}$. For example, $2 \in \{2, 3\}$ but $4 \notin \{2, 3\}$.

The second rule is that the empty set, which contains no elements, can be
written either $\{\}$ or $\emptyset$. In other words, $e \notin \{\}$ for all
elements $e$.

The third rule is that two sets can be the same even if they are written
differently. Just like $2/4$ is the same number as $1/2$ and $0.5$, the sets
$\{1, 2, 3\}$ and $\{1, 3, 2\}$ and $\{3, 2, 1\}$ so on are all different ways
of writing the same set, e.g. $\{1, 2, 3\} = \{3, 2, 1\}$. For sets of numbers,
since there is a natural ordering on the numbers, there is a natural way to
order the elements of such a set too, but for a set of e.g. colours, there might
not be an obvious way to order the elements.

The fourth rule is that a set cannot contain an element more than once, so
$\{2, 3\}$ and $\{2, 2, 3\}$ are two ways of writing the same set. The second
one is legal and sometimes occurs as an intermediate step in a calculation with
sets, just like you might temporarily get ``3 hours 65 minutes'' in a calculation
with time, before you adjust to the more usual form. Similarly, $3 + 1$ is a
denormalised way of writing the number $4$.
Whether or not your elements are numbers, you can always normalise a set by
writing repeated elements only once.

Some common sets to choose as universes are:

\begin{itemize}
\item $\mathbb N = \{0, 1, 2, \ldots\}$ the set of \textbf{natural numbers}.
Whether $0$ is a natural number or not varies between textbooks; in our case,
$0$ is a natural number.

\item $\mathbb Z = \{ \ldots, -2, -1, 0, 1, 2, \ldots \}$, the set of
\textbf{integers}.

\item $\mathbb Q$, the set of \textbf{rational numbers}, that is integers and
fractions.

\item $\mathbb R$, the set of \textbf{real numbers} a.k.a. ``the whole number line''
(but not including ``infinity'').

\item $\mathbb C$, the set of \textbf{complex numbers}.

\item $\mathbb B = \{0, 1\}$, the set of \textbf{Boolean values} or \textbf{Bits}.
\end{itemize}

We will see more examples of different ``kinds of number'' in the later lectures.

\subsection{Operations on sets}

If you have some sets $A, B$, you can form more sets with the following operations:

\begin{itemize}
\item $A \cup B$ is the \textbf{union} of $A$ and $B$ and contains all elements
that are in $A$, $B$ or both. The union symbol is a rounded version of the logical
or symbol $\vee$, since $e \in (A \cup B) \iff (e \in A) \vee (e \in B)$.

\item $A \cap B$ is the \textbf{intersection} of $A$ and $B$ and contains all elements
that are in both $A$ and $B$. As a formula, 
$e \in (A \cap B) \iff (e \in A) \wedge (e \in B)$.

\item $A \setminus B$ (sometimes also written $A - B$, but not in these notes)
is the \textbf{difference} of $A$ and $B$ and contains all elements that are in
$A$ but not in $B$. As a formula, 
$e \in (A \setminus B) \iff (e \in A) \wedge (e \notin B)$.
\end{itemize}

As a general rule for unions, if you have both sets explicitly then you can just
write both sets together as one and normalise:
$\{1, 2\} \cup \{3, 2\} = \{1, 2, 3, 2\} = \{1, 2, 3\}$.
This also explains how denormalised ways of writing a set sometimes appear in
the middle of a calculation.

Within a universe $\mathcal U$, we can define the complement $\overline{A}$ of
a set $A$ as $\mathcal U \setminus A$, e.g. all elements not in $A$. This is
sometimes also written $A^c$. Obviously, $\overline{\emptyset} = \mathcal U$
and $\overline{\mathcal U} = \emptyset$.

Set operations, like logical operations, follow \textbf{de Morgan's laws} such as
$\overline{A \cup B} = \overline{A} \cap \overline{B}$ and
$\overline{A \cap B} = \overline{A} \cup \overline{B}$.
While the proof of these laws for logical operations is through truth tables,
proofs about sets are almost always done by expressing them in terms of elements,
at which point you can use logic again.

\begin{diamondsec}
Let's prove 
$\overline{A \cup B} = \overline{A} \cap \overline{B}$ as an example.
The way to prove two sets $X$ and $Y$ are equal is to show they contain the same
elements, which takes two steps: (1) pick any element $x \in X$ and show that
$x \in Y$ too and (2) pick any $y \in Y$ and show that
$y \in X$ too.

Supose that $x \in \overline{A \cup B}$. (We are allowed to suppose this even
if the original set is empty, as an implication with a false premise is 
always true.)
Expand definitions to get $\neg(x \in A \vee x \in B)$ and use logic's deMorgan
to get $x \notin A \wedge x \notin B$. Using the definition of $\cap$ and of
complements, this is $x \in \overline{A} \cap \overline{B}$ which is what we set
out to prove. In fact, this proof is reversible, so starting with
$x \in \overline{A} \cap \overline{B}$ we get $x \in \overline{A \cup B}$ too.
This shows that $\overline{A \cup B} = \overline{A} \cap \overline{B}$.
\end{diamondsec}

\subsection{Subsets}

If $A$ and $B$ are sets then the statement $A \subseteq B$ is pronounced ``$A$
is a \textbf{subset} of $B$'' and means that every element in $A$ is also in $B$,
as a formula: $e \in A \Rightarrow e \in B$.

Whereas $\cap$ and $\cup$ are operations on sets, $\subseteq$ is a predicate on
sets and does not ``return a set''. As an analogy, $+$ is an operation on numbers
since $2 + 3$ is another number but $\leq$ is a predicate on numbers: $2 \leq 3$
is not a number. Similarly, the symbol $\in$ is a predicate on elements and sets.

Every set is a subset of itself, e.g. $A \subseteq A$: the predicate is similar
to $\leq$ and not $<$ on numbers. However, the structure created by the subset
operator is called a \emph{partial order} as opposed to the \emph{total order}
that you get on numbers:

\begin{itemize}
\item For numbers, $n \leq n$ for all $n$ and for sets, $A \subseteq A$ for all
$A$. This is called the \textbf{reflexive} property.
\item For numbers, if $m \leq n$ and $m \neq n$ then we cannot have $n \leq m$.
For sets, if $A \subseteq B$ and $A \neq B$ (which means there must be
at least one element in $B$ that is not in $A$) then we cannot have $B \subseteq A$.
This is called the \textbf{transitive} property.
\item For numbers, if $a \leq b$ and $b \leq c$ then $a \leq c$ too. For sets,
if $A \subseteq B$ and $B \subseteq C$ then $A \subseteq C$ too. This is called
the \textbf{transitive} property.
\end{itemize}

However, for numbers, for any two numbers $m, n$ with $m \neq n$ we have exactly
one of the cases $m \leq n$ or $n \leq n$, which makes $\leq$ a total order.
For sets, $\{1\}$ and $\{2\}$ are not the same but neither is a subset of the
other, so we have a partial order.

In proper set theory, we have to prove that $\subseteq$ is a partial order and
the way you prove things about sets is to use the definitions of elements. For
example, let's prove the transitive property. Let $A, B, C$ be any sets with
$A \subseteq B$ and $B \subseteq C$. We want to show $A \subseteq C$. There are
two cases: if $A$ is the empty set then $A \subseteq C$ is automatic since the
empty set is a subset of everything: there are no elements in $A$ that are not
in $C$. If $A$ is not empty then pick any element $a$ of $A$. The statement
$A \subseteq B$ means that for any $e$, $e \in A \Rightarrow e \in B$ so setting
$a = e$ we get $a \in B$. The statement $B \subseteq C$ means $e \in B \Rightarrow
e \in C$ so we get $a \in C$. This shows $a \in A \Rightarrow a \in C$. But this
is exactly the definition of $A \subseteq C$. \hfill q.e.d.

Using the subset operation, we can state that for sets we have
\[
A = B \iff A \subseteq B \wedge B \subseteq A
\]
This equivalence is useful because the way you prove that two sets $A$, $B$ are
equal is to show that $A \subseteq B$ and $B \subseteq A$.

\subsection{Cartesian products}

If $A$ and $B$ are sets then $A \times B$ is the set of all pairs of elements
$(a, b)$ where $a \in A$ and $b \in B$. For example if $A = \{\textrm{red},
\textrm{green}, \textrm{blue}\}$ and $B = \{\textrm{square}, \textrm{triangle}\}$
then
\[
A \times B = 
\left\{
\begin{array}{l}
(\textrm{red}, \textrm{square}),
(\textrm{green}, \textrm{square}),
(\textrm{blue}, \textrm{square}), \\
(\textrm{red}, \textrm{triangle}),
(\textrm{green}, \textrm{triangle}),
(\textrm{blue}, \textrm{triangle})
\end{array}
\right\}
\]

A pair is something different to a set: while $\{a, a\} = \{a\}$ and 
$\{a, b\} = \{b, a\}$, if $a \neq b$ then $(a, b) \neq (b, a)$ and $(a, a)$
is something different again.

\begin{diamondsec}
In formal set theory, the pair $(a, b)$ is defined as the nested set
$\{a, \{a, b\}\}$. One can then prove that this definition has all the usual
properties one would expect from a pair, even if $a$ and $b$ are themselves
sets. For practical purposes we can pretend that pairs are a separate type of
object to sets though.
\end{diamondsec}

The generalisation of a pair is a tuple, which is a list of a fixed length.
Formally, $(a, (b, c))$ is a pair one of whose elements is itself a pair whereas
$(a, b, c)$ is a triple, but in practice one can treat the two as interchangeable
in many cases. We can then write $A \times B \times C$ for the set of all triples
of elements $(a, b, c)$ with $a \in A, b \in B, c \in C$. For products of a single
set we can use ``powers'' notation, e.g. $\mathbb R^3$ is the set of all triples
of real numbers, which one could use for example to represent points in 3D space.

\subsection{Cardinality}

For a set $A$, we define $|A|$ to be the cardinality of $A$ as follows:
\begin{itemize}
\item The cardinality of a finite set is the number of elements that it contains,
so $|\emptyset| = 0$, $|\{1, 2, \ldots, n\}| = n$ and so on.
\item The cardinality of infinite sets is best left to professionals, who have
invented new numbers called \emph{infinite cardinals} to deal with the problem.
\end{itemize}

\subsection{Power sets}

For any set $S$, the \textbf{power set} $\mathcal P(S)$ is the set of all subsets of $S$.
This definition is allowed (it doesn't run into the problems with the ``set of
all sets'') and extremely useful.

First, a word on sets of sets. $\{1,2\}$ is a set with two elements whereas
$\{\{1, 2\}\}$ is a set with one element, that is itself a set (with two elements).
Similiarly, $\{\}$ is the empty set --- the only set with $0$ elements, whereas
$\{\{\}\}$ is a set with one element, that element being the empty set. One can
now construct a chain of sets of arbitrary depth out of nothing but the empty
set, the next one being $\{\{\{\}\}\}$.
Similarly, $\{\{\},\{\}\} = \{\{\}\}$ is a set containing one element, written in a
denormalised form whereas $\{\{\}, \{\{\}\}\}$ is a set of two elements, one being
the empty set and the other the set containing the empty set.

For an example of a power set,
\[
\mathcal P\{1,2,3\} = \{\{\}, \{1\}, \{2\}, \{3\},
\{1,2\}, \{1,3\}, \{2,3\}, \{1,2,3\}\}
\]

For a finite set with $n$ elements, the power set has $2^n$ elements, hence the
name. (Some authors write $2^S$ for $\mathcal P(S)$ too.)

\subsection{$\diamond$ Georg Cantor}

\begin{diamondsec}
Most of set theory was invented by Georg Cantor. He also chose the German
word \emph{Menge} which is translated as \emph{set} in English-speaking
mathematics.

It turns out that there are different kinds of infinite cardinality.
The cardinality of the natural numbers is usually written $\aleph_0$ (aleph-zero,
aleph is the first Hebrew letter).
The story goes that Cantor discovered that there is an ``infinity of infinities'' 
and found this
so awe-inspiring that he decided to use Hebrew letters to commemorate the
discovery. It is a matter of debate whether Cantor had Jewish ancestry and/or
would have seen himself as partly Jewish. Considering that at the time he lived
(Russian and German empires pre-World War I)
being Jewish could get you banned from academia and public life and there were
vigilante groups actively trying to expose any Jews, Cantor understandably did
not leave many hints to this effect. The matter of him introducing Hebrew
letters in mathematics is of course mentioned in such debates.

The idea behind extending cardinals to infinite sets is that two sets should have
the same cardinality if there is a bijetive mapping between them, whereas if 
there is an injective mapping from $A$ to $B$ then we should have $|A| \leq |B|$.
This implies that the cardinality of the set of even natural numbers must be the
same as that of the natural numbers, even if one might think that there are ``twice
as many'' numbers than even numbers, due to the bijection $n \mapsto 2n$.

With similar reasoning, Cantor showed that $|\mathbb N| = |\mathbb Z| = |\mathbb Q|$
and even, for every natural number $n$, $|\mathbb N^n| = |\mathbb N|$.
However, $|\mathbb N| < |\mathbb R|$, a famous result known as Cantor's diagonal
argument. Cantor also defined $\aleph_1$ to be the first infinite cardinal beyond
$\aleph_0$, and posed the problem of whether $|\mathbb R| = \aleph_1$. It turns
out that this question (the so-called \emph{continuum hypothesis}) is undecidable
in set theory as constructed by Cantor (and later by Zermelo and Fraenkel, commonly
called \emph{ZF set theory}).

The reason why the power set does not have the problems with the ``set of all
sets'' is that if you think as a set as a tree with one root node for the set
itself and a leaf for each element, then forming a new set out of existing sets ---
whether by taking the power set or otherwise --- can only create trees of finite
depth. The ``set of all sets'' would be not only a tree of infinite depth, but
it would contain a cycle in the tree since it is itself a set.

The power set also explains how Cantor got his ``infinity of infinities''. For
every set, $|S| < |\mathcal P(S)|$ since Cantor showed that you cannot create
an injective function from a power set to its base set but there is an
obvious one the other way round namely $x \mapsto \{x\}$. So by taking
$\mathbb N, \mathcal P(\mathbb N), \mathcal P(\mathcal P(\mathbb N)) \ldots$ you
get an infinite sequence of sets, each one strictly larger than the last.

The computer science version of this argument, discovered by Alan Turing, is
that 
\[
|\{\textrm{Turing machines}\}| = |\mathbb N| \textsf{ whereas }
|\{\textrm{languages over } \{0,1\}\}| = |\mathcal P(\mathbb N)|
\]
which means that almost all languages are Turing-undecidable.
\end{diamondsec}

\subsection{Functions}

A function from $A$ to $B$ can be thought of as a widget where you feed an
$a \in A$ in and a $b \in B$ comes out, and if you feed the same $a$ in
multiple times you also get the same $b$ out each time. If when you feed $a$
in to a function $f$ you get $b$ out, you can write $b = f(a)$.
A function is futher called
\begin{itemize}
\item \textbf{injective}, if whenever you feed in two different $a, a'$ then you
get different $b, b'$ out, that is for all $a, a'$ we have $a \neq a' \Rightarrow
f(a) \neq f(a')$.
\item \textbf{surjective}, if for every $b \in B$ there is at least one $a \in A$
so that $f(a) = b$.
\item \textbf{bijective}, if the function is both injective and surjective.
In this case there exists a function $g$ from $B$ to $A$ such that whenever
$f(a) = b$ then $g(b) = a$, in other words $g(f(a)) = a$ and $f(g(b)) = b$ for
all $a \in A, b \in B$. This $g$ is called the \textbf{inverse} of $a$.
\end{itemize}

In set theory, a function $f: A \to B$ is a subset of set $A \times B$ containing
exactly those pairs $(a, b)$ where $b = f(a)$. For example, the function
$f: \{0, 1, 2, 3\} \to \{0, 1\}$ that maps $0$ and $2$ to $0$ and that maps 
$1$ and $3$ to $1$ is formally the set
\[
    f = \{(0, 0), (1, 1), (2, 0), (3, 1) \}
\]

To be precise, the function arrow $\to$ in set theory is itself a set operation,
just like $\cup$. For any two sets $A, B$ the set $A \to B$ is the set of all
functions from $A$ to $B$, that is the set of all sets of pairs $(a, b) \in A \times B$
which contain each $a \in A$ exactly once on the left-hand side of a pair.
When we write $f: A \to B$ what we are really saying is $f \in A \to B$, i.e.
$f$ is an element of the set of functions $A \to B$.

For a function $f \in A \to B$, $A$ is called the \textbf{domain} of $f$ and
$B$ is called the \textbf{range} of $f$. The set of all $b \in B$ that $f$ can
actually hit, e.g. set of $b$ for which there exists some $a \in A$ such that
$f(a) = b$ is called the \textbf{image} of $f$. The image and the range are the
same exactly when the function is surjective.

\subsection{Set restriction and projection}

If we have a set $A$, there are two ways to create new sets from it using a function.
Both use similar notation and are often confused.

(1.) If $f: A \to B$ is a function out of $A$ then a \textbf{restriction} of $A$ is
a set written
\[
    \{ a \in A \mid f(a) = b \}
\]
which is a subset of $A$, containing all elements $a$ where $f(a) = b$.
You read this as ``the (sub)set of all $a \in A$ for which $f(a) = b$''.

A \textbf{predicate} on $A$ is a function $p: A \to \{\textrm{true}, \textrm{false}\}$.
A special case of restriction is that $\{ a \in A \mid p(a) \}$ is the subset
of $A$ containing all elements $a$ on which the predicate evaluates to true.
This is called the restriction of $A$ to $p$ and is how restriction is usually
introduced in maths courses.

(2.) Again if $f: A \to B$ then
\[
    \{f(a) \mid a \in A\}
\]
is a subset of $B$ (not of $A$) called a \textbf{projection} of $A$ using $f$.
Of course, this is just another name for the image of $f$.

In some programming lanuage, this is also called \textbf{comprehension}. For
example in python, \texttt{[2*x for x in N]} is a list comprehension that takes
an input list N and creates a new list whose elements are double those of the
original list. Of course, the point of list comprehension syntax is that the
list is not evaluated up front, so you can use this syntax on an infinite list
N of natural numbers and as long as you only read the resulting list 10 times, 
you will get the first 10 even numbers.

\subsection{Preimages and Images}

Start with sets $A, B$ and a function $f: A \to B$. If the function $f$ is
bijective then there exists an inverse function $g: B \to A$ such that for all
$a \in A$ we have that if $b = f(a)$ then $g(b) = a$. Sensibly enough, one can
call an inverse function $g$ like this $f^{(-1)}$. For example, if $f: \mathbb R
\to \mathbb R$ with $f(x) = 2x+1$ then $f^{(-1)}(y) = (y-1)/2$.

The notation $f^{(-1)}$ is often misused for something else too, which we will
avoid here by borrowing other notation that already exists for this purpose in
other parts of mathematics.

If $f: A \to B$ is a function then the function $f^*: \mathcal P(B) \to \mathcal P(A)$
is defined as 
\[
    f^*(T) = \{a \in A \mid f(a) \in T \}
\]
You apply $f^*$ to a subset $T$ of $B$ and get back a subset of $A$ containing all
the elements $a$ where, if you apply $f$ to these elements then you land in $T$.
This subset of $A$ is called the \textbf{preimage} of $T$ under $f$.
The function $f^*$ exists for every function, whether injective or not. If you
apply it to all of $B$ then $f^*(B)$ is simply the image of $f$.

The other way round, for every function $f: A \to B$ there is a function
$f_*: \mathcal P(A) \to \mathcal P(B)$ defined as 
\[
    f_*(S) = \{ f(s) \mid s \in S \}
\]
that takes a subset $S \subseteq A$ instead of a single element $a$ and gives
you the set of values $f(s)$ for all elements of $S$. This is called the
\textbf{direct image} of $S$ under $f$.

The intuition for preimages and direct images is that you start with a subset
of $B$ (resp. $A$) and find the ``matching'' subset on the other side. Because
you are working with subsets instead of individual elements, it does not matter
if $f$ is not bijective:
\begin{itemize}
\item If $f$ is not injective, there might be two elements $s, s'$ with $f(s) =
f(s')$. In this case $\{f(s), f(s')\} = \{f(s)\}$, so the use of sets ``eats up''
repeated elements. The other way round, the preimage of a set with a single element
might be a set with more than one element.
\item If $f$ is not surjective, taking the inverse image of something in $B$
that $f$ cannot reach simply gives you the empty set.
\end{itemize}

To denote that the preimage and direct image functions go from subsets to subsets,
their domains and ranges are defined as the power sets of the original sets.
