\ifdefined\booklet
\else
\documentclass{scrartcl}

\include{header}

\setuplecture{13}{Coding Theory}

\begin{document}

\titlestuff

\noindent\emph{Learning outcomes:} After this lecture and revision, you should
be able to
\begin{itemize}
\item Explain the main definitions of coding theory.
\item Encode and decode using repetition and checksum codes
      (where a checksum function is provided).
\item Compute the information length, redundancy and information rate of a code
      and compare different codes using these measures.
\item Define and compute Hamming weights and distances over finite fields.
\item State the Singleton bound.
\end{itemize}
\fi

\section{Coding Theory}

Coding Theory studies the problem of transmitting information reliably across a 
\emph{channel}. There are two main kinds of channels:

\begin{itemize}
\item Channels across space: these include wired links (such as telephone lines,
undersea cables etc.) and wireless links. Sending a message via semaphore flags
also counts as a channel across space, as does dictating your message to a
messenger who should travel to the intended recipient and repeat your message
to them.
\item Channels across time: these are more prosaically known as `disks' or
`documents'. You store information on a disk or write it down on paper, with the
intention that you can read it again at a later point in time.
\end{itemize}

There are two problems that you need to address before sending information
across a channel. The first is that a channel can only transmit a particular
kind of information, so you need to \emph{encode} your information in a way that
the channel can handle. To store a text on a disk or send it across the internet,
the message has to be somehow encoded as a sequence of 0s and 1s. In a more
general sense, if you want to produce a 2D drawing of a 3-dimensional object,
you have to choose a particular perspective to draw from, which could also be
seen as a kind of encoding. This kind of encoding is called \hi{source coding}
and the aim is to encode as efficiently as possible.

The second problem is that channels can make mistakes. In Coding Theory the term
\emph{noise} is used for any kind of mistake in a channel, since at the time
when the theory was being written, telephone wires and radio links were the main
way of getting information from one place to another quickly, and any form of
electrical interference (such as bad weather, or unreliable equipment) would
produce literal noise in the channel. Encoding can help solve this problem too:
ideally you encode your message in such a way that even if it gets distorted by
noise, the same message still comes out at the other end. The next best thing is
to encode in such a way that you can at least detect if your message got
corrupted in the channel, and arrange to send it again. This is called
\hi{channel coding} and the aim is usually to get probability of correct
transmission as high as possible, or at least above a certain limit.

\needspace{12\baselineskip}
\subsection{Terminology}

We define the following terms to talk about channels in more detail.

\begin{itemize}
\item We work over an \hi{alphabet} $\Sigma$, that is a finite set with at least
two elements. Sometimes it is also a finite field, 
most often simply $\mathbb B = \{0, 1\}$.

\item $\mathcal M = \Sigma^k$ is a set called the \hi{message space}, with
$k$ the message length.

\item An (information) \hi{source} is something that outputs a message $m \in 
\mathcal M$. A \hi{destination} is something that can input a message $m \in 
\mathcal M$. The problem we are trying to solve is to get a message $m$ from a
source to a destination.

\item A \hi{channel} has an associated input space $\mathcal X$ and an output 
space $\mathcal Y$. When you send an $x \in \mathcal X$ to the channel, it 
outputs a value $r \in \mathcal Y$ ``at the other end''.

\item An \hi{encoding} (procedure) is a function $E: \mathcal M \to \mathcal 
X$ that takes a message $m$ that you want to send and returns an $x \in \mathcal X$
that a channel can handle. Encoding functions must be injective.

\item A \hi{decoding} (procedure) is a function $D$ that takes a $r \in 
\mathcal Y$ as input and outputs either a message $m \in \mathcal M$ or a 
special symbol $\bot \notin \mathcal M$ which indicates that decoding failed.

\item A \hi{code} is formally a subset of $\mathcal X$, namely the image of the
encoding function. Occasionally it is important to distinguish different encoding
and decoding functions that use the same code.
\end{itemize}

A channel is not a function: if you input the same message $x$ several times,
you might get a different output each time. You can think of the channel as
outputting ``$x$ $+$ noise'', but the noise is random and may be different each
time you send a value $x$.

In these lecture notes, we use \emph{encoding} and \emph{decoding} to mean
procedures to get a message correctly across a channel, not ways to keep a message
secret (that would be encryption and decryption).

\needspace{2\baselineskip}
Graphically, the situation looks like this:

\begin{center}
\begin{tikzpicture}[x=3.1cm]
\node[draw] (source) at (0, 0) {Source};
\node[circle,draw] (encode) at (1, 0) {E};
\node[draw] (chan) at (2, 0) {Channel};
\node[circle,draw] (decode) at (3, 0) {D};
\node[draw] (dest) at (4, 0) {Dest};

\draw[->] (source) to node[above] {$m \in \mathcal M$} 
                      node[below] {message} (encode);
\draw[->] (encode) to node[above] {$x \in \mathcal X$} 
                      node[below] {} (chan);
\draw[->] (chan) to   node[above] {$r \in \mathcal Y$} 
                      node[below] {} (decode);
\draw[->] (decode) to node[above] {$m' \in \mathcal M$}
                      node[below] {or $\bot$} (dest);
                      
\node (noise) at (2, 1) {Noise};
\draw[->] (noise) to (chan);
\end{tikzpicture}
\end{center}

In many cases, a channel has $\mathcal X \subseteq \mathcal Y$ and when it 
operates without errors, it outputs the same value that it got as input. In 
this case, for a sensible pair of encoding and decoding procedures we should 
have the equation that for all messages $m \in \mathcal M$ we have $D(E(m)) = 
m$, in other words if the channel makes no errors then you get the same message 
out that you put in.

\subsection{The repetition code}

The simplest way to encode a message with some redundancy is to send it multiple
times.
Using notation borrowed from theory of computation, we write $m^s$ to
mean message $m$ repeated $s$ times.
The $s$-times repetition code for message space $\mathcal M = \Sigma^k$
is the following code:

\begin{itemize}
\item This code applies to a channel with $\mathcal X = \mathcal Y = 
\Sigma^{s \times k}$ (that is, $n = s \times k$).
\item To encode, repeat $m$ exactly $s$ times: $E(m) = m^s$.
\item To decode a string $r \in \Sigma^{s \times k}$, check if it is the
$s$-times repetition of some message $m \in \Sigma^k$:
\[
    D(r) = \left\{ \begin{array}{ll}
    m, & r = m^s \text{ for some $m \in \Sigma^k$}\\
    \bot, & \text{otherwise}
    \end{array} \right.
\]
\end{itemize}

This certainly meets the condition $D(E(m)) = m$. For any $s \geq 2$, this code 
will detect at least a single \hi{error}, meaning a single symbol that gets 
flipped to some other symbol: if exactly one symbol gets flipped then the parts 
of the received word $r$ will not match up. However, if $s$ symbols get flipped 
then the worst case is that these symbols are e.g. all copies of the first 
symbol of the message $m$ and they all get flipped to the same other symbol, so 
an incorrect $m'$ would result. In the form presented here, the $s$-times 
repetition code can always \hi{detect} up to $s-1$ errors and \hi{correct} no 
errors.

Can we do better than this? For $s = 2$, if a single symbol gets flipped, we 
know the position where something went wrong but not which of the two is the 
correct one. For $s = 3$, if at most one error occurs then for each symbol, at
least 2 of 3 copies will agree and we can pick that symbol.
For all $s$ we can define the following alternative 
decoding procedure: for $i = 1$ to $n$, if a majority of the copies of the 
$i$-th symbol agree, then take that one; return a message if this rule applies 
to all $n$ positions. This is an example of a different decoding procedure for
the same code as before, which is one reason to keep the notions of code and
encoding/decoding procedure separate.
This code can reliably correct up to one error but on the other hand, it cannot 
detect two errors anymore.

Of course, the code might correct up to $n$ errors if they all happen in 
different positions, for example if $s = 3$ and the first copy of the message 
gets all its symbols flipped then the other two copies will sill yield the 
correct result. By \emph{(reliably) detect / correct} errors, we mean a 
worst-case analysis.

For $s = 4$ with the majority decoding rule, we can correct 1 error and detect
up to two. Generally, the repetition code corrects up to $\lfloor (s-1)/2 \rfloor$
errors and detects up to $\lfloor s/2 \rfloor$, where $\lfloor \cdot \rfloor$
means \emph{rounded down}. On the other hand, the strict decoding rule shown
earlier can detect up to $s-1$ errors but correct none.

\subsection{Checksums}

The repetition code is not particularly efficient. To detect even a single 
error reliably, you need to transmit $2k$ symbols for a $k$-symbol message. But 
you can do the same with $k + 1$ symbols: simply add a \hi{checksum} symbol on 
the end. If $\Sigma = \{0, 1\}$, the checksum can be a single bit that is the 
sum modulo $2$ of all bits in the message, which will catch any 1-bit errors.

More formally, if we have a checking function $f: \Sigma^k \to \Sigma^\ell$
then the checksum code for $f$ is as follows:
\begin{itemize}
\item This code applies when $\mathcal X = \mathcal Y = 
\Sigma^{k + \ell}$ (that is, $n = k + \ell$).
\item $E(m) = m || f(m)$ where $||$ means concatenation.
\item To decode, recompute the checksum:
\[
    D(r) = \left\{ \begin{array}{ll}
    m, & r = m||c \text{ for } |m| = k \text{ and } |c| = \ell
    \text{ and } c = f(m)\\
    \bot, & \text{otherwise}
    \end{array} \right.
\]
\end{itemize}

What a checksum code can detect depends on the checking function $f$ (and in
the currently defined form it cannot correct anything).

For $|\Sigma| = n$, if we label the elements of $\Sigma$ with the numbers
$0, 1, \ldots (n-1)$ then the function $f(m) = \sum_{i=1}^k m_i \bmod{n}$
which adds the elements modulo $n$ can detect a single error while
using only one extra symbol to do so. Let's prove this.

Take any message $m = m_1 \ldots m_k$ and compute its checksum $c = (m_1 + 
\ldots + m_k) \bmod{n}$. If the $j$-th symbol gets changed from $m_j$ to 
$m'_j$, then the new checksum is $(c - m_j + m'_j) \bmod{n}$ since we have taken 
out a $m_j$ symbol and put a $m'_j$ symbol in. Unless $m_j = m'_j$, the new 
checksum cannot be identical to the old one, so an error in a single message 
symbol will always get detected. Of course, an error in the checksum symbol 
will also get detected, by the definition of the decoding procedure.

We can generalise this approach. Choosing $\Sigma = \mathbb Z_n$ is no
restriction as we can label the elements of any set of $n$ elements with the
integers $0, \ldots, n-1$.

\begin{definition}
For $\Sigma = \mathbb Z_n$ and $\mathcal M = \Sigma^k$, the linear checksum code
defined by vector $\vec a \in \Sigma^k$ is the checksum code with
\[
    f(\vec m) = \langle \vec a, \vec m \rangle = \sum_{i=1}^k a_i \times m_i
\]
\end{definition}

If we choose our coefficients carefully, we can detect all single-symbol errors
and some transpositions. The first obvious thing to do is not to have any zero
coefficients. To catch transposition errors, adjacent coefficients should differ.

\begin{proposition}
A linear checksum code has the following properties:
\begin{itemize}
\item It detects any one error if none of the $a_i$ is zero or a zero-divisor.
\item If $n$ is prime (so we are working over a field), $a_i \neq 0$ for all $i$
and $a_i \neq a_{i+1}$ for all $i < k$ and $a_k \neq 1$ then it also detects all
transpositions of two adjacent symbols.
\end{itemize}
\end{proposition}

For the case of a single error, the argument is the same as above: suppose $m_j$
is replaced with $m'_j$. This changes the expected checksum to
$c' = c - a_j \times (m_j - m'_j)$. If $a_j$ is not zero or a zero-divisor then
this implies $c' \neq c$ whenever $m_j \neq m'_j$, so we detect the error.

For transposition errors, suppose we swap $m_j$ and $m_{j+1}$. This changes the
checksum to $c' = c - (a_j m_j + a_{j+1} m_{j+1}) + (a_j m_{j+1} + a_{j+1} m_j)$
so if the checksum were still correct ($c = c'$) then because $a_j \neq a_{j+1}$
we can substitute $a_{j+1} = a_j + \delta$ to get $\delta(m_j - m_{j-1}) = 0$.
As we are in a field and $\delta \neq 0$, this implies $m_j = m_{j-1}$ so we
only fail to detect the transposition if the two adjacent digits were identical,
in which case no error has occurred. (This argument would also work in $\mathbb
Z_n$ if $\delta$ is not a zero-divisor, but as we will see in a moment this is
not much use.)

The condition that $a_k \neq 1$ is to guard against transposition of the check
digit with the last message digit, which can be shown to be detected with a
similar argument to the one we just made.

\subsection{Examples}

The \hi{international standardised book number} (ISBN) is a 9-digit number
followed by a single check symbol. The digits are usually written in groups of
1, 3, 5 and 1 e.g. 0--444--85193--3. While the first nine digits are all in the
range $0 \ldots 9$, the checksum is $d = \sum_{i=1}^9 i \times d_i \bmod{11}$
where $d_i$ is the $i$-th digit. If the check digit turns out to be 10, it is
replaced by the letter X. This not only has the benefit of detecting any 1-digit
errors, but it also catches the single most common type of error that humans
make: transposing two digits. ISBNs are meant to be entered by humans, after all.

Suppose that a human has swapped digits $x$ and $y$. The checksum of the 
correct ISBN will be $d_1 + 2 d_2 + \ldots 9 d_9$, whereas the checksum of the 
ISBN with the two digits swapped is $d_1 + 2 d_2 + \ldots 9 d_9 - (x d_x + y 
d_y) + (x d_y + y d_x)$ by subtracting out the two correct digits that were not 
typed and replacing them with the two swapped digits that were typed. 
Subtracting the new from the old checksum cancels the first 9 terms and gives 
$(x-y)(d_y - d_x)$. The new checksum will only be equal to the old one, which 
would mean the transposition is not detected, if this difference is zero. Over 
a finite field (namely $\mathbb F_{11}$) a product is only zero if at least one 
of its factors is zero, otherwise we would have found a zero divisor. Since 
both $x$ and $y$ are distinct field elements, their difference cannot be zero. 
This leaves $d_y - d_x$ which is only zero if the two digits are equal, in 
which case no error has occurred.
We can interpret this as follows: ISBN treats the digits as elements of the next
larger field in which they can be embedded, in this case $\mathbb Z_{11}$, then
applies a linear checksum of the type we just discussed.

\hi{UPC 12-digit barcodes} use only the 10 decimal digits. There is an encoding
of the digits into a sequence of bars and spaces, which we will not discuss in
detail here (explanations can be found online). Instead we will focus on the
checksum in the final digit. The formula is
\[
    c = - \sum_{i=1}^{11} a_i \times d_i \qquad \text{ with } a_i =
    \left\{ \begin{array}{ll}
        3, & i \text{ odd} \\
        1, & i \text{ even}
    \end{array}\right.
\]
The minus at the start means that a codeword is correct if $\sum_{i=1}^{12}
a_i \times d_i = 0$ with $a_{12} = 1$. This detects all single-digit errors and
most transpositions, but it will not detect swapping e.g. 05 for 50 since
$1 \times 0 + 3 \times 5 = 1 \times 5 + 3 \times 0 = 5$ modulo $10$. The issue
here is that $3 - 1 = 2$ is a zero-divisor modulo $10$.

\begin{exercise}
$(\star)$ If the UPC code chose coefficient $2$ instead of $3$ for digits in odd
places, it would detect all transposition errors. What is the problem with this
variant of the UPC code?
\end{exercise}

The \hi{Luhn code} does have something like alternating coefficients of $1$ and
$2$, but uses a different approach to summing so it is not a linear checksum.

\begin{definition}[Luhn code]
For the alphabet $\Sigma = \mathbb Z_{10}$ and message space $\mathcal M = \Sigma^k$,
the Luhn code is the following code.
\begin{itemize}
\item Start with a checksum of $c = 0$.
\item For each digit in an odd-numbered position, add the digit to the checksum.
(If the message has an odd length, reverse ``odd'' and ``even'' in this description
--- so that the coefficient of the last message digit is never $1$.)
\item For each digit in an even-numbered position, double the digit. If this
produces a two-digit number, then add the two digits together. Add the resulting
digit to the checksum.
\item The final check digit is $10 - c$ (for the same reason as with the UPC code,
to make a codeword correct if it sums to $0$).
\end{itemize}
\end{definition}

This code detects all single-digit errors and ``most'' transpositions; it will
catch changing 05 for 50 but not 09 for 90 since $2 \times 9 = 18$ but $1 + 8 = 9$
again. The Luhn algorithm is used among other things in credit card numbers,
which is why websites can show an error message if you make a single typo in your
card number before sending the data off to the bank to be validated. The Luhn
algorithm has the additional advantage that the checksum does not change if you
store any number of leading zeroes before the message, which is what you do when
you need to store variable-length decimal numbers in a fixed-length data
structure. For this to work, the Luhn checksum is computed from right to left
so that the last message digit always gets doubled.

\ifdefined\booklet
\else
\end{document}
\fi
