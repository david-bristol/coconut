
\subsection{Polynomial spaces as vector spaces}

We look at the space $V = GF(p)^n$ constructed by taking a finite field
$GF(p^n) = GF(p)[X]/q(X)$ modulo an irreducible polynomial $q$ of degree $n
\geq 1$ and interpreting it as a vector space.
The elements of this space are of the form $(c_0, c_1, \ldots, c_{n-1})$ which
could be written as $c_0 + c_1 X + \ldots + c_{n-1} X^{n-1}$.
Interpreting it as a vector space means forgetting about multiplication of
$GF(p^n)$ elements but adding multiplication with $GF(p)$ elements: $a \times
(c_0, \ldots, c_{n-1}) := (a \times c_0, \ldots, a \times c_{n-1})$.

One basis of this vector space consists of vectors $\vec b_i$ for $i = 0$ to
$n-1$ where $\vec b_i$ is $1$ at position $r$ and $0$ elsewhere. Written as
polynomials, the $i$-th basis vector $\vec b_i$ is the monomial $X^i$.

The map $GF(p) \to GF(p^n), a \mapsto (a, 0, \ldots, 0)$ is a field
homomorphism. It is sometimes called the embedding of the base field $GF(p)$
into the extension field $GF(p^n)$. This map commutes with field multiplication
in the following way: for any element $a$ of $GF(p)$ and any element $\vec v$
of $GF(p^n)$, you get the same if you perform the scalar multiplication $a
\times \vec v$ or if you embed $a$ in $GF(p^n)$ and then do field multiplication
there. We can express this in a diagram.

\begin{center}
\begin{tikzpicture}
\node[align=left] at (0, 0) (s) {$a \in GF(p)$ \\ $\vec v \in GF(p^n)$};
\node[align=left] at (0, -3) (t) {$(a v_0, \ldots a v_{n-1}$)};
\draw[-triangle 60] (s) to node[left,align=left] {scalar\\mult.} (t);
\node[align=left] at (8, 0) (u) {$(a, 0, \ldots, 0) \in GF(p^n)$ \\
    $\vec v = (v_0, \ldots, v_{n-1}) \in GF(p^n)$};
\draw[-triangle 60] (s) to node[above] {embedding} (u);
\draw[-triangle 60] (u) to node[below right,align=left] {$GF(p^n)$\\mult.} (t);
\end{tikzpicture}
\end{center}

\subsection{Automorphisms revisited}

An automorphism $f$ of $GF(p^n)$ can be represented as a $n \times n$
matrix, since such a $f$ must be linear over the field $\mathbb F$: if $a \in
\mathbb F$ and $\vec v \in V$ then $f(a \vec v) = a \times f(\vec v)$.
However, we know that field automorphisms cannot change degree-$0$ polynomials:
$f(1, 0, \ldots, 0) = (1, 0, \ldots, 0)$. Writing $f$ out as a matrix, we see
\[
\left(\begin{array}{llll}
f_{0, 0} & f_{0, 1} & \cdots & f_{0, n-1} \\
f_{1, 0} & f_{1, 1} & \cdots & f_{1, n-1} \\
\vdots & \vdots & \ddots & \vdots \\
f_{n-1, 0} & f_{n-1, 1} & \cdots & f_{n-1,n-1}
\end{array}\right)
\left(\begin{array}{r}
1 \\ 0 \\ \vdots \\ 0
\end{array}\right)
=
\left(\begin{array}{l}
f_{0, 0} \\ f_{1, 0} \\ \vdots \\ f_{n-1, 0}
\end{array}\right)
=
\left(\begin{array}{r}
1 \\ 0 \\ \vdots \\ 0
\end{array}\right)
\]
so the zeroth column of the matrix of $a$ must start with a $1$ and be zero
everywhere else. Since we also know how to compute $f(X^2)$ from $f(X)$, this
means we know how to compute $f(0, 0, 1, 0, \ldots, 0)$ from $f(0, 1, 0,
\ldots, 0)$ and so on --- so all the information about $f$ is contained in the
first column of the matrix of $f$ and we can always compute the other columns
from it.

We look at two examples. The first is $GF(7^2)$ where for $p(X) = X^2 + X + 6$.
we found two automorphisms $id, \phi$ with $id(X) = X$ and $\phi(X) = 6 + 6X$
(the Frobenius map). As matrices, these are

\[
id = \left(\begin{array}{ll}
1 & 0 \\ 0 & 1
\end{array}\right) \qquad
\phi = \left(\begin{array}{ll}
1 & 6 \\ 0 & 6
\end{array}\right)
\]
It is now obvious how to calculate $\phi$ on an arbitrary field element $(a +
bX)$:
\[
\phi(a + bX) = \left(\begin{array}{ll} 1 & 6 \\ 0 & 6 \end{array}\right) \times
\left(\begin{array}{l}a \\ b \end{array}\right) =
\left(\begin{array}{r}a + 6b \\ 6b \end{array}\right)
\]

If we introduce another irreducible polynomial $q(Y) = Y^2 + 1$, the
isomorphisms we found between these representations last time were $f_1(a, b)
= (a+3b, 2b)$ and $f_2(a, b) = (a+3b, 5b)$. If we know $f_1$, we can calculate
$f_2$ by multiplying from the right with the Frobenius map:
\[
f_1 \phi =
\left(\begin{array}{ll} 1 & 3 \\ 0 & 2 \end{array}\right) \times
\left(\begin{array}{ll} 1 & 6 \\ 0 & 6 \end{array}\right) =
\left(\begin{array}{ll} 1 & 3 \\ 0 & 5 \end{array}\right) = f_2
\]

Matrices give us an easy way to find the inverses of isomorphisms, that is the
isomorphisms of $GF(7^2)$ from the representation modulo $q(Y)$ back to the
representation modulo $p(X)$. All we need to do is invert the matrices of $f_1,
f_2$:
\[
\m{1 & 3 \\ 0 & 2}^{(-1)} = \m{1 & 2 \\ 0 & 4} \qquad
\m{1 & 3 \\ 0 & 5}^{(-1)} = \m{1 & 5 \\ 0 & 3}
\]
Giving $f_1^{(-1)}(a +bX) = (a+2b) + 4bX$ and $f_2^{(-1)}(a+bX) = (a+5b) +
3bX$.

Our second example is $GF(2^3)$, this time with the irreducible polynomial
$p(X) = X^3 + X + 1$. The Frobenius map sends $X \mapsto X^2$ and $X^2 \mapsto
[X^4] = X^2 + X$. As a matrix, we get
\[
\phi = \m{1 & 0 & 0 \\ 0 & 0 & 1 \\ 0 & 1 & 1}, \qquad
\phi \m{a \\ b \\ c} = \m{a \\ c \\ b + c}
\]
from which we read off $\phi(a + bX + cX^2) = a + cX + (b+c)X^2$. Finding the
other automorphisms is easy too:
\[
\phi^2 = \m{1 & 0 & 0 \\ 0 & 0 & 1 \\ 0 & 1 & 1} \times
\m{1 & 0 & 0 \\ 0 & 0 & 1 \\ 0 & 1 & 1} =
\m{1 & 0 & 0 \\ 0 & 1 & 1 \\ 0 & 1 & 0}
\]
which maps $X \mapsto X + X^2$ (middle column) and $X^2 \mapsto X$ (right
column). Multiplying with the column vector $(a; b; c)$ we get $\phi^2(a + bX +
cX^2) = a + (b+c)X + b X^2$. If we look at the third power
\[
\phi \times \phi^2 = \m{1 & 0 & 0 \\ 0 & 0 & 1 \\ 0 & 1 & 1} \times
\m{1 & 0 & 0 \\ 0 & 1 & 1 \\ 0 & 1 & 0} =
\m{1 & 0 & 0 \\ 0 & 1 & 0 \\ 0 & 0 & 1}
\]
we get the identity map back, as expected. If we take the isomorphism $f(X) =
Y^2 + 1$ into the representation modulo $q(Y) = Y^3 + Y^2 + 1$, we find $f(X^2)
= Y^2 + Y$. The other isomorphisms are
\[
f_1 \phi =
\m{1 & 1 & 0 \\ 0 & 0 & 1 \\ 0 & 1 & 1}
\m{1 & 0 & 0 \\ 0 & 0 & 1 \\ 0 & 1 & 1} =
\m{1 & 0 & 1 \\ 0 & 1 & 1 \\ 0 & 1 & 0} = f_2
\]
\[
f_1 \phi^2 = f_3
\]
from which we read off the columns $f_2(X) =  Y^2 + Y$ and $f_2(X^2) = 1+Y$,
giving $f_2(a+bX+cX^2) = (a+c) + (b+c)Y + bY^2$. We could read this last
formula off the rows of the matrix directly, since we get the last formula by
multiplying the matrix of $f_2$ with the column vector $(a; b; c)$.
To invert the isomorphisms, one again just needs to invert the matrices.

\begin{exercise}
\emph{The rest of the example.}
\begin{itemize}
\item $(\star)$ Compute $f_3 = f_1 \phi^2$, then write out the expressions for
$f_3(X)$, $f_3(X^2)$ and $f_3(a + bX + c X^2)$.
\item $(\star\star)$ Invert the matrix for $f_1$ to get the inverse
isomorphism. Note: matrix inversion is a lot easier in $GF(2)$ as $1+1=0$ so
you never need to multiply rows through to cancel constants!
\end{itemize}
\end{exercise}

\begin{exercise}
$(\star)$ \emph{More finite fields.}
Consider the field $GF(3^3)$ with the irreducible polynomials $p(X) = X^3 + 2
X + 1$ and $q(Y) = Y^3 + 2 Y^2 + Y + 1$.
\begin{enumerate}
\item Find the Frobenius map $\phi$ as a $3 \times 3$ matrix modulo $p(X)$.
\item Find the powers of the Frobenius map.
\item One isomorphism $f$ between the representations $p(X)$ and $q(Y)$ has
$f(X) = 2Y^2 + 2Y$. Find the matrix of $f$.
\item Find the inverse of $f$ by inverting the matrix of $f$.
\item How many isomorphisms are there between the two representations?
\item Find the other isomorphisms by matrix multiplication using the matrix of
$f$ and the Frobenius map $\phi$.
\item $(\star\star)$
Here is another way to find the Frobenius map in the representation
modulo $q(Y)$. We have the following situation with $V = GF(3)[X]/p(X)$ and $W
= GF(3)[Y]/q(Y)$:
\begin{center}
\begin{tikzpicture}
\node (v1) at (0, 0) {$V$};
\node (v2) at (0, -2) {$V$};
\draw[->] (v1) to node[left] {$\phi$} (v2);
\node (w1) at (2, 0) {$W$};
\node (w2) at (2, -2) {$W$};
\draw[->] (w1) to node[right] {$\hat\phi$} (w2);
\draw[->] ([yshift=0.1cm]v1.east) to node[above] {$f$} ([yshift=0.1cm]w1.west);
\draw[->] (w1) to node[below] {$f^{(-1)}$} (v1);
\draw[->] ([yshift=0.1cm]v2.east) to node[above] {$f$} ([yshift=0.1cm]w2.west);
\draw[->] (w2) to node[below] {$f^{(-1)}$} (v2);
\end{tikzpicture}
\end{center}
From this we see that the Frobenius map in $W$ has matrix $\hat\phi = f \times
\phi \times f^{(-1)}$. Compute $\hat\phi$ this way.
\end{enumerate}
\end{exercise}

\begin{diamondsec}
The conditions for field automorphisms say that $f(\vec v + \vec w) = f(\vec v)
+ f(\vec w)$, $f(\vec v \times \vec w) = f(\vec v) \times f(\vec w)$ and $f(1, 0,
\ldots, 0) = (1, 0, \ldots, 0)$ since this element is the one of the field.
Since the scalar multiplication $a \times \vec v$ we get for $V$ as a vector
space is equivalent to the vector multiplication $(a, 0, \ldots, 0) \times \vec
v$, a field automorphism must satisfy $f(a \times \vec v) = f((a, 0, \ldots,
0)\times \vec v) = f(a, 0, \ldots, 0) \times f(\vec v) = (a, 0, \ldots, 0) \times
f(\vec v) = a \times f(\vec v)$. This explains why in field multiplication we
get $f(a \times \vec v) = a \times f(\vec v)$ with the $a$ appearing ``outside
$f$'' whereas the automorphism rule says $f(\vec v \times \vec w) = f(\vec v)
\times f(\vec w)$.
\end{diamondsec}
