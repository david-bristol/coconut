\ifdefined\booklet
\else
\documentclass{scrartcl}

\include{header}

\setuplecture{17}{Information Theory}

\begin{document}

\titlestuff

\noindent\emph{Learning outcomes:} After this lecture and revision, you should
be able to
\begin{itemize}
\item Define and construct prefix-free codes and explain why they are useful.
\item Create the decoding tree for a given encoding function.
\end{itemize}
\fi

\section{Information Theory}

\subsection{Mathematical Preliminaries}

We begin with a quick reminder of some mathematical concepts. A \hi{function} 
$f: X \to Y$ is something where you can repeatedly stick an $x \in X$ in and 
get a $y \in Y$ out; if on input $x$ you get $y$ we can write $y = f(x)$.
The set $X$ is called the \hi{domain} of the function and $Y$ is its \hi{range}
(sometimes also called \emph{codomain}).

A function is \emph{memoryless} in the sense that if you stick the same $x$ in 
multiple times, you get the same $y$ out each time. We can visualise a function 
as a table with two columns, the left one listing all $x \in X$ and the right 
one listing $f(x)$ next to each $x$.

A function is called \hi{injective} if $f(x) = f(x')$ only occurs when $x = x'$,
equivalently $x \neq x' \Rightarrow f(x) \neq f(x')$. A function is called
\hi{surjective} if for every $y \in Y$ there is at least one $x \in X$ such that
$f(x) = y$. If a function is both injective and surjective then it is called
\hi{bijective} and this is equivalent to the function being invertible, that is
there is an inverse function $g: Y \to X$ such that for all $x \in X, y \in Y$
we have $f(x) = y \iff g(y) = x$.

The \hi{image} of a function is the set of all $y \in Y$ for which there is some
$x \in X$ with $f(x) = y$; if a function is surjective then the image is the same
as the range, otherwise the image is a strict subset of the range. If a function
is injective then there is a function $g: I \to X$ where $I$ is the function's
image such that $f(x) = y \iff g(y) = x$. In other words, an injective function
is invertible on its image.

\subsection{Source coding}

We define an \emph{alphabet} to be a finite set of size at least 2. For a set
$S$, the set $S^+$ is the set of nonempty words of finite length over $S$.

We have a message $m$ over some alphabet $\Sigma$, for example the set of all
letters, numbers, and some punctuation. We would like to send this message over
a channel that has a different alphabet $X$, typically $\{0, 1\}$. It is not a
problem if the channel alphabet is smaller than the message alphabet as long as
we can encode a message symbol as more than one channel symbol.

This is called \hi{source coding} since we are trying to build an encoder for 
messages coming from a certain source. In source coding, we do not care about 
error correction but we do care about efficiency. We model a \hi{message 
source} as a probability distribution over elements of $\Sigma$ and try to make 
our encoding as efficient as possible \emph{on average}, that is to minimise 
the expected length of an encoded message.

We are looking for an encoding scheme $(E, D)$ with $E: \Sigma \to X^+$ and
$D: X^+ \to \Sigma$ to encode and decode, with the property that for all
$m \in \Sigma$ we have $D(E(m)) = m$. In other words, we want $E$ to be injective
and $D$ to be surjective.

We can always create such an encoding: pick a length $n$ such that $|X|^n \geq 
|\Sigma|$; this is always possible as for $|X| \geq 2$ the term on the left
goes to infinity as $n$ increases. Then choose any injective mapping $\Sigma \to
|X|^n$ and define the decoding function to be the inverse on the image of the
encoding and $\bot$ everywhere else.

The above encoding has a fixed length: all codewords are the same length. For
greater efficiency (which we will formalise soon), we can also construct
variabl-length encodings, for example if $\Sigma = \{A, B, C, D, E, F\}$ and
$X = \{0, 1\}$ we could choose the following encoding function:
\[
\begin{array}{l|llllll}
\sigma & A & B & C & D & E & F \\
\hline
E(\sigma) & 0 & 1 & 00 & 01 & 10 & 11
\end{array}
\]

\subsection{Encoding words}

In reality, we do not want to send a single symbol $\sigma \in \Sigma$ but a
word $m \in \Sigma^+$. If we have an encoding scheme $(E, D)$ for single symbols,
we can try and define a word-encoding function
\[
E^+: \Sigma^+ \to X^+, \quad E^+(m_1 m_2 \ldots m_n) := E(m_1) E(m_2) \ldots E(m_n)
\]
that just concatenates the symbol encodings. Is this injective, so that we can
decode again? In general it is not: in the example above, $11$ could decode to
$F$ or to $BB$. This is clearly no good.

In fact, we want an even stronger property than injectivity for word encoding.
Consider the following encoding:

\[
\begin{array}{l|llllll}
\sigma & A & B & C & D & E & F \\
\hline
E(\sigma) & 1 & 10 & 100 & 1000 & 10000 & 100000
\end{array}
\]

The associated encoding $E^+$ is injective: to decode any word you split it just
before each 1 symbol, for example 110100 uniquely decodes as 1--10--100 e.g. ABC.
But if you receive characters one at a time with some delay, e.g. 100\ldots you
do not know if you are in the ``middle of a character'' or not until you see
either the next 1 or you are sure that the transmission is over.

In contrast, consider this encoding:

\[
\begin{array}{l|llllll}
\sigma & A & B & C & D & E & F \\
\hline
E(\sigma) & 1 & 01 & 001 & 0001 & 00001 & 000001
\end{array}
\]

Here, once you have received a 1, you know immediately that a character is
complete, and you can pass it on to the next part of your application to be
processed. An encoding with this property is called \hi{instantaneous}.

What makes an encoding instantaneous? The answer is that there are no two
codewords $c, c'$ in such that $c$ is a prefix of $c'$. In the previous example,
$c =$ 100 was a prefix of e.g. $c' =$ 1000 so after receiving 100 we are not
sure if we have a character yet or not.

\begin{definition}[prefix-free]
An encoding $E: \Sigma \to X^+$ is prefix-free if there are no two 
distinct codewords $c, c'$ in the image of $E$ such that one is a prefix of the 
other.
\end{definition}

It turns out that if we construct a prefix-free encoding $E: \Sigma \to X^+$
then the associated word encoding $E^+: \Sigma^+ \to X^+$ is always injective.

\begin{proposition}
Let $\Sigma$ and $X$ be alphabets.
If $E: \Sigma \to X^+$ is injective and prefix-free then $E^+: \Sigma^+ \to
X^+$ defined as $E^+(m_1 \ldots m_n) = E(m_1) \ldots E(m_n)$ is also injective.
\end{proposition}

We are dealing with two different codes here: $C$ is the image of $E$ and $C'$ 
is the image of $E^+$. We know that $C$ is prefix-free. Since a word in $C'$ is 
a sequence of words in $C$, this means that $C' = C^+$.

Suppose that some word $w \in C^+$ encodes two different messages, e.g. there
are distinct words $m, m' \in \Sigma^+$ with $E^+(m) = E^+(m')$. We are going to
show that this leads to a contradiction.

Assume that $|m| \leq |m'|$ without loss of generality, otherwise swap $m$ and 
$m'$. There are two possible cases. If $m$ is a prefix of $m'$ then we can 
write $m' = ms$ for some $s \in \Sigma^+$, but then we would have $E^+(m') = 
E^+(m)E^+(s) = E^+(m)$ so $E^+(s)$ would have to be the empty word, which is 
impossible. So $m$ cannot be a prefix of $m'$, although the two words may still 
have different lengths. This means that there must be a symbol in $m$ that 
differs from the symbol at the same position in $m'$, so we can write $m = PcD$ 
and $m' = Pc'D'$ with $P, D, D' \in \Sigma^*$ and $c, c' \in \Sigma$ with $c 
\neq c'$. In other words, $m$ and $m'$ share a (possibly empty) prefix $P$, then
comes a position at which $m$ has character $c$ and $m'$ has a different
character $c'$, after this we do not mind what the words do.

We can now write
\[
w = E^+(m) = E^+(P)E(c)E^+(D) = E^+(P)E(c')E^+(D') = E^+(m)
\]
where we have used $E^+(c) = E(c)$ for single symbols $c \in \Sigma$.
We can write $w = E^+(P)s$ for some $s \in X^+$ to get
\[
s = E(c) E^+(D) = E(c') E^+(D') 
\]
We consider three cases:
\begin{itemize}
\item $E(c)$ and $E(c')$ are the same length, call this length $n$. Then we must
have $E(c) = E(c')$ since both of these are the first $n$ characters of $s$.
This is a contradiction to $E$ being injective.
\item $E(c)$ is shorter than $E(c')$. But this means that $E(c)$ is a prefix
of $E(c')$, since both of these are prefixes of $s$. This contradicts
prefix-freeness of $E$.
\item $E(c)$ is longer than $E(c')$. The same argument as above with the roles
of $c, c'$ reversed.
\end{itemize}

This proves the proposition.

Actually, it is also the case that word encoding is instantaneous if and only if
it is based on a prefix-free symbol encoding. So we have several good reasons to
make our encodings prefix-free.

\subsection{Decoding trees}

We can visualise the decoding operation as a state machine. Take the following
encoding for example:

\[
\begin{array}{l|llllll}
\sigma & A & B & C & D \\
\hline
E(\sigma) & 1 & 01 & 000 & 001
\end{array}
\]

\usetikzlibrary{automata}
\begin{center}
\begin{tikzpicture}[initial text={},->,auto,>=triangle 45]
    \node [initial,state] (n) at (0, 0) {};
    \node [state,accepting] (n1) at (2, 0) {A};
    \node [state] (n0) at (2, -1.5) {};
    \node [state,accepting] (n01) at (4, -1.5) {B};
    \node [state] (n00) at (4, -3) {};
    \node [state,accepting] (n000) at (6, -1.5) {C};
    \node [state,accepting] (n001) at (6, -3) {D};
    
    \path (n) edge node {1} (n1)
          (n) edge node {0} (n0)
          (n0) edge node {1} (n01)
          (n0) edge node {0} (n00)
          (n00) edge node {0} (n000)
          (n00) edge node {1} (n001);
\end{tikzpicture}
\end{center}

The rule is, you trace the received word and if you reach an accepting state
then you received a correct codeword and decode to the symbol indicated.

As long as we are not doing error correction, there is no need for two different
words in $X^+$ to decode to the same symbol, so the state machine will not have
any loops and we can leave off all nodes and edges that cannot lead to an
accepting state. This turns the state machine into a tree.
Such a tree --- a binary tree if the
code alphabet is $X = \{0, 1\}$ --- is called a \hi{decoding tree}.

\begin{proposition}
A code is prefix-free if and only if all the accepting states in the decoding
tree are leaves.
\end{proposition}

\subsection{Constructing efficient codes}

If we try and design an efficient prefix-free encoding, we find that the 
prefix-free property restricts the number of codewords that we can use. For
example over $\{0, 1\}$, if we choose to include the word 0 in our code then
we cannot use any other codewords starting with 0, so we have lost roughly half
our potential codewords. Choosing 100 as a codeword would prevent 1 and 10 as
well as anything longer starting 100-- from being in our code.

\begin{diamondsec}
To be precise, if 0 is a codeword then for any $n > 1$ we have excluded half the
potential codewords of length $n$. It would not be correct to say that we have
excluded roughly half of $\{0, 1\}^+$ because that is an infinite set, so
``half of'' is not well-defined.
\end{diamondsec}

This principle leads to the following inequality:

\begin{proposition}[Kraft inequality]
For any uniquely decodable code $C \subseteq X^+$ over an alphabet $X$, we have
\[
\sum_{w \in C} |X|^{-|w|} \leq 1
\]
\end{proposition}

To understand this equality, focus on the case $|X| = 2$ for example $X = \{0, 1\}$.
We imagine assigning each codeword a weight that gets exponentially smaller as
the codewords get longer: a word of length 1 has weight 1/2, a word of length 2
has weight 1/4, a word of length 3 has weight 1/8 and so on. The Kraft inequality
says that the total weight of all words in the code is at most 1.

For example, if we choose the words 0 and 1 then we have already exhausted the
total weight of 1. If we need more codewords, we have to give up a shorter word
to get twice as many words that are one symbol longer, for example if we drop
the word 1 then we can have the code $\{$0, 10, 11$\}$ which has weight 1 again.

\subsection{Example: Morse code}

Morse code is typically introduced with the following encoding table, where a
dash is three times as long as a dot:

\newcommand{\SPACE}{\rule{4pt}{0pt}}
\newcommand{\CSP}{\SPACE\SPACE}
\newcommand{\WSP}{\SPACE\SPACE\SPACE\SPACE\SPACE\SPACE}
\newcommand{\DOT}{\rule{4pt}{4pt}\rule{1pt}{0pt}\SPACE}
\newcommand{\DASH}{\rule{14pt}{4pt}\rule{1pt}{0pt}\SPACE}
\newcommand{\DD}{\rule{15pt}{4pt}\rule{1pt}{0pt}}
\newcommand{\DN}{\rule{4pt}{4pt}\rule{1pt}{0pt}}

\newcommand{\I}[1]{\rule{0pt}{0pt}\rlap{#1}\rule{1em}{0pt}}
\begin{minipage}{0.45\textwidth}
\I{A} \DOT \DASH \\
\I{B} \DASH \DOT \DOT \DOT \\
\I{C} \DASH \DOT \DASH \DOT \\
\I{D} \DASH \DOT \DOT \\
\I{E} \DOT \\
\I{F} \DOT \DOT \DASH \DOT \\
\I{G} \DASH \DASH \DOT \\
\I{H} \DOT \DOT \DOT \DOT \\
\I{I} \DOT \DOT \\
\I{J} \DOT \DASH \DASH \DASH \\
\I{K} \DASH \DOT \DASH \\
\I{L} \DOT \DASH \DOT \DOT \\
\I{M} \DASH \DASH \\
\end{minipage}
\begin{minipage}{0.45\textwidth}
\I{N} \DASH \DOT \\
\I{O} \DASH \DASH \DASH \\
\I{P} \DOT \DASH \DASH \DOT \\
\I{Q} \DASH \DASH \DOT \DASH \\
\I{R} \DOT \DASH \DOT \\
\I{S} \DOT \DOT \DOT \\
\I{T} \DASH \\
\I{U} \DOT \DOT \DASH \\
\I{V} \DOT \DOT \DOT \DASH \\
\I{W} \DOT \DASH \DASH \\
\I{X} \DASH \DOT \DOT \DASH \\
\I{Y} \DASH \DOT \DASH \DASH \\
\I{Z} \DASH \DASH \DOT \DOT \\
\end{minipage}

From this, we could draw the following decoding tree:

\usetikzlibrary{automata}
\begin{center}
\begin{tikzpicture}[
    initial text={},->,auto,state/.style={circle,draw,text depth=0pt,text height=7pt},
    xscale=1,yscale=0.75,
]
    \node [initial,state] (start) at (0, 0) {};
    
    \node [state] (d) at (1, 4.5) {E};
    \node [state] (D) at (1, -4.5) {T};
    
    \node [state] (dd) at (2, 6.5) {I};
    \node [state] (dD) at (2, 2.5) {A};
    \node [state] (Dd) at (2, -2.5) {N};
    \node [state] (DD) at (2, -6.5) {M};
    
    \node [state] (ddd) at (4, 7.5) {S};
    \node [state] (ddD) at (4, 5.5) {U};
    \node [state] (dDd) at (4, 3.5) {R};
    \node [state] (dDD) at (4, 1.5) {W};
    \node [state] (Ddd) at (4, -1.5) {D};
    \node [state] (DdD) at (4, -3.5) {K};
    \node [state] (DDd) at (4, -5.5) {G};
    \node [state] (DDD) at (4, -7.5) {O};

    \node [state] (dddd) at (6, 8) {H};
    \node [state] (dddD) at (6, 7) {V};
    \node [state] (ddDd) at (6, 6) {F};
    %\node [state] (ddDD) at (6, 5) {};
    \node [state] (dDdd) at (6, 4) {L};
    %\node [state] (dDdD) at (6, 3) {};
    \node [state] (dDDd) at (6, 2) {P};
    \node [state] (dDDD) at (6, 1) {J};
    \node [state] (Dddd) at (6, -1) {B};
    \node [state] (DddD) at (6, -2) {X};
    \node [state] (DdDd) at (6, -3) {C};
    \node [state] (DdDD) at (6, -4) {Y};
    \node [state] (DDdd) at (6, -5) {Z};
    \node [state] (DDdD) at (6, -6) {Q};
    %\node [state] (DDDd) at (6, -7) {};
    %\node [state] (DDDD) at (6, -8) {};    
    
    \path (start) edge node {\DOT}  (d)
          (start) edge node {\DD}   (D)
          
          (d) edge node {\DOT}  (dd)
          (d) edge node {\DD}   (dD)
          (D) edge node {\DOT}  (Dd)
          (D) edge node {\DD}   (DD)
          
          (dd) edge node {\DOT}  (ddd)
          (dd) edge node {\DD}   (ddD)
          (dD) edge node {\DOT}  (dDd)
          (dD) edge node {\DD}   (dDD)
          (Dd) edge node {\DOT}  (Ddd)
          (Dd) edge node {\DD}   (DdD)
          (DD) edge node {\DOT}  (DDd)
          (DD) edge node {\DD}   (DDD)

          (ddd) edge node {\DOT}  (dddd)
          (ddd) edge node {\DD}   (dddD)
          (ddD) edge node {\DOT}  (ddDd)
          %(ddD) edge node {\DD}   (ddDD)
          (dDd) edge node {\DOT}  (dDdd)
         % (dDd) edge node {\DD}   (dDdD)
          (dDD) edge node {\DOT}  (dDDd)
          (dDD) edge node {\DD}   (dDDD)
          (Ddd) edge node {\DOT}  (Dddd)
          (Ddd) edge node {\DD}   (DddD)
          (DdD) edge node {\DOT}  (DdDd)
          (DdD) edge node {\DD}   (DdDD)
          (DDd) edge node {\DOT}  (DDdd)
          (DDd) edge node {\DD}   (DDdD)
          %(DDD) edge node {\DOT}  (DDDd)
          %(DDD) edge node {\DD}   (DDDD)
    ;
\end{tikzpicture}
\end{center}

\needspace{2\baselineskip}
While this is adequate for learning to send and receive Morse code, it does not
tell the full story yet. The decoding tree shows that the code in this form is
not prefix free, so if you receive \DASH \DASH then how do you know if it was an
encoding of TT or of M ?

The answer is that Morse code is \emph{not} a code with two symbols, dash and dot.
Instead it uses a concept of time-units:

\begin{itemize}
\item A dot is one time-unit long.
\item A dash is three time-units long.
\item A space between dots and dashes within a symbol is one time-unit long.
\item A space between characters is three time-units long.
\item A space between words is seven time-units long.
\end{itemize}

Once you take timing and character spaces into account, then using one 
bit per time-unit the encoding of A for example is
(\DOT \SPACE \DN \DN \DN \SPACE \SPACE \SPACE) or in binary, 10111000.
This code is prefix-free, as every character ends in --000 for the three-unit 
end-of-character space and no 000 sequences occur within characters.

\subsection{Example: Baudot code}

Baudot code was used in teletypewriter machines, that is electric typewriters
connected to a telegraph network. It uses the following 5-bit encoding:

\begin{minipage}{0.45\textwidth}
\rule{-8pt}{0pt}
\begin{tabular}{lll}
A & -- & 11000 \\
B & ? & 10011 \\
C & : & 01110 \\
D & \emph{ENQ} & 10010 \\

E & 3 & 10000 \\
F & \% & 10110 \\
G & \@ & 01011 \\
H & \pounds & 00101 \\

I & 8 & 01100 \\
J & \emph{BEL} & 11010 \\
K & ( & 11110 \\
L & ) & 10001 \\

M & . & 00111 \\
N & , & 00110 \\
O & 9 & 00011 \\
P & 0 & 01101
\end{tabular}
\end{minipage}
\begin{minipage}{0.45\textwidth}
\begin{tabular}{lll}
Q & 1 & 11101 \\
R & 4 & 01010 \\
S & ' & 10100 \\
T & 5 & 00001 \\

U & 7 & 11100 \\
V & = & 01111 \\
W & 2 & 11001 \\
X & / & 10111 \\

Y & 6 & 10111 \\
Z & + & 10001 \\
\emph{CR} & & 00010 \\
\emph{LF} & & 01000 \\

\multicolumn{2}{l}{\emph{unshift}} & 11111 \\
\multicolumn{2}{l}{\emph{shift}} & 11011 \\
\multicolumn{2}{l}{\emph{space}} & 00100 \\
\multicolumn{2}{l}{\emph{not used}} & 00000 \\
\end{tabular}
\end{minipage}

Baudot code slightly cheats with our definition of a code. It can encode 57 
symbols, for which one would normally need 6 bits per symbol in a fixed-length 
code. Instead, Baudot chooses two codewords 11011 to mean \emph{shift to 
figures} and 11111 to mean \emph{unshift (back to letters)}. To encode the 
message A12, you would send the codeword 11000--11011--11101--11001--11111 (A, 
shift, 1, 2, unshift). The dashes in between are purely to illustrate the 
example: Baudot code, unlike Morse, does not need a pause between characters.
This shift has nothing to do with capital and small letters, which Baudot code
does not distinguish in any case.

From an efficiency point of view, this means that a message which is mostly text
takes around 5 bits/character to encode, whereas an unusual message that
alternates between letters and figures might take 10 bits/character as you need
an (un)shift after every character.

A nice side-effect of this encoding is that you can turn most typing mistakes
into ``ignore me'' sequences. Baudot code was punched on paper tape with a hole
to represent 1 and no hole to represent 0. If a typist made a mistake, she would
move the tape back one position and put holes in all 5 positions, producing
the unshift symbol that has no effect if a shift to numbers is not currently in
effect. Further, since 00000 (no holes at all) is unused, an automatic tape
reader can stop if it ever sees this character.

The special character \emph{ENQ} means \emph{equiry} and is sent to ask the 
machine on the other end of the telegraph wire to identify itself. \emph{BEL} 
causes the other machine to ring a bell to attract the operator's attention. 
\emph{CR} is a \emph{carriage return} that returns the typewriter head to the 
left of the current line and \emph{LF} is a \emph{line feed} that advances the 
paper one line. The combination CR,LF was used to indicate a new line, 
something that some computer manufacturers copied and others did not. It is a 
sad fact of life that internet-connected software e.g. web browsers need to 
watch out for this difference even today.

\subsection{Example: UTF-8}

\hi{ASCII}, the \emph{American Standard Code for Information Interchange}, was
developed in the 1960s. It is an encoding of a 128-character alphabet using 7
bits, as although most computers at the time had an 8-bit ``word size'', the
8th bit was often used as a parity check bit: the forms of storage at the time
(magnetic and paper tapes) were not entirely reliable.

ASCII kept the convention from Baudot of using the all-ones character (111 
1111, decimal 127) as an ``ignore me'' character for use with punched cards. It 
also kept the BEL, CR and LF special characters (but with new encodings) and 
continued to use 000 0000 (now called NUL) as an ``unused / end of 
transmission'' symbol that was later adopted by the C programming language for 
its string handling functions.

Although ASCII had capital and lowercase letters and separate encodings for
numbers and punctuation, abandoning the shift/unshift Baudot characters, with
128 symbols it did not offer ways to encode any accented, umlauted or other
``international'' characters. Later personal computers adopted different
\hi{code pages} which gave country-specific meanings to the 8-bit characters
with the highest bit set, for example under ``code page 863'' used in French
Canada (which of course had a different code page to mainland France), the
symbol \'e was encoded as 11101001. Thus began the age in which a text file
created on a computer in one country would often display in a garbled way on
computers in other countries.

The \hi{Unicode} standard aimed to create one standard to rule them all. Of 
course, this ended in them producing multiple standards. The basic idea behind 
unicode is to give each character a unique number called its \hi{code point} 
and then define a way to encode these code points as bit strings.

In earlier versions, the Unicode committee decided that one would never need 
more than $2^{16} = 65536$ code points, known as the \hi{basic multilingual 
plane} (BMP).
Obviously, one could simply encode each character in the BMP as a two-byte
integer. This is called \hi{UCS-2} (Universal (Coded) Character Set) encoding.

Unfortunately, different machines have a different endianness, so there are two
versions of UCS-2 in use called UCS-2-LE and UCS-2-BE for little and big endian
respectively.

For English text, UCS-2 takes twice as much space as ASCII. This motivated
the Unicode standards body to define an alternative, variable-length encoding
namely \hi{UTF-8} (Universal Text Format). In the meantime, new characters with
code points $2^{16}$ and higher had been defined so these were added to the
encoding:

\begin{tabular}{lll}
Code points & data bits & encoding \\
\midrule
$0$ -- $2^8 - 1$ & \phantom{0}7 & 0xxx xxxx \\
$2^8$ -- $2^{12} - 1$ & 11 & 110x xxxx 10xx xxxx \\
$2^{12}$ -- $2^{17} - 1$ & 16 & 1110 xxxx 10xx xxxx 10xx xxxx \\
$2^{17}$ -- $2^{22} - 1$ & 21 & 1111 0xxx 10xx xxxx 10xx xxxx 10xx xxxx
\end{tabular}

Since the code points for the 128 ASCII characters were chosen to be the same
as their ASCII encodings, the result is that a valid ASCII file is also valid
UTF-8 (as long as it really only uses 7-bit characters and not any of the
international extensions). English text continues to be encodable at one byte
per character.

UTF-8 is prefix-free: the number of leading 1s in a byte indicates whether it
is a single-byte character, the start of a multibyte character (and how long
this character is) or the continuation of a multibyte character. This is also
useful for binary searching and other algorithms in UTF-8 strings: if you read a
byte starting with 10 then you know you are ``in the middle of a character'' and
can scan backwards or forwards to find the next character boundary.

There are two ways in which a sequence of bytes can be invalid UTF-8. The first 
is a byte starting with more than four 1s in a row. The second is an encoding 
of a character with more bytes than necessary, e.g. 1100 0000 1010 0000 where 
the actual data bits are 00000100000, representing the character decimal 32 
(which is a space). This should be encoded in one byte as 00100000, but a 
decoding implementation that just strips the ``padding'' bits and then decodes 
the resulting code point would not notice this. Problems with invalid unicode 
have led to security exploits, for example if a function that is supposed to
strip all spaces, slashes or quote marks just looks for the corresponding 1-byte
encodings then it will miss incorrectly encoded ones.

\ifdefined\booklet
\else
\end{document}
\fi
