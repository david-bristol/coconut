\ifdefined\booklet
\else
\documentclass{scrartcl}

\include{header}

\setuplecture{14}{Hamming distance}

\begin{document}

\titlestuff

\noindent\emph{Learning outcomes:} After this lecture and revision, you should
be able to
\begin{itemize}
\item Explain the main definitions of coding theory.
\item Encode and decode using repetition and checksum codes
      (where a checksum function is provided).
\item Compute the information length, redundancy and information rate of a code
      and compare different codes using these measures.
\item Define and compute Hamming weights and distances over finite fields.
\item State the Singleton bound.
\end{itemize}
\fi

\section{Hamming distance}

Richard Hamming (1915--1998) developed what is nowadays know as a Hamming code,
a more general form of checksum that takes $O(\log(n))$ bits for a $n$-bit
message and that can correct as well as detect $O(\log(n))$ errors. Before we
get to the general Hamming code, here are some of his earlier observations.

\subsection{Hamming distance}

Assume that your message space $\Sigma$ is a finite field with $q$ elements 
(for example $q = 2$, e.g. you are working with bits, but the theory also works 
for larger $q$).

\begin{definition}[Hamming weight and distance]
The \hi{Hamming weight} $w(x)$ of a word $x \in (\mathbb F_q)^k$ is the number 
of nonzero symbols $x_i \neq 0$ in $x$.

The \hi{Hamming distance} $d(x, y)$ between two words $x, y \in (\mathbb 
F_q)^k$ is $w(x - y)$ where the subtraction is done symbol-wise (e.g. viewing 
the words as vectors).
\end{definition} 

In fact, the Hamming distance can be defined for any alphabet $\Sigma$, even if
it is not a finite field: $d(x, y)$ is the number of edits you need to make to
$x$ to get $y$, where an edit means overwriting a single symbol with another.
The Hamming distance is what mathematicians call a \hi{metric}:

\begin{itemize}
\item For any $x$, we have $d(x, x) = 0$ and for any $x, y$ we have $d(x, y) \geq 0$.
\item For any $x, y$ we have $d(x, y) = d(y, x)$.
\item We have the \hi{triangular inequality}: for any $x, y, z$ we have
\[ d(x, y) + d(y, z) \geq d(x, z) \]
\end{itemize}

The reason that this definition is important in coding theory is that if you
send a codeword $c$ and there are $u$ errors in transmission, the received word
$r$ will have $d(c, r) = u$.

\subsection{Minimum distances}

If a code $C$ has two codewords with Hamming distance $1$, then a single error
could turn one codeword into another. So to detect or correct as many errors as
possible, you want your codewords to be as far apart as possible according to
the Hamming distance. This leads to the following definition:

\begin{definition}[(minimum) distance of a block code]
The (minimum) distance of a block code $C \subseteq \Sigma^n$ is
\[
    d(C) := \min_{\substack{c, c' \in C \\ c \neq c'}} d(c, c')
\]
\end{definition}

For example, in a code with minimum distance 3, the Hamming distance between 
any two codewords is at least 3. If you receive a word $r$ out of a channel 
that is not a codeword, but has Hamming distance 1 to some codeword $c$, then 
it will have Hamming distance at least 2 to all other codewords; in this case 
if you assume that the most likely explanation is that a single error has
occurred, you can decode $r$ as $c$.

This observation leads to the following important fact, showing that we should
aim for codes with a large minimum distance if we are trying to avoid errors:

\begin{theorem}
\label{prop:c1-mindist}
For a code to correct up to $t$ errors it needs a minimum distance of at least
$d \geq 2t + 1$. To detect $t$ errors, it is enough to have $d \geq t + 1$.
\end{theorem}

Let's prove this. Suppose a code has minimum distance $d \leq t$. Then there 
must be two codewords $c, c'$ with $d(c, c') \leq t$. Therefore, if you send $c$
there is a way for at most $t$ errors to occur resulting in someone receiving
$c'$. Since $c'$ is also a codeword, the wrong message will be decoded.

Conversely, if a code has minimum distance $t+1$ then you can detect $t$ errors
with the strict decoding function (that returns $\bot$ for anything that is not
a codeword). If you send a codeword $c$, then all words that can occur due to
at most $t$ errors are in the set $B_t(c) = \{w \mid d(c, w) \leq t\}$ which is
also known as the \hi{ball of radius $t$ with centre $c$}. But this ball cannot
contain any other codeword $c'$ or we would have $d(c, c') \leq t$ which would
mean $d(C) \leq t$. This proves the bound for detecting errors.

To prove the bound for correcting errors, the idea is that you can correct a
received word $r$ to $c$ if $r$ is closer to $c$ than to any other codeword $c'$.
Suppose you have a code with minimum distance $2t + 1$.
The following decoding function corrects up to $t$ errors:
\[
D(r) = \left\{ \begin{array}{ll}
m, & \text{ there is some } c = E(m) \text{ with } r \in B_t(c) \\
\bot, & \text{otherwise}
\end{array}\right.
\]

We need to check that this is in fact a function, e.g. that $D(r)$ cannot be 
defined more than once for a single $r$. Suppose that there is some $c$ with $r 
\in B_t(c)$. We declared that encoding functions must be injective, so there 
cannot be two distinct messages $m, m'$ with $c = E(m) = E(m')$. It remains to
show that no $r$ can be in two balls $B_t(c)$ and $B_t(c')$ for $c \neq c'$.
If this were the case, then $d(c, r) \leq t$ and $d(c', r) \leq t$ too so by
the triangular inequality we get $d(c, c') \leq 2t$ which is a contradiction.

Therefore, the ``ball'' decoding function is really a function and it correctly
decodes codewords with up to $t$ errors.

It remains to show that every code with minimum distance $2t$ cannot reliably 
correct $t$ errors, whatever the decoding function. Pick any such code and pick
two codewords $c, c'$ with $d(c, c') \leq 2t$, which must exist otherwise the
code would have a higher minimum distance. Let $r$ be as follows: start with
$c$ and change the first $t$ positions of $r$ at which $c$ and $c'$ differ to be
equal to the corresponding symbols of $c'$. If there are fewer than $t$ such
positions, then $r = c'$. We now have $d(c, r) \leq t$ since we made at most $t$
changes to $c$ and $d(c', r) \leq t$ as we started out with a word $c$ that had
at most $2t$ differences to $c'$ and we removed $t$ of those differences.
However you decode $r$, you now have a problem. Suppose that $c = E(m)$ and
$c' = E(m')$:
\begin{itemize}
\item If $D(r) = m$ then you end up decoding incorrectly if $m'$ was sent,
resulting in codeword $c'$ and up to $t$ mistakes happened producing $r$.
\item If $D(r) = m'$ then you end up decoding incorrectly if $m$ was sent
resulting in codeword $c$ and up to $t$ mistakes happened in such a way to
produce $r$.
\item If $D(r)$ is something else, then both of the above cases apply.
\item If $D(r)$ is $\bot$, then you do not decode incorrectly but you still have
to resend the message if $r$ results due to at most $t$ errors on $c$ or $c'$.
\end{itemize}

This completes the proof. The decoding function that we presented here is an 
example of a general approach called \hi{maximum-likelihood decoding}, where a 
received word $r$ is decoded like the codeword $c$ if there is a unique 
codeword $c$ that is closest to $r$ under the Hamming distance. If there is no 
such codeword, then $r$ decodes to $\bot$. The term \emph{likelihood} is 
because if we assume that each symbol in a codeword gets transmitted 
incorrectly with a fixed probability then given that you received $r$, the 
closest $c$ is the most likely word to have been sent.

\subsection{The Singleton bound}

How many symbols (resp. bits, if $|\Sigma| = 2$) do we need to add to get a code
with minimal distance $d$? A simple observation:

\begin{theorem}[Singleton bound]
A code $C \subseteq \Sigma^n$ of minimimal distance $d$ has at most 
$|\Sigma|^{n-d+1}$ codewords.

Put another way, if $E: \Sigma^k \to \Sigma^n$ then $d \leq n - k + 1$.
\end{theorem}

To prove this, note that if you take a code of minimal distance $d$ and remove
the last $d-1$ symbols from each codeword, you still get a code of the same size;
it cannot be the case that any two words $c, c'$ differ only in the last $d-1$
symbols since that would mean $d(c, c') \leq d-1$.

But after removing $d - 1$ symbols, there are only $n-d+1$ left, all of which
are elements of $\Sigma$. This proves the bound.

As a consequence, if we want to construct a code with distance $d$ for messages
of length $k$, then since we need $|C| = |\Sigma|^k$ just to be able to
encode every message, we get $k \leq n - d + 1$ resp. $d \leq n - k + 1$.

Unfortunately, this bound is not tight, and especially in the case $\Sigma = 
\{0, 1\}$ we are often worse off. Suppose you have $k = 2, n = 4, d = 3$ e.g. 
you want a set of four $4$-bit codewords to let you transmit $2$-bit messages 
and correct any one error.

Suppose you encode the message 00 as 0000 (any other encoding would lead to the
same problem). Then to correct a 1-bit error, all the words of Hamming weight 1
must decode to 00, that is the set $\{0000, 0001, 0010, 0100, 1000\}$ must all
decode to 00. We need 4 ``neighbour'' words to ``protect'' the original word.
However we encode 01, we need a word of at least Hamming weight 3 to maintain the
distance. That leaves $\{1110, 1101, 1011, 0111, 1111\}$ as candidates, but
whichever word we pick we need 4 more ``neighbours'' for the 4 possible 1-bit
errors. Unfortunately, we have already used up 10 of 16 possible words, and we
have only managed to encode two of four original words so far.

For particular $(n, k, d)$ combinations, we can do better than this, but to
understand how we need to take a step back and look at some more Linear Algebra
in the next lecture.

\subsection{More Terminology}

A \hi{code} $C$ is formally a subset of the channel space $\mathcal X$ (in ToC
terminology, a \emph{language}). An element $c \in C$ is called a \hi{codeword}.
The encoding function has signature $\mathcal M \to C$.

A \hi{block code} is a code where all codewords have the same length.
Over an alphabet $\Sigma$, a $(n, M)$-code is a block code $C$ that contains $M$
codewords each of which is $n$ symbols long. For such a code we define
\begin{itemize}
\item The \hi{information length} of the code is $\log(M)$.
\item The \hi{redundancy} of the code is $n - \log(M)$.
\item The \hi{information rate} of the code is $\log(M) / n$.
\end{itemize}
All the logarithms are to the basis $b = |\Sigma|$, the number of symbols in the
alphabet (although logarithms are easy to convert across different bases).

This lets us compare the codes as follows, for $k$-symbol messages (so the
information length will always be $k$):

\begin{tabular}{lllllll}
code & detect & correct & $n$ & redundancy & info. rate \\
\hline
none & $0$ & $0$ & $k$ & $0$ & $1$ \\
$2$-times rep. & $1$ & $0$ & $2 \times k$ & $k$ & $1/2$ \\
$s$-times rep. & $\lfloor s/2 \rfloor$ & $\lfloor (s-1)/2 \rfloor$ &
  $s \times k$ & $(s - 1) \times k$ & $1/s$ \\
checksum & $1$ & $0$ & $k + 1$ & $1$ & $k / (k + 1)$
\end{tabular}

For codes with the same detection/correction properties, a lower redundancy
and so a higher information rate is better.


%Based on this idea, let's construct a checksum code for $\mathcal M = \mathbb B^3$
%that can correct any one error. This means we need $d \geq 2$ and so we need
%$n \geq 5$.

%Let's try and construct a checksum code with a 2-bit checksum, which would
%achieve $n = 5$. Suppose we choose to encode 000 as 00000, which we'll write
%000--00 to indicate that the last two digits are check digits. The argument
%would work equally well if we chose any other checksum for 000.

%The encoding for 001 must be 001--11, as any other encoding would have at most
%Hamming weight 2, and so Hamming distance 2 to 000-00.

%But we are now stuck with 010, as 010--00 has distance 1 to 000--00, 010--11 
%has distance 2 to 001--11 and 010--01 and 010--10 both have Hamming weight 2
%so distance 2 to 000--00.

%XXX What went wrong? We tried to make a code based on the assumption that the
%first bits of the codeword would be the message itself, with just some extra
%bits stuck on the end. It will turn out that this is not the most general way
%to construct a code.

%~ \begin{tabular}{ll}
%~ message & checksum \\
%~ \hline
%~ 000 & 00 \\
%~ 001 & 11 \\
%~ 010 &    \\
%~ 011 &    \\
%~ 100 &    \\
%~ 101 &    \\
%~ 110 &    \\
%~ 111 &   
%~ \end{tabular}

\subsection{Optimal and perfect codes}

How many codewords can we have in a code of length $n$ over an alphabet, if we
require a minimum distance $d$? Let's give this quantity a name.

\begin{definition}
For $q$ a prime power, $A_q(n, d)$ is the maximal number of words in any code
of length $n$ over an alphabet of size $q$ with minimal distance $d$.

Any code meeting this bound is called an \hi{optimal code}.
\end{definition}

A code with $M$ words of size $n$ and distance $d$ is also called an $(n, M, d)_q$
code. In general, there is no way to compute $A_q(n, d)$ but there are various
bounds from above and below. We have already seen the Singleton bound; here is
another.

\begin{proposition}
For $d \leq n$ we have $A_q(n, d) \leq q^n$, $A_q(n, 1) = q^n$ and finally $A_q(n, n)
= q$.
\end{proposition}

The first claim follows because such a code is a subset of $\Sigma^n$ which has
$q^n$ elements. The second claim says that for $d = 1$ the bound is exact,
indeed the code where every word of $\Sigma^n$ is a codeword has $d = 1$.
The third claim looks at codes where the minimal distance is as long as the
codeword itself, so no two codewords can share a symbol in the same position.
The $n$-fold repetition code is such a code with $q = |\Sigma|$ codewords; any
code with more words than this would have two words with the same symbol in the
same position.

\needspace{2\baselineskip}
Hamming himself proved a tighter bound.

\begin{theorem}[Hamming bound]
Suppose that $\Sigma = \mathbb Z_q$.
Let $V_q(n, s)$ be the number of points (volume) in the ball
$B_s(\vec 0) \subseteq \Sigma^n$.
For every $q > 1$ and $d \leq n$ and $t = \lfloor (d-1)/2 \rfloor$ we have
\[
    A_q(n, d) \leq \frac{q^n}{V_q(n, t)}
\]
\end{theorem}

The intuition behind this theorem is what we showed as an example above: to get
a code with minimum distance $d$, each codeword must have a ball of radius $t$
around it that contains no further codewords. The other ingredient in this
theorem is that the volume of a ball does not depend on its centre, which is why
we made the arbitrary choice of defining $v_q(n, s)$ as the volume of the ball
centred at the all-zero vector.

A code that achieves Hamming's bound is called a \hi{perfect code} and Hamming
himself designed a set of codes that achieve this bound. A perfect code is
automatically optimal (but not the other way round).

Finally, we can bound $A_q(n, d)$ from below:

\begin{theorem}[Gilbert-Varshamov bound]
For $q \geq 1$ and $d \leq n$ we have
\[
    A_q(n, d) \geq \frac{q^n}{V_q(n, d-1)}
\]
\end{theorem}

The resasoning here is that in an optimal code, every word in $\Sigma^n$ must
be contained in some ball of radius $d-1$ around a codeword: if there were any
word outside all of these balls, then it could be added to the code to make a
larger code while keeping minimal distance $d$ to all existing codewords.

\subsection{Exercises}

\begin{exercise}
\emph{Repetition codes:}

$(\star)$ If you do not care about correcting errors, how many errors can
you reliably detect with a 4-times repetition code? What about 5-times? Find
examples of the worst case for errors.

$(\star\star)$ If you do care about correcting errors, how many errors can you
reliably correct with a 4-times repetition code? What about 5-times? Define the
decoding function in this case. How many errors can it detect now? 
\end{exercise}

\begin{exercise}
\emph{Checksums:}

One possible checksum for integer values is to repeatedly sum all the digits 
until you are left with a single digit. This has the nice property that the 
checksum of a sum or product of integers is the sum or product of their 
checksums, so it can be used to cross-check longhand addition and multiplication.

$(\star\star)$ Convince yourself that this checksum can never become zero for
a nonzero integer.

$(\star\star)$ How many digit errors can this checksum code correct?

$({\star}{\star}{\star})$ Show that the checksum of an integer $n$ in this
case is $n \bmod{9}$ with the value $0$ replaced by $9$, and convince yourself
that this explains why the checksum commutes with addition and multiplication.
\end{exercise}

\needspace{8\baselineskip}
\begin{exercise}
\emph{Hamming distance:}

We take $\Sigma = \mathbb F_3$ and the following code $C \subseteq \Sigma^4$:
\[
\begin{array}{l|lllllllll}
m & 00 & 01 & 02 & 10 & 11 & 12 & 20 & 21 & 22 \\
\hline
E(m) & 0000 & 0112 & 0221 & 1021 & 1100 & 1212 & 2012 & 2121 & 2200
\end{array}
\]

$(\star)$ Find two codewords with Hamming distance 3 and two codewords with
Hamming distance 4.

$(\star\star)$ What is the minimum distance of this code? How many errors can it
therefore correct?

$(\star)$ Write down the elements of the balls $B_0(1100)$ and $B_1(2012)$.

$(\star\star)$ How many elements does the ball $B_2(2200)$ have?

$(\star\star)$ In the proof of Theorem \ref{prop:c1-mindist}, find suitable 
codewords $c, c'$ in this code $C$ and a counter-example $r$ as described in 
the last part and convince yourself of the points made in the bulleted list.

\end{exercise}

\begin{exercise}
$(\star\star)$ Give an explicit formula for $V_q(n, s)$, the number of points
in $(\mathbb Z_q)^n$ that lie in the ball $B_s(\vec 0)$.
(This exercise requires some combinatorics, which we did not cover explicitly in
this unit.)
\end{exercise}

\ifdefined\booklet
\else
\end{document}
\fi
