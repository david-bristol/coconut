\ifdefined\booklet
\else
\documentclass{scrartcl}

\include{header}

\setuplecture{99}{Entropy}

\begin{document}

\titlestuff

\noindent\emph{Learning outcomes:} After this lecture and revision, you should
be able to
\begin{itemize}
\item XXX
\end{itemize}
\fi

\section{Entropy}

I will pick an integer between 0 and 255 (inclusive) at random. Your task is to
guess this number; you may ask me questions about the number that I can answer
with true or false.

\begin{itemize}
\item Which of the following pieces of information is more useful to determine
the number? (a) The number is less or equal to 1, (b) the number is less or
equal to 127, (c) the number is less or equal to 255.
\item Which of the following questions would you ask first, if you want to win
the game with as few questions as possible: (a) is the number less or equal to
1, (b) is the number less or equal to 127, (c) is the number even, (d) is the
number an integer.
\item How many questions do you need in the worst case?
\end{itemize}

An answer is more useful, the lower its probability (bearing in mind that the
original number was drawn uniformly at random). The event ``number less or equal
to 1'' has probability 1/128 so knowing that this event gives you a lot of
information about the number, whereas ``the number is not less or equal to 1''
has probability 127/128 and so tells you almost nothing.

Even though the answer to each true/false question is one bit, not every answer
carries one bit of information. For example, an answer that you know already
(e.g. because you have asked this question already, or it is always true such
as ``the number is an integer'') gives you no new information about the number.

Which questions should you ask? If you ask whether the number is less or equal
to 1, then if the answer is true you get a lot of information --- but this
happens only 1/128 of the time. Most of the time, you get the answer false which
is much less useful. If you ask whether the number is less or equal to 127, or
whether the number is even, then whatever answer you get, the probability of the
event you have described (or its complement) will be 1/2.

\subsection{Information and entropy}

Information theory was invented by Claude Shannon in his 1948 paper
\emph{A Mathematical Theory of Communication}. We will follow his ideas but use
more modern notation in some places. In all cases, we assume some probability
space $(\Omega, P)$ is given.

\begin{definition}[information in an event]
\rule{0pt}{0pt}\\
The (Shannon) information conveyed when an event $E$ occurs is $\log_2 \frac{1}{P(E)}$.
\end{definition}

The idea is that a lower-probability event conveys more information than a
higher-probability one, so taking the inverse of a probability makes sense;
since probabilites range from 0 to 1 their inverses range from 1 to infinity
and taking the logarithm moves this range from 0 to infinity. The logarithm has
other useful properties that we will come to later.

The choice of basis 2 for the logarithm is somewhat arbitrary but turns out to 
be useful. If you have a calculator that can only take logarithms to basis 10,
you can convert such a logarithm to basis 2 by multiplying with 3.322, or more
precisely $1/\log_{10}(2)$.
We can see that an event with probability 1 conveys 0 information 
(whatever basis you choose) and an event with probability $2^{-n}$ conveys $n$ 
bits of information. In the example earlier, a number between 0 and 255 has 8 
bits. Learning that the number is even, or less than 128 fixes 1 bit of the 
number and indeed an event of probability 1/2 conveys 1 bit of information. 
Learning that the number is less or equal to 1 fixes the top 7 bits of the 
number and indeed, $\log_2 1/(1/128) = 7$.

An event of probability 0 would convey an infinite amount of information, but
such events do not happen in discrete probability theory so we can ignore this
case.

If $X$ is a random variable then we can compute the expected value of the
information conveyed by $X$.

\begin{definition}[entropy]
The entropy $H(X)$ of a random variable $X$ with range $\mathcal X$ is the quantity
\[
H(X) \ = \ \sum_{x \in \mathcal X} p_X(x) \log_2 \frac{1}{p_X(x)} \ = \ {} 
- \sum_{x \in \mathcal X} p_X(x) \log_2(p_X(x))
\]
\end{definition}

The version with the minus is useful for computing entropies as it avoids
division, but one needs to remember that the logarithm of a number less than 1
is itself negative so the overall entropy will always be positive.

\textbf{Two Examples.}
If we pick an integer between 0 and 255 inclusive uniformly at random, then we
can compute the entropy of the random variable $X$ representing this integer as
follows: there are $2^8$ possible cases each of which occurs with probability
$2^{-8}$ so $H(X) = 2^8/2^8 \log_2(1/2^{-8}) = 8$, and indeed such a number can
be represented by exactly 8 bits.

Suppose we pick an integer between 0 and 255 uniformly at random subject to the
condition that the parity is even, that is the sum of all bits is 0 mod 2. In
this case, the 128 8-bit integers with odd parity occur with probability 0 and
the 128 8-bit integers with even parity occur with probability $2^{-7}$ each, so
$H(X) = 7$ in this case. Despite the number itself having 8 bits, there are only
7 bits of uncertainty as if you know 7 of the bits, you can compute the last
one. Indeed the way you would sample such a number is you would pick 7 bits at
random and then compute the last bit as the parity checksum of the previous ones.

\begin{proposition}
Facts about entropy:
\begin{itemize}
\item The entropy of a random variable $X$ is always greater or equal to 0.
\item For a random variable $X$ with finite range $\mathcal X$, the entropy is
bounded by $H(X) \leq \log_2 |\mathcal X|$ and this bound is tight if and only
if $X$ has the uniform distribution, e.g. each $x \in \mathcal X$ has propability
$p_X(x) = 1/|\mathcal X|$.
\item For two independent random variables $X, Y$, if $Z = (X, Y)$ is the random
variable that gives you both the values of $X$ and $Y$ then we have the sum
formula $H(Z) = H(X) + H(Y)$.
\end{itemize}
\end{proposition}

We will not prove this formally, but we will give some more examples. For a 
random variable $X$ with two possible outcomes $\{0, 1\}$ where the outcome $0$ 
has probability $p$, the entropy is $H(X) = -p \log_2(p) - (1-p) 
\log_2(1-p)$. This function is important enough to get a name: it is called the
\hi{binary entropy function} $h_2(p)$. Its graph is as follows:

\begin{center}
\begin{tikzpicture}[scale=4]
\draw (0, 0) -- (1, 0);
\draw (0, 0) -- (0, 1);
\node [anchor=west] at (1.1, 0) {$p$};
\node [anchor=south] at (0, 1.1) {$H(X)$};

\node at (0, -0.1) {$0$};
\node at (1, -0.1) {$1$};
\node at (-0.1, 0) {$0$};
\node at (-0.1, 1) {$1$};

\draw [variable=\x,domain=0.001:0.999,smooth] 
 plot ({\x}, {-(\x * log2(\x) + (1-\x) * log2(1-\x))});
\end{tikzpicture}
\end{center}

The maximum is at $p = 1/2$ where $H(X) = 1$, e.g. a binary random variable that
is true exactly half the time (based on what you know already) always gives you
one bit of new information. This is why you should ask question such as ``is the
number less or equal to 127''.

\subsection{The meaning of the logarithm}

Shannon shows that (apart from a constant) the entropy function that he defined
is the only one that satisfies certain properties that a measure of information
should intuitively have.

First, consider an experiment in which we pick a uniformly random 8-bit number.
Then we flip a coin: if it lands on heads, we reveal the first two bits of the
number, if it lands on tails then we reveal the last three bits.
If $X$ is a random variable for this process, then we should have
$H(X) = 2.5$ since that is how many bits we
reveal on avarage. If we set up random variables as follows:

\begin{itemize}
\item $A_1$ is a random variable for the first two bits of the number.
\item $A_2$ is a random variable for the last three bits of the number.
\item $X$ is a random variable for the process described above.
\end{itemize}

Then it is reasonable to think that we can compute
$H(X) = 1/2 \times H(A_1) + 1/2 \times H(A_2)$
since half the time, we reveal whatever information we get from $A_1$ and the
other half the time, we reveal what we get from $A_2$. Indeed, this quantity
turns out to be $2.5$.
More generally, we would like the following proposition.

\begin{proposition}
For any two stage process where we first sample according to a distribution $S$
with range $\{1, \ldots, n\}$ that returns an integer $i$ with probability
$p_i$, then sample from a distribution $A_i$ and return that value, if the
ranges of the $A_i$ are disjoint then for the random variable $X$ representing
the whole process we have
\[
    H(X) = \sum_{i=1}^n p_i \times H(A_i)
\]
\end{proposition}

The only function $H$ (up to a constant) that has this property as well as some
other desirable ones like that the entropy of picking a $n$-bit random number
should be $n$ bits is exactly the one that Shannon defined.

The condition that the ranges of the $A_i$ are disjoint is important. Suppose
that we have the following experiment: for the initial distribution $S$, flip a
fair coin. If the coin lands on heads, sample from a distribution $A_1$ on
$\{0, 1\}$ that is 1 with probability 1/2; if the coin lands on tails then
sample from $A_2$ that returns a single bit which is 1 with probability 3/4.
If we try and compute $1/2 \times H(A_1) + 1/2 \times H(A_2) = 1/2 \times 1
+ 1/2 \times h_2(1/4) \approx 0.90564$ we get a different result to if we
compute the entropy of the whole experiment directly: the probability that the
experiment returns 1 is $1/2 \times 1/2 + 1/2 \times 3/4 = 5/8$ and
$h_2(5/8) \approx 0.95443$.

This definition has a useful side-effect.
Suppose for an unknown, uniformly random 8-bit number we defined the random
variables $X$ to be the first bit of the number and $Y$ to be the last bit of
the number. Then the random variable $Z = (X, Y)$ is a random variable that
gives you two bits, namely the first and last ones of the unknown number.
Since $X$ and $Y$ are independent, for any $x, y$ we have
$P(X = x \wedge Y = y) = P(X = x) \times P(Y = y)$ and the logarithm turns
multiplication into addition, so $H(Z) = H(X) + H(Y) = 2$ as we would expect
since we are revealing two bits.
These ideas explain why Shannon defined entropy with a logarithm function.

\subsection{Conditional entropy and mutual information}

If we know nothing about a uniformly random 8-bit number then the entropy in 
the lowest 2 bits of this number (resp. the random variable $X$ that returns 
these 2 bits) is exactly 2 bits. If we learn that the number is even, then the
entropy in $X$ should intuitively drop to one bit. To formalise this we need a
notion of conditional entropy, and there are two such notions: we can condition
on an event or on a random variable. Recall that if $Y$ is a random variable
then $Y = y$ (random variable $Y$ takes value $y$) is an event.

\begin{definition}[Conditional entropy]
The conditional entropy of a random variable $X$ with range $\mathcal X$ 
conditioned on the event $Y = y$ is
\[
H(X \mid Y = y) = - \sum_{x \in \mathcal X} p_{X|Y}(x, y) \log_2(p_{X|Y}(x, y))
\]
The conditional entropy of a random variable $X$ with range $\mathcal X$
conditioned on the random variable $Y$ with range $\mathcal Y$ is
\[
H(X \mid Y) = \sum_{y \in \mathcal Y} p_Y(y) H(X|Y=y)
\]
\end{definition}

It is also common to write $H(X, Y)$ for the entropy in the pair of random
variables $X, Y$ (which we did above by defining $Z = (X, Y)$). This notation
gets you the formulas
\[
H(X, Y) = H(X) + H(Y \mid X) = H(Y) + H(X \mid Y)
\]
which are of course symmetric in $X$ and $Y$. This gives us another way to
compute conditional entropies, and since we already know that if $X, Y$ are
independent then $H(X, Y) = H(X) + H(Y)$ we can see that for indepdent $X, Y$
we have $H(X \mid Y) = H(X)$ e.g. conditioning on something indepdendent of $X$
does not change the entropy in $X$.

One should interpret $H(X \mid Y)$ as the amount of entropy in $X$ that is
independent of $Y$. For example, if $X$ represents the lower 3 bits of a
number (chosen uniformly at random) and $Y$ is the lowest bit on its own, then
$H(X) = 3$ and $H(X \mid Y) = 2$ since 2 of the bits have nothing to do with $Y$.

\textbf{Example.}
For another example, suppose that $X$ is a uniformly random two-bit sequence and
$Y$ is the XOR of the two bits. That is, the possible outcomes are:

\begin{tabular}{lll}
prob. & $X$ & $Y$\\
\midrule
1/4 & 00 & 0 \\
1/4 & 01 & 1 \\
1/4 & 10 & 1 \\
1/4 & 11 & 0 
\end{tabular}

The conditional probability $p_{X|Y}(x, y)$ is 1/2 in each of the four possible
cases and 0 for all other possibilities. This means that for all $y$, the
conditional entropy $H(X \mid Y = y)$ is $- 2 \times 1/2 \times \log_2(1/2) = 1$,
e.g. if you know that $Y = 0$ then there is only one bit of uncertainty remaining
in $X$, even if you do not know either of the bits directly; the same for $Y = 1$.
Finally, the conditional entropy $H(X \mid Y)$ calculates to $1$ as well, which
tells you that revealing the value of $Y$ will on average leave one bit of
entropy in $X$.

And finally, the mutual information in two random variables.

\begin{definition}[mutual information]
The mutual information between two random variables $X, Y$ with ranges
$\mathcal X$ and $\mathcal Y$ respectively is
\[
I(X, Y) = \sum_{x \in \mathcal X} \sum_{y \in \mathcal Y}
          p_{XY}(x, y) \log_2 \frac{p_{XY}(x,y)}{p_X(x) \times p_Y(y)}
\]
\end{definition}

To compute mutual information it is sometimes quicker to use the following formulas:
\[
I(X, Y) = H(X) - H(X \mid Y) = H(Y) - H(Y \mid X)
\]
We can interpret the formula as follows. The mutual information between $X$ and
$Y$ is the entropy of $X$ minus the entropy of $X$ given $Y$, e.g. all the
information in $X$ minus the information in $X$ that is independent of $Y$.

\subsection{Example}

A standard UK pack of cards (with no jokers) has 52 cards in 4 suits and 13
numbers:
$\{$ $\heartsuit $ hearts, $\diamondsuit$ diamonds, $\clubsuit$ clubs,
$\spadesuit$ spades $\} \times \{$ A (ace), 2, 3, 4, 5, 6, 7, 8, 9, 10, J (jack),
Q (queen), K (king) $\}$.

\begin{itemize}
\item What is the entropy of the random variable $C$ that represents picking a
particular card at random?

\item Someone picks a card at random.
What is the entropy of the random variable $X_1$ which is 1 if the card is an 8
and 0 otherwise?

\item Let $Y_1$ be a random variable giving you the suit of the chosen card.
What is $H(Y_1)$ and $H(X_1 \mid Y_1)$?

\item Suppose that you pick 5 cards and get $\heartsuit$ 6, $\heartsuit$ 8,
$\spadesuit$ 8, $\clubsuit$ 3, $\clubsuit$ K.
Someone else picks a 6th card. What is the entropy of $X_2$ which is 1 if the
6th card is an $8$, otherwise 0?

\item If $Y_2$ is a random variable for the suit of the 6th card, what is
$H(X_2 \mid Y_2)$?
\end{itemize}

For the first question, note that we could represent 4 suits with 2 bits and
13 numbers in 4 bits, so we can encode a particular card in 6 bits, but this is
being generous as not all 6-bit numbers encode valid cards. However 5 bits will
not be enough as $2^5 = 32$ which is less than $52$. This lets us guesstimate
$5 < H(C) < 6$. Using the formula for a uniform distribution,
is $H(C) = \log_2(52) \approx 5.7004397$.

The event $X_1 = 1$ (it's an 8) has probability $1/13$ so the entropy must be 
$H(X) = - 1/13 \log_2(1/13) - 12/13 \log_2(12/13) \approx 0.39124$. Knowing 
whether a number is an 8 or not can \emph{on average} give us at most one bit 
of information, but in fact it will give us less since fewer cards are 8 than 
not-8.

Note that the event $X_1 = 1$ conveys $\log_2(13) \approx 3.7004397$ bits
of information, so $H(C \mid X_1 = 1) = 2$ which means that if you know the
number of a card is 8 then there are two bits of entropy remaining, namely the
two bits that encode the suit.

Since there are four suits and all are equally likely, $H(Y_1) = 2$. We can say
that $H(X_1 \mid Y_1) = H(X_1) \approx 0.39124$ since suit and number are
independent in a uniform draw.

For the remaining questions we actually have to do the calculations. The five
cards we picked leave 11 hearts, 13 diamonds, 12 spades and 11 clubs in the pack
for a total of 47 cards. The joint probability distributions of $X_2$ and $Y_2$ are:

\begin{center}
\begin{tabular}{l|llll}
$X_2 \wedge Y_2$ & $\heartsuit$ & $\diamondsuit$ & $\spadesuit$ & $\clubsuit$ \\
\hline
1 & 0 & \phantom{0}1/47 & 0 & \phantom{0}1/47 \\
0 & 11/47 & 12/47 & 12/47 & 10/47
\end{tabular}
\end{center}

For example, since there are 13 diamonds still in the pack of which one is an 8,
the probability of $X_2 = 1$ conditioned on $Y_2 = \diamondsuit$ must be $1/13$.
Or we can just use the formula 
$P(X_2 = x \mid Y_2 = y) = P(X_2 = x \wedge Y_2 = y) / P(Y_2 = y)$ to get
$(1/47)/(1/47 + 12/47) = 1/13$.

The entropy $H(X_2)$ is $-(2/47 \log_2(2/47) + 45/47 \log_2(45/47)) 
\approx 0.2539$ bits. $H(X_2)$ is lower than $H(X_1)$ because it is less likely
to draw an 8 from the new pack with two of the 8s removed than from the original
pack.

On the other hand, since two of the 8s are gone, $X_2$ and $Y_2$ are no longer
independent. In fact, there is a 23/47 chance that $Y_2$ will be one of the two
sets where the 8 is gone, in which case you can infer $X_2 = 0$. But let's do
the calculation. First, the conditional probabilities:

\begin{center}
\begin{tabular}{l|llll}
$P_{X_2 \mid Y_2}$ & $\heartsuit$ & $\diamondsuit$ & $\spadesuit$ & $\clubsuit$ \\
\hline
1 & 0 & \phantom{0}1/13 & 0 & \phantom{0}1/11 \\
0 & 1 & 12/13 & 1 & 10/11
\end{tabular}
\end{center}

From this we get the following intermediate values (ignoring the logarithms of
0 because we won't use them in the calculation):

\begin{center}
\begin{tabular}{lllll}
$x$ & $y$ & $p_{X_2|Y_2}(x,y)$ & $\log_2(p_{X_2|Y_2}(x,y))$ & $p_{Y_2}(y)$ \\
\midrule
0 & $\heartsuit$   & 1     & \phantom{-}0 & 11/47 \\
0 & $\diamondsuit$ & 12/13 & -0.11548     & 13/47 \\
0 & $\spadesuit$   & 1     & \phantom{-}0 & 12/47 \\
0 & $\clubsuit$    & 10/11 & -0.13750     & 11/47 \\
1 & $\heartsuit$   & 0     &              & 11/47 \\
1 & $\diamondsuit$ & 1/13  & -3.70044     & 13/47 \\
1 & $\spadesuit$   & 0     &              & 12/47 \\
1 & $\clubsuit$    & 1/11  & -3.45943     & 11/47
\end{tabular}
\end{center}

From which we calculate
\[
H(X_2 \mid Y_2) = - \sum_{x} \sum_{y} p_{Y_2}(y) \times
                                    p_{X_2|Y_2}(x,y) \times
                                    \log_2(p_{X_2|Y_2}(x,y))
\]
to get a value of approximately $0.21108$. Note that from $H(X_2) \approx 0.2539$
we dropped to $H(X_2 \mid Y_2) \approx 0.21108$ which reflects the fact that
learning $Y_2$ gives you some information about $X_2$.

\begin{diamondsec}
Why can we ignore the $\log_2(0)$ terms? To be precise, we should define a
function $L(a) = a \times \log_2(a)$, then $H(X) = - \sum_a L(p_X(a))$.
While $\log_2(x)$ diverges at $0$, $L(x)$ may be undefined there but it gets
arbitrarily close to $0$ so we can set $L(0) = 0$ without upsetting anyone.
So the exact rule is that we can ignore a term of the form $0 \times \log_2(0)$.
\end{diamondsec}

\subsection{Example: entropy of the English language}

The English language has 26 letters (ignoring capitalisation), so the entropy of
English text can be at most $\log_2(26) \approx 4.7$ bits per character. However
this limit would only apply if English text were uniformly random, which it is
not.

\emph{Fo? exa??le, in t?is s?nt?nce, som? l?tt?rs have ??en replac?? with questi??
ma??s}, but you should still be able to figure out its meaning. You can even
\emph{sawp arnoud msot letetrs if you levae the frsit and lsat ones anole and the text
wlil stlil be mroe or less raeadble}. So there must be considerable redundancy
in the the English language that our brain can sometimes decode automatically.
Did you notice the repeated word in the last sentence?

Cornell University computed\footnotemark{} the following frequency distribution
(in percent) for the English language:
\footnotetext{Source: http://pi.math.cornell.edu/~mec/2003-2004/cryptography/subs/frequencies.html}

{
\renewcommand{\o}{\phantom{0}}

\begin{minipage}{0.45\textwidth}
\begin{tabular}{lr}
A & 8.12 \\
B & 1.49 \\
C & 2.71 \\
D & 4.32 \\
E & 12.02 \\
F & 2.3\o \\
G & 2.03 \\
H & 5.92 \\
I & 7.32 \\
J & 0.1\o \\
K & 0.69 \\
L & 3.98 \\
M & 2.61
\end{tabular}
\end{minipage}
\begin{minipage}{0.45\textwidth}
\begin{tabular}{lr}
N & 6.95 \\
O & 7.68 \\
P & 1.82 \\
Q & 0.11 \\
R & 6.02 \\
S & 6.28 \\
T & 9.1\o \\
U & 2.88 \\
V & 1.11 \\
W & 2.09 \\
X & 0.17 \\
Y & 2.11 \\
Z & 0.07
\end{tabular}
\end{minipage}
}

In the days of linotype machines, before the QWERTY keyboard,
the keys were arranged in order of letter frequency (though not based on exactly
the same distribution as above) and the first two columns of keys were
\hi{ETAOIN SHRDLU}. References to this phrase occasionally appear in popular
culture.

The probability distribution above reduces the limit for the entropy of an
English character to around 4.18134 bits. But that is still too high. If we look
at the frequency distribution of pairs of letters in English, we see that Q for
example is only ever followed by U (admittedly with some exceptions such as
``Iraq'' or ``Qatar''), the vowels tend to be very sociable letters and pair up
with most other letters whereas consonants are more fussy about which other
consonants they stand next to. This is one reason why monoalphabetic ciphers are
so easy to break. For reference, the 10 most common\footnotemark{} letter pairs are:
\footnotetext{Source: http://jnicholl.org/Cryptanalysis/Data/DigramFrequencies.php}

\begin{tabular}{l|llllllllll}
&    TH   & HE   & AN   & IN   & ER   & RE   & ON   & ES   & EA   & TI   \\
\% & 3.25 & 2.51 & 1.72 & 1.69 & 1.54 & 1.48 & 1.45 & 1.45 & 1.31 & 1.28
\end{tabular}

The most common three-letter combinations are THE and AND, a lot of which come
from these combinations standing alone as common short words. As a general rule,
\hi{structure reduces entropy} and all this additional structure in the English
language reduces the entropy even further. Shannon himself calculated the
entropy of English to be only 2.62 bits per letter.
Since $2.62/\log_2(26) \approx 0.55$, this suggests that just under half all
letters in an average English text are redundant!

David Kahn, in his volume The Codebreakers, gives the following example of a 
poem in which more than half all letters contribute equally well to two 
sentences with opposite meanings:

\needspace{3\baselineskip}
\begin{verbatim}
  cur     f     w        d      dis     and p      
A    sed   iend  rought   eath     ease      ain. 
 bles    fr     b       br     and         ag   
\end{verbatim}

And finally, what is special about the following paragraph, the second
one of the 1939 novel Gadsby by one E. V. Wright 
(that continues like this for 260 pages) ?

\begin{quote}
Up to about its primary school days a child thinks, naturally, only of play. 
But many a form of play contains disciplinary factors. ``You can't do this,'' 
or ``that puts you out,'' shows a child that it must think, practically, or 
fail. Now, if, throughout childhood, a brain has no opposition, it is plain 
that it will attain a position of ``status quo,'' as with our ordinary animals. 
Man knows not why a cow, dog or lion was not born with a brain on a par with 
ours; why such animals cannot add, subtract, or obtain from books and 
schooling, that paramount position which Man holds today.
\end{quote}

\ifdefined\booklet
\else
\end{document}
\fi
