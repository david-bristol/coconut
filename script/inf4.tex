\ifdefined\booklet
\else
\documentclass{scrartcl}

\include{header}

\setuplecture{20}{Shannon's theorems}

\begin{document}

\titlestuff

\noindent\emph{Learning outcomes:} After this lecture and revision, you should
be able to
\begin{itemize}
\item Define the capacity of a channel and compute it in simple cases.
\item State and interpret Shannon's two main theorems.
\item Work with discrete memoryless channels, in particular the binary symmetric
and erasure channels.
\item Construct Huffman codes for a given probability distribution.
\end{itemize}
\fi

\section{Channel coding}

\subsection{Discrete memoryless channels}

In this section we consider a channel with an input alphabet $X$ and an output
alphabet $Y$, represented by a conditional probability function $p_{Y|X}$. This
means that if you send $x \in X$ to the channel, the probability that it outputs
$y \in Y$ is $p_{Y|X}(y, x)$.

The term discrete refers to the channel having finite input and output sets and
memoryless means that which symbol gets received depends only on which symbol
was sent, not what might have happened earlier. In other words, using the channel
multiple times to send multiple symbols, the probabilities of an error in each
symbol are independent of each other.

For example, in a noiseless channel we have $X = Y$ and $p_{Y|X}(y, x)$ equal 
to $1$ if $y = x$, otherwise $p_{Y|X}$ is zero.

As another example, consider a channel with $X = Y = \{00, 01, 10, 11\}$ with
the following transition matrix:

\begin{center}
\begin{tabular}{l|llll}
\diagbox{$X$}{$Y$} & 00 & 01 & 10 & 11 \\
\hline
00      &  0.75 & 0.1  & 0.1  & 0.05 \\
01      &  0.1  & 0.75 & 0.05 & 0.1  \\
10      &  0.1  & 0.05 & 0.75 & 0.1  \\
11      &  0    & 0.1  & 0.1  & 0.8  \\
\end{tabular}
\end{center}

For any input except 11, the input gets transmitted correctly with probability
0.75, with probability 0.2 a single bit gets flipped (which is equally likely
for either bit) and with probability 0.05, both bits flip. Note that the
probabilities of the bits flipping are not independent. If the input was 11, the
output will never be 00.

A channel on its own just gives you a conditional probability distribution: it
makes no sense to ask how likely this channel is to output 11 unless you know
how likely the message 11 is as input. We cannot in general even ask how likely
the channel output is to be correct, since the channel may transmit some inputs
more reliably than others.

Combining a channel with a source (e.g. a probability distribution over inputs) 
gives you a probability distribution over outputs.

In the example above, if the input is uniformly distributed over $X$ then the
probability of the channel returning $y = x$ is 0.7625 whereas if the input is
always 01 or 10 (with equal probabilities) then the channel returns the same
output only 0.75 of the time.

\subsection{Shannon's second theorem}

However, we can still give a measure of how good a channel is based on the
channel characteristics alone; the trick is to take the maximum over all possible
input distributions:

\begin{definition}[channel capacity]
The capacity of a discrete memoryless channel with distribution $p_{Y|X}$ is
the quantity
\[
  C = \max_{X} I(X, Y)
\]
\end{definition}

Shannon's second theorem says that however well we encode, we cannot get
information across a channel at a higher rate than the channel capacity. The
good news is that for any information source and channel, there exists an
encoding arbitrarily close to the capacity. Recall that the information rate
of a block code that has $M$ codewords each $n$ symbols long is $R = \log_2(M)/n$.

\begin{theorem}[Shannon II]
Let a channel with capacity $C$ be given.

For any $\varepsilon > 0$ and for any $R < C$ there exists an block code with
rate $R$ such that the probability of a block not decoding correctly is less
than $\varepsilon$.

There is no encoding that achieves an arbitrarily small block error probability
for any rate $R > C$.
\end{theorem}

The bad news is that the proof of this theorem is non-constructive, so we cannot
actually construct a Shannon-optimal code.

\subsection{Binary Symmetric and Erasure Channels}

Two basic examples of channels that appear in all the Coding Theory textbooks
are the Binary Symmetric Channel (BSC) and the Binary Erasure Channel (BEC).
They both take a parameter $p$ which is the probability that ``something
goes wrong'':

\begin{definition}[BSC and BEC]
The binary symmetric channel $BSC(p)$ for $p \in [0, 1]$ is a channel with 
$ X = \{0, 1\}$, $ Y = \{0, 1\}$ and the following behaviour:
on input $x$, with probability $(1-p)$ the channel outputs $x$ and with
probability $p$ it outputs $1 - x$.

The binary erasure channel $BEC(p)$ for $p \in [0, 1]$ is a channel with 
$ X = \{0, 1\}$, $ Y = \{0, 1, e\}$ and the following behaviour:
on input $x$, with probability $(1-p)$ the channel outputs $x$ and with
probability $p$ it outputs $e$.
\end{definition}

We can draw the channels as follows.

\begin{center}
\begin{tikzpicture}
    \node (bsc0in) at (0, 0) {$0$};
    \node (bsc1in) at (0, -1) {$1$};
    \node (bsc0out) at (2, 0) {$0$};
    \node (bsc1out) at (2, -1) {$1$};
    \draw[->] (bsc0in) to node[above] {$1-p$} (bsc0out);
    \draw[->] (bsc1in) to node[below] {$1-p$} (bsc1out);
    \draw[->] (bsc0in) to node[above] {$p$} (bsc1out);
    \draw[->] (bsc1in) to node[below] {$p$} (bsc0out);
    \node at (1, -2) {BSC($p$)};
    
    \node (bec0in) at (4, 0) {$0$};
    \node (bec1in) at (4, -1) {$1$};
    \node (bec0out) at (6, 0) {$0$};
    \node (bec1out) at (6, -1) {$1$};
    \node (beceout) at (6, -0.5) {$e$};
    \draw[->] (bec0in) to node[above] {$1-p$} (bec0out);
    \draw[->] (bec1in) to node[below] {$1-p$} (bec1out);
    \draw[->] (bec0in) to (beceout);
    \draw[->] (bec1in) to node[above] {$p$} (beceout);
    \node at (5, -2) {BEC($p$)};
\end{tikzpicture}
\end{center}

Both channels have the property that with probability $1 - p$, the input arrives
correctly and with probability $p$, it does not. However in the BEC, unlike the
BSC, you notice when something has gone wrong; the BEC will never output a $0$
if the input was $1$.

Why do we care about these channels? Because they are examples of the basic
channels that we can build in practice. Imagine you have a wire connected to
some electric system and you define the convention that zero volts means $0$
and 10 volts means $1$. What do you do if you measure 8 volts?

There are two answers. One is that you define anything at or above $5.0$ volts
to mean $1$ and anything else to mean $0$. This gets you a BSC, with $p$ the
probability that the voltage you measure is more than $5$ volts away than it
should be.

The other answer is that you define e.g. everything below $1V$ to mean $0$, 
everything at least $9V$ to mean $1$, and everything in between to be an error. 
The probability of an error occurring might be higher in this case (e.g. if the 
actual voltage fluctuates around $2V$ from what it should be) but on the other 
hand, as long as you do get a bit out of the channel, you can have much higher 
confidence that the bit is correct. The BEC is an abstraction of this case where
you assume that if you get a bit out of the channel, then the bit is always
correct.

A basic observation on channels with an error probability $p$ per bit is that
the probability of an $n$-bit message arriving correctly is $(1-p)^n$, which
converges to zero as $n$ gets large even for small but nonzero $p$:
if $p = 10^{(-4)}$ which means that a single bit has a 1 in 10'000 chance of
being flipped, then sending 1000 bits has an almost 10\% chance of a bit flip
happening somewhere and if you send 1MB of data, the chance of it all arriving
successfully is around $3.7 \times 10^{(-44)}$, which we can approximate in
practice by the number zero.

\textbf{Example.}
We will compute the capacity of the BSC($p$) channel. Since $I(X, Y) = H(Y) -
H(Y|X)$ we want to maximise this quantity over all possible input distributions
$X$. Any distribution over the set $\{0, 1\}$ can be described by a value
$q \in [0,1]$ representing the probability that the source outputs 1 (as opposed
to $p$, which is the probability that the channel transmits correctly).

We define $h_2(z) = -z \log_2(z) - (1-z) \log_2(1-z)$ to be the binary entropy
function for parameter $z$ and we compute 
\begin{itemize}
\item $H(Y|X) = (1-q) \times H(Y|X=0) + q \times H(Y|X=1)$
\item $H(Y|X=0) = - p_{Y|X}(0, 0) \log_2(p_{Y|X}(0, 0)) - 
       p_{Y|X}(1, 0) \log_2(p_{Y|X}(1, 0))$
=\\ $-p \log_2(p) - (1-p) \log_2(1-p)$ = $h_2(p)$.
\item For the same reason, $H(Y|X=1) = h_2(p)$.
\item Therefore, $H(Y|X) = (q + (1-q)) \times h_2(p) = h_2(p)$.
\end{itemize}

In other words, the entropy of $Y$ conditioned on a particular input $x$ depends
only on the channel properties. This intuitively makes sense: if you fix the
input, the only uncertainty remaining is the noise in the channel. Since the
BSC is symmetric in its inputs, the terms depending on the input distribution
sum to 1 and disappear.

We want to maximise $H(Y) - H(Y|X)$ over all input distributions $X$. We have
just computed that $H(Y|X)$ does not depend on $X$ at all but only on $p$, so
it remains to maximise $H(Y)$. We know that the maximum entropy is attained for
a uniform distribution, in which case (since the range has two elements) the
entropy is 1 bit. Therefore, the capacity of the BSC($p$) is
\[
    C_{\text{BSC}(p)} = 1 - h_2(p) .
\]
Intuitively, this makes sense: for $p = 0$ or $p = 1$ the capacity is one bit
(you send one bit and you can always tell from the received bit what you sent),
whereas for $p = 1/2$ the output is indepdendent of the input, so you can never
transmit any information at all.

\subsection{The Z channel}

The Z channel is an example of an asymmetric channel:

\begin{center}
\begin{tikzpicture}
\node (in0) at (0, 0) {0};
\node (in1) at (0, -2) {1};
\node (out0) at (2, 0) {0};
\node (out1) at (2, -2) {1};
\draw[->] (in0) to node[above] {1} (out0);
\draw[->] (in1) to node[above left] {$p$} (out0);
\draw[->] (in1) to node[above] {$1-p$} (out1);
\end{tikzpicture}
\end{center}

If the input is 0 then the output is always 0 but an input of 1 can get flipped
to 0 with probability $p$.
Let's try and compute the channel capacity.

As before, let $X$ be the channel input, $Y$ be the channel output and
$q = p_X(0)$. We want a formula for $I(X, Y) = H(Y) - H(Y|X)$.
We define $L(a) = a \times \log_2(a)$ with $L(0) := 0$
and $h_2(a) = -L(a) - L(1-a)$.

Since $p_Y(1) = (1-q)(1-p)$ we have $H(Y) = h_2((1-q)(1-p))$. For the conditional
entropy term, we expand
\[ \begin{array}{rrrll}
    H(Y|X) & = & (1-q) & \times & H(Y|X=1) + q \times  H(Y|X=0) \\ 
           & = & (1-q) & \times & (L(p_{Y|X}(0,1)) + L(p_{Y|X}(1,1))) \\ 
           &   & +   q & \times & (L(p_{Y|X}(0,0)) + L(p_{Y|X}(1,0)))
\end{array} \]

The conditional probabilities themselves we can read off the diagram; since
$L(0) = L(1) = 0$ the second term vanishes and we are left with
$H(Y|X) = (1-q) \times h_2(p)$.

Since this channel is not symmetric, we could not eliminate the $q$ term from
the conditional entropy. Therefore the task is to pick the $q$ that maximises
$I(X, Y) = h_2((1-q)(1-p)) - (1-q) \times h_2(p)$ depending on $p$.
The way to do this involves some analysis that we will not go into here, the
solution turns out to be
\[
    q = 1 - \frac{1}{(1-p) \times \left(1 + 2^{\frac{h_2(p)}{1-p}}\right)}
\]

What we can do is plot $I(X, Y)$ for all $q$ and some values of $p$. The
result is the following, showing that the maximum is attained somewhere in
between $0.5$ and $1$ and the exact position varies with $p$. The smaller
the $p$, the greater the attainable maximum, which makes sense: the less likely
a bit flip, the more information you can get across the channel.

For the perfect case $p = 0$ the formula collapses to $I(X, Y) = h_2(1-q) = h_2(q)$
which makes sense as the channel is simply outputting whatever its input was;
in this case the maximal entropy is achieved for $q = 1/2$.

\begin{center}
\begin{tikzpicture}
	\begin{axis}[
        xmin=0, xmax=1,
        ymin=0, ymax=1,
    ]

    \addplot[mark=none,dotted] coordinates {
        (0,0.0)
        (0.066666666666667,0.12409798306884)
        (0.13333333333333,0.23096034751091)
        (0.2,0.32192809488736)
        (0.26666666666667,0.39783716271778)
        (0.33333333333333,0.45914791702724)
        (0.4,0.50600757931233)
        (0.46666666666667,0.53826892807646)
        (0.53333333333333,0.55547159729456)
        (0.6,0.55677964944704)
        (0.66666666666667,0.54085208297276)
        (0.73333333333333,0.50558726169826)
        (0.8,0.44758467982457)
        (0.86666666666667,0.36082517699473)
        (0.93333333333333,0.23231174881868)
        (1.0,4.5177966968445e-15)
    };
    \addlegendentry{$p=0.25$}


    \addplot[mark=none,dashed] coordinates {
    	(0,0.0)
        (0.066666666666667,0.063458298648303)
        (0.13333333333333,0.12047110770552)
        (0.2,0.17095059445467)
        (0.26666666666667,0.21474491026057)
        (0.33333333333333,0.25162916738782)
        (0.4,0.28129089923069)
        (0.46666666666667,0.30330740860783)
        (0.53333333333333,0.31711028081803)
        (0.6,0.32192809488736)
        (0.66666666666667,0.31668908831502)
        (0.73333333333333,0.29984283988624)
        (0.8,0.26899559358928)
        (0.86666666666667,0.22002600168809)
        (0.93333333333333,0.14417563365187)
        (1.0,2.8865798640254e-15)
    };
   	\addlegendentry{$p=0.5$}
    
    
    \addplot[mark=none] coordinates {
        (0,0.0)
        (0.066666666666667,0.026584031322844)
        (0.13333333333333,0.050926348332551)
        (0.2,0.072905595320056)
        (0.26666666666667,0.092377801560897)
        (0.33333333333333,0.1091703386756)
        (0.4,0.12307343004092)
        (0.46666666666667,0.1338278401747)
        (0.53333333333333,0.14110632842338)
        (0.6,0.14448434380563)
        (0.66666666666667,0.14339080881726)
        (0.73333333333333,0.13701850183232)
        (0.8,0.12414133222413)
        (0.86666666666667,0.10267188372398)
        (0.93333333333333,0.068206388772099)
        (1.0,1.4364866935447e-15)
    };
    \addlegendentry{$p=0.75$}
	\end{axis}
\end{tikzpicture}
\end{center}

\ifdefined\booklet
\else
\end{document}
\fi
