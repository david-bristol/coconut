Exercises, assessment, coursework and exams
===========================================

Exercises
---------

There will be an exercise sheet each week (except weeks 8 and 12).
You can find them in the [PDFs folder](PDF/).

Exercises are graded from one to three stars, as explained on the first page
of [exercise sheet 1](PDF/ex1.pdf). You should attempt the one- and two-star
exercises each week; the three-star ones are extra challenges that go beyond
what you are expected to learn in this unit.

The idea is that you

  * Look at the exercise sheet after the Tuesday lecture, but before your exercise class.
  * Pre-discuss the exercise sheet at the start of the exercise class, that is make sure you understand how to solve the one- and two-star exercises and which techniques to employ for each one.
  * Solve the exercises during the next week (you can start in the exercise class if you want to).
  * Write up and hand in solutions to certain exercises that the TAs will tell you about each week.
  * Post-discuss the exercise sheet in the following exercise class.

If you get stuck on an exercise halfway through and can't make progress after spending a reasonable time on it, stop, and ask for the TA to explain it to you in the post-discussion the following week. Particularly in mathematical subjects, there are often problems that take minutes to solve if you know how, and you can spend hours failing in different ways if you don't get the "trick".
If you find this happening to you on a particular exercise, stop and let the TA help you in the next exercise class.

Assessment and coursework
-------------------------

This unit is assessed by

  * 80% of marks from the final exam.
  * 10% of marks from two pieces of coursework, worth 5% of the unit mark each.
  * 10% of marks for attendance and engagement.

The coursework will consist of two pieces of work, one covering weeks 1-5 and one covering weeks 6-10.

  * [Coursework 1](PDF/cwk1.pdf) due Friday week 6, 5pm.
  * [Coursework 2](PDF/cwk2.pdf) due Friday week 11, 5pm.

The coursework is not intended to test your endurance: I expect that most students will be able to get
most or all of the marks quite easily, although each piece of coursework does contain a three-star question.

For each half of the unit (weeks 1-5 and weeks 6-7,9-11), you get the 5% attendance
and engagement mark if you

  * attend at least 4 out of the 5 exercise classes, and
  * hand in reasonable attempts to the indicated exercises on at least 4 out of the 5 exercise sheets.

"Reasonable attempts" means that an attempt where the answers are wrong, but the student has still clearly spent some time on the exercises, still counts.

The hand-in questions for each sheet in 2019-20 are:

| Part 1 | Part 2 |
|--------|--------|
| Sheet 1: 3a-d | Sheet 6: 2 |
| Sheet 2: 4    | Sheet 7: 1 |
| Sheet 3: 3c-d | Sheet 8: 3a-b |
| Sheet 4: 2a-c | Sheet 9: 1e |
| Sheet 5: 3a-b | Sheet 10: 1a-c |

To track this, there will be sign-in sheets in the exercise classes each week. If you are unwell one week, fill in the university self-certification for absence form and
hand it in or e-mail it to the school office, then hand in your solutions one week later.
If you are required to be absent from an exercise class for another good reason, such as a job interview for an internship,
please let me know before the class in question.

Attendance at lectures will not be tracked and does not count towards your attendance mark.
Since this is a mathematics unit, my style of lecturing will be writing on the blackboard and
replay (lecture recording) may or may not be able to record this, depending on how the new building
is set up. In many other lecture theatres, it records the projector output (e.g. slides) only,
and there are no slides on this unit.

If you do not meet both the attendance and hand-in requirements for one half of the
unit and you have not self-certified for absence, then you will not get the 5% mark
for this half of the unit. The 5% mark is "all or nothing".

This is an attempt at a compromise between making the exercises purely formative and turning everything into coursework. The former has the problem that on Group Theory when I was a TA several years ago, out of an exercise class of 10 the same 3 students turned up each week, and some of the ones I never saw ended up failing the exam. The latter has two problems: first, it's an extra coursework deadline every single week; secondly, the focus shifts from learning to scoring marks - and an exercise you're stuck on can take up almost unlimited time.

Exam
----

The final exam is a 2-hour examination. No textbooks, formula sheets or other 
printed materials are allowed although you can bring and use a calculator as
long as it has the Faculty Seal of Approval. A calculator is not required for
the exam, especially if you are proficient in mental arithmetic on numbers up to
100; if you want to bring one for extra comfort then make sure you know where
the modulo operation is on your calculator.

A table of logarithms to base 2 will be provided as part of the exam.

The list of [calculations you should know](calculations.md) can also be used as revision notes.

For the 2018-19 academic year, I wrote three exams, picked one at random to be the real exam
and released the other two to the students for practice.

Here are the exams and solutions from that year (exam 'C' was the real one):

  * [Exam A](PDF/exams/MOCK-exam-A.pdf) and [Solutions A](PDF/exams/MOCK-A-solutions.pdf)
  * [Exam B](PDF/exams/MOCK-exam-B.pdf) and [Solutions B](PDF/exams/MOCK-B-solutions.pdf)
  * [Exam C](PDF/exams/COMS20002-2019-exam.pdf) and [Solutions C](PDF/exams/COMS20002-2019-solutions.pdf)

The 2018-19 exams contained 8 questions, of which 6 were in "part 1" at roughly
two-star difficulty and worth 70% of the exam; two further questions in "part 2"
at three-star difficulty were worth the other 30% of the exam.

It turned out that my students were a lot better than expected, so for 2019-20 I have made the following changes:

  * 10 questions instead of 8.
  * Removed the distinction between parts 1 and 2.

The questions will contain a mix of calculation, theory, and proof. The style of the questions remains the
same as in last year's exam and on the exercise sheets.
There will also be a combination of straightforward "bookwork" questions,
where you just have to apply something taught in the lectures, and questions where you have to work on a
previously unseen situation and decide by yourselves which technique(s) taught in the unit to use.

I recommend that you show your workings on anything where you can't see the answer immediately.
Among other things, this will allow me to give you partial marks even if the solution is incorrect.
If a question says "briefly explain" or words to that effect, then just a number as a solution will
not give any marks even if it is correct. If a question says "show that", "prove that" or words to
that effect then more formal reasoning is required and at least some of the marks are for properly
documenting your steps - for example, in a proof on group theory, make it clear when you're using
the associative law.

Unless I specifically indicate a method (e.g. Euclid's algorithm, Lagrange interpolation) on a
question, then any valid method that gets the right answer and where I can clearly see the steps
involved can reach full marks.

Please indicate clearly which part of your working is meant to be the solution.


